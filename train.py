# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import print_function

import sys
import os
import argparse
import numpy as np
if '/data/software/opencv-3.4.0/lib/python2.7/dist-packages' in sys.path:
    sys.path.remove('/data/software/opencv-3.4.0/lib/python2.7/dist-packages')
if '/data/software/opencv-3.3.1/lib/python2.7/dist-packages' in sys.path:
    sys.path.remove('/data/software/opencv-3.3.1/lib/python2.7/dist-packages')
import cv2
from datetime import datetime

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.autograd import Variable

from lib.utils.config_parse import cfg_from_file
from lib.ssds_train import train_model
from lib.utils.config_parse import cfg
import shutil
import random

def parse_args():
    """
    Parse input arguments
    """
    parser = argparse.ArgumentParser(description='Train a ssds.pytorch network')

    parser.add_argument('--cfg', dest='config_file',
            help='optional config file', default=None, type=str)

    #RESUME_CHECKPOINT
    parser.add_argument('--model', dest='FIX_MODEL_CHECKPOINT',
            help='optional model file', default=None, type=str) # in this flow dont check the previous model existance. Use this argumetn model as it is
    parser.add_argument('--RESUME_CHECKPOINT', dest='RESUME_CHECKPOINT',
            help='optional model file', default=None, type=str)
    parser.add_argument('--resume_checkpoint', dest='RESUME_CHECKPOINT',
            help='optional model file', default=None, type=str)

    #LOG_DIR
    #for backward comaptability
    parser.add_argument('--logdir', dest='LOG_DIR',
            help='optional model file', default=None, type=str)
    parser.add_argument('--LOG_DIR', dest='LOG_DIR',
            help='optional model file', default=None, type=str)
    parser.add_argument('--log_dir', dest='LOG_DIR',
            help='optional model file', default=None, type=str)

    #PHASE
    parser.add_argument('--phase', dest='PHASE',
            help='optional phase', default=None, type=str)
    parser.add_argument('--PHASE', dest='PHASE',
            help='optional phase', default=None, type=str)

    #EVAL_METHOD
    parser.add_argument('--eval_method', dest='EVAL_METHOD',
            help='optional phase', default=None, type=str)
    parser.add_argument('--EVAL_METHOD', dest='EVAL_METHOD',
            help='optional phase', default=None, type=str)

    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()
    return args

def train():
    #parses command line args
    args = parse_args()

    #parses args from file
    if args.config_file is not None:
        cfg_from_file(args.config_file)

    if (args.FIX_MODEL_CHECKPOINT):
      args.FIX_MODEL_CHECKPOINT = args.FIX_MODEL_CHECKPOINT.replace(" ", "")
      args.FIX_MODEL_CHECKPOINT = args.FIX_MODEL_CHECKPOINT.replace("=", "")
      cfg.RESUME_CHECKPOINT = args.FIX_MODEL_CHECKPOINT
      cfg.CHECK_PREVIOUS = False
      if (os.path.exists(cfg.RESUME_CHECKPOINT) == False):
          print('Exiting the process as asked model for resuming is not found')
          exit()

    if (args.RESUME_CHECKPOINT):
      cfg.RESUME_CHECKPOINT = args.RESUME_CHECKPOINT

    if (args.LOG_DIR):
      cfg.EXP_DIR = args.LOG_DIR

    cfg.LOG_DIR = cfg.EXP_DIR

    if (args.PHASE):
      cfg.PHASE = []
      cfg.PHASE.append(args.PHASE)

    if (args.EVAL_METHOD):
      cfg.DATASET.EVAL_METHOD = args.EVAL_METHOD

    #for backward compatibility
    if cfg.DATASET.DATASET == 'psd':
      cfg.DATASET.DATASET = 'tiod'

    if cfg.DATASET.BGR_OR_RGB == True:
        #cfg.DATASET.PIXEL_MEANS = (123.68, 116.78, 103.94)
        #cfg.DATASET.PIXEL_MEANS = (123, 117, 104)
        cfg.DATASET.PIXEL_MEANS = (128.0, 128.0, 128.0) # simpler mean subtraction to keep data in int8 after mean subtraction

    print("cfg: ", cfg)

    for phase in cfg.PHASE:
      cfg_dir = cfg.LOG_DIR + '/' + phase + '_cfg/'
      os.makedirs(os.path.dirname(cfg_dir), exist_ok=True)
      shutil.copy(args.config_file, cfg_dir)

    # to making every run consistent # TII
    np.random.seed(100)
    torch.manual_seed(100)
    torch.cuda.manual_seed(100)
    random.seed(100)
    torch.cuda.manual_seed_all(999)
    torch.backends.cudnn.enabled = False

    train_model()

if __name__ == '__main__':
    train()
