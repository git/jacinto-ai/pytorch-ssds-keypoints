## Sample config file for key point based object detection such as parking spot detection and others##

# ---------------------------------------------------------------------------- #
# CNN Model related options
# ---------------------------------------------------------------------------- #
MODEL:
  # Meta architecture
  SSDS: ssd_lite

  # Base feature net
  NETS: mobilenet_v1

  # image size given to first layer of CNN
  IMAGE_SIZE: [384, 768]

  # Number of classes including background class
  NUM_CLASSES: 4

  # List of head layers and their output channels. E.g. for value
  # FEATURE_LAYER: [[11, 13, 'S', 'S', 'S', 'S'], [512, 1024, 512, 256, 256, 128]] 11th and 13th layer is
  # used as it is from base net as head for meta architecture. 'S' indicates custom head. And it is attached to the last
  # layer of base net in sequence. FEATURE_LAYER[1] is the list of output channels of each head.
  # Length of FEATURE_LAYER[0/1] tell total number of heads.
  FEATURE_LAYER: [[11, 13, 'S', 'S', 'S', 'S'], [512, 1024, 512, 256, 256, 128]]

  # Minimum and maximum scale ratio. Min and Max value is equally divided into number of heads
  SIZES: [0.06, 0.6]

  # Aspect ratio for anchor boxes in use for each head.
  ASPECT_RATIOS: [[1, 3], [1, 3, 5], [1, 3, 5], [1, 3, 5], [1, 3], [1, 3]]

  # Number of key points used for each object
  NUM_KEYPOINTS: 4

  FEATURE_NORM_EN: False

  # If below flag is set true then box parameter is derived from kep points
  BOXES_FROM_KP: False

  # kernel size for location and confidence head
  LOC_CONF_KERNEL_SIZE: 1

  # True means softmax, false means sigmoid for confidence loss
  SOFTMAX_OR_SIGMOID: True

  # True means RELU6 will not be used
  AVOID_RELU6: True
# ---------------------------------------------------------------------------- #
# Training related options
# ---------------------------------------------------------------------------- #

TRAIN:
  # Maximum number of epochs
  MAX_EPOCHS: 1

  # Checkpoint save frequency
  CHECKPOINTS_EPOCHS: 1

  #Batch size of training phase
  BATCH_SIZE: 32

  #trainable parameters. 'base' is base network, 'extras' are head, 'loc' and 'conf' are location and confidence
  #computation related CNN weights
  TRAINABLE_SCOPE: 'base,extras,loc,conf'

  # List of CNN weights to be loaded when training resumes.
  RESUME_SCOPE: 'base,extras,loc,conf'

# ---------------------------------------------------------------------------- #
# optimizer options
# ---------------------------------------------------------------------------- #

  OPTIMIZER:
    # Type of the optimizer
    OPTIMIZER: sgd
    # Initial learning rate
    LEARNING_RATE: 0.004
    # Momentum
    MOMENTUM: 0.9
    # Weight decay, for regularization
    WEIGHT_DECAY: 0.0001

# ---------------------------------------------------------------------------- #
# Training learning rate scheduler options
# ---------------------------------------------------------------------------- #

  LR_SCHEDULER:
    # type of the LR_SCHEDULER
    SCHEDULER: SGDR
    # Warm_up epochs. After these many epochs LR scheduler kicks on
    # This means learning rate remains fixed for WARM_UP_EPOCHS number of epochs
    WARM_UP_EPOCHS: 100

# ---------------------------------------------------------------------------- #
# Testing or validation related options . Currently below parameters are not supported
# Test and validation is happening for fixed batch size of 2
# ---------------------------------------------------------------------------- #
TEST:
  #Batch size in test or eval phase. Currently this value is ignored and fix batch size of 2 is used.
  BATCH_SIZE: 32

# ---------------------------------------------------------------------------- #
# Matcher options
# ---------------------------------------------------------------------------- #
MATCHER:
  # Matching threshold used in training while matching with anchor boxes.
  MATCHED_THRESHOLD: 0.5
  # Un-Matching threshold used in training while matching with anchor boxes.
  UNMATCHED_THRESHOLD: 0.3
  # alone ground truth boost th. This value is added into alone ground truth IOU with best anchor box
  ALONE_GT_BOOST_TH: 0.3
  NEGPOS_RATIO: 3

# ---------------------------------------------------------------------------- #
# Post process options
# ---------------------------------------------------------------------------- #

POST_PROCESS:
  # Objects whose score is higher than SCORE_THRESHOLD are accounted into mAP calculation
  SCORE_THRESHOLD: 0.01
  # IOU threshold for NMS of detections
  IOU_THRESHOLD: 0.6
  # Maximum number of detection given out for each class
  MAX_DETECTIONS: 200
  #Objects whose score is higher than VIZ_THRESHOLD are dumped into file
  VIZ_THRESHOLD: 0.8
  SAVE_ONNX: True
  #Save proto text file related to SSD meta architecure
  SAVE_OD_PROTOTXT: True
  #Class names
  VIZ_CLASS: ['parking_empty','parking_occupied', 'vehicle']
# ---------------------------------------------------------------------------- #
# Dataset options
# ---------------------------------------------------------------------------- #

DATASET:
  # Name of the dateset. For key point based object detection 'psd' should be selected
  # other supported data set are 'voc' and 'coco'
  DATASET: 'tiod'

  #Base folder of image dir of data set. This is mandatory input field
  DATASET_IMG_DIR: './data/ti_psd_vd_v3_tiny/images'

  #Base folder of annotation dir of data set. This is mandatory input field
  # This field is not needed in testing phase
  DATASET_ANOT_DIR: './data/ti_psd_vd_v3_tiny/annos'

  # List of the folders which will be used in training, These folders should be present at both the paths DATASET_IMG_DIR and DATASET_ANOT_DIR
  # This is not mandatory field, and default value of this is [''], in that case images/annotations present at exactly DATASET_IMG_DIR/DATASET_ANOT_DIR level will be used
  TRAIN_SETS: ['train']

  # List of the folders which will be used in Evaluation, These folders should be present at both the paths DATASET_IMG_DIR and DATASET_ANOT_DIR
  # This is not mandatory field, and default value of this is [''], in that case images/annotations present at exactly DATASET_IMG_DIR/DATASET_ANOT_DIR level will be used
  EVAL_SETS:  ['val']

  # List of the folders which will be used in Testing, This folders should be present at DATASET_IMG_DIR level
  # This is not mandatory field, and default value of this is [''], in that case images present at exactly DATASET_IMG_DIR level will be used for testing
  TEST_SETS:  ['val']

  # Probability for data augmentation while doing training
  PROB: 0.6

  #Total number of keypoints available for each object. For each object same number of keypoints
  NUM_KEYPOINTS: 4

  # For given path DATASET_IMG_DIR/(TRAIN or TEST or EVAL)_SETS all the images are used for training/eval/test.
  # However specific file names can be provided in text files. FILE_NAME[0]/FILE_NAME[1]/FILE_NAME[2] are used for
  # training/eval/test phase respectively. This is not mandatory field. If for particular phase a explicit file name list is
  # not provided then all files at given path is executed.
  #FILE_NAME: ['names.txt', 'names.txt', 'names.txt']

  # List of arguments to be used while training. To check the supported list of augmentation method please check file 'lib\utils\data_augment.py'
  # Currently following augmentation method are supported ['crop', 'expand','mirror', 'distort', 'expand', 'elastic']
  AUG_LIST: ['crop', 'expand','mirror', 'grey']

  CROP_AUG_PARTIAL_OBJ: True
  # evaluation method. Supported values are 'voc','coco'. Default is 'voc'
  EVAL_METHOD: 'voc'
  
  # Object classes text names
  OBJ_CLASSES: ('__background__', 'parking_empty', 'parking_occupied', 'vehicle')

  # image BGR or RGB, False means BGR, True means RGB. Default BGR
  BGR_OR_RGB: True

#  KEEP_DIFFICULT: [True,True]

# Resume checkpoint path. 
# if there are other checkpoints available in train log folder then that supersedes this path
# this path can also be set from command line using --resume_checkpoint option
# Priority order for model path is #1 --model option #2 Any saved checkpoint in train_chkpts folder inside the folder $EXP_DIR #3 below path
# If --model is provided from command line then that supersedes all other options
RESUME_CHECKPOINT: ./weights/ssd_lite_box_kp/ssd_lite_mobilenet_v1_tiod_box_kp.pth

# Three phases are supported train, eval, test. for test phase ground truth is not needed. All the phases should
# be provided as list of strings e.g. ['train','eval'] will execute train and eval. However currently only phase is
# supported in single run. Phase can be provided from command line also as --phase=train, in that case command line arguments
# supersedes this argument.
PHASE: ['train','eval']

# Training check points, test results etc are stored at below path. It also can be set through command line argument e.g. --logdir='./log'
# If it is provided by command line argument then that supersedes the below path
EXP_DIR: './logs/log_exp_tiny'
