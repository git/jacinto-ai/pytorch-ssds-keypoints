######################################
config=$1
config_file=$config".yml"
checkPoint=$2
one=1
testChkpt=$((checkPoint+one))
trainingDir=./experiments/models/
srcDir=$trainingDir$config/
chkptDir=/train_chkpts/
chkptBaseName=ssd_lite_mobilenet_v1_psd_epoch_


chkpt=$trainingDir$config_name$chkptDir$chkptBaseName$checkPoint".pth"
testDir=$trainingDir$config"/test_out/"$testChkpt"/results/VOC2007/Main/"


numDet=-1
folderName="pytorch_"$config
datasetFlag=0
detTh=0.5
######################################
now="$(date +'%Y-%m-%d-%H-%M-%S')"
#now="${now// /}"
saveLabel=$now-$folderName
echo $chkpt
echo $testDir
echo $folderName
#########################################

echo '::::Eval has started::::'
python train.py --cfg=./experiments/cfgs/psd/$config_file --logdir=./experiments/models/$config --phase=eval |& tee -a $srcDir"eval_log.txt"

echo '::::Test has started::::'
python train.py --cfg=./experiments/cfgs/psd/$config_file --logdir=./experiments/models/$config --phase=test |& tee -a $srcDir"eval_test.txt"

echo '::::result transformation started::::'
python /user/a0393749/devkit-datasets/psd/detectConvert.py -i $testDir -o $testDir |& tee -a 

echo '::::output mapping has started::::'
if [ $datasetFlag -eq '0' ]
then 
echo '::::10x meta data::::'
metaDataFolder=meta_data_10w_2h
else
echo '::::2x meta data::::'
metaDataFolder=meta_data_2w_2h
fi

#python /user/a0393749/files/devkit-datasets/psd/showDetect.py -i $testDir  -x /data/ssd/TIAD_2017_local/data/sequence0001/ -o $testDir/results_fe/ -m /user/a0393749/files/tiscapes_psd/$metaDataFolder/ -p $datasetFlag -d $detTh |& tee -a $srcDir"/showdetect_log.txt"

python /user/a0393749/files/devkit-datasets/psd/showDetect.py -i /user/a0393749/files/logs/log_exp_config14_avoid_relu6_softmax/test_out/651/sequence0001/camera001/data/  -x /data/tensorlabdata1/data/datasets/ti/vision/common/TIAD_2017/data/sequence0001/camera001/data/ -o ./ -m /user/a0393749/files/tiscapes_psd/meta_data_10w_2h/ -d 0.5

#echo $cmd
## change below move commands to shell commands
dstDir=~/psd_nw_results/$saveLabel/
mkdir $dstDir

for folder in '/eval_log' '/test_cfg' '/test_out' '/train_cfg' '/train_chkpts' 'train_log'
do

echo '::::Copying of results has started::::'
dstPath=$dstDir$folder
srcPath=$srcDir$folder

echo $srcPath
echo $dstPath
echo 'copying from to' $srcPath $dstPath
cp -a $srcPath $dstPath
done

for file in '/showdetect_log.txt' '/test_log.txt' 
do

echo '::::Copying of results has started::::'
dstPath=$dstDir$file
srcPath=$srcDir$file

echo $srcPath
echo $dstPath
echo 'copying from to' $srcPath $dstPath
cp -a $srcPath $dstPath
done


