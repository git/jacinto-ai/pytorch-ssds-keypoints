# Texas Instruments (C) 2018-2019
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import numpy as np
from lib.utils.pycocotools.cocoeval import COCOeval
import copy


def coco_kp_eval(_gts, _dts,  sigmas, image_ids, catIds):

    coco_eval = COCOeval(iouType='keypoints')
    coco_eval._gts = _gts 
    coco_eval._dts = _dts 

    # As original evaluate function can not be called as ground truth is not in coco format
    # hence copied couple of lines of code from cocoeval.py::evaluate
    
    coco_eval.params.imgIds = list(np.unique(image_ids))
    coco_eval.params.catIds = list(np.unique(catIds))
    coco_eval.params.maxDets = sorted(coco_eval.params.maxDets)
    

    # for every groud truth image and given class calcualte Oks
    coco_eval.ious = {(imgId, catId): coco_eval.computeOks(imgId, catId,sigmas) \
                    for imgId in image_ids
                    for catId in catIds
                    }

    evaluateImg = coco_eval.evaluateImg
    maxDet = coco_eval.params.maxDets[-1]
    coco_eval.evalImgs = [evaluateImg(imgId, catId, areaRng, maxDet)
                     for catId in catIds
                     for areaRng in coco_eval.params.areaRng
                     for imgId in coco_eval.params.imgIds
                     ]
                     
    coco_eval._paramsEval = copy.deepcopy(coco_eval.params)
    coco_eval.accumulate()
    coco_eval.summarize()
    return(coco_eval.stats[0])
    #_print_detection_eval_metrics(len(catIds), coco_eval)

