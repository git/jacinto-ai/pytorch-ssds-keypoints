# Texas Instruments (C) 2018-2019
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from lib.dataset import voc
from lib.dataset import coco
from lib.dataset import ti_data_set
import os

dataset_map = {
                'voc': voc.VOCDetection,
                'coco': coco.COCODetection,
                'tiod':ti_data_set.TIODDetection,
            }

def gen_dataset_fn(name):
    """Returns a dataset func.

    Args:
    name: The name of the dataset.

    Returns:
    func: dataset_fn

    Raises:
    ValueError: If network `name` is not recognized.
    """
    if name not in dataset_map:
        raise ValueError('The dataset unknown %s' % name)
    func = dataset_map[name]
    return func


import torch
import numpy as np

def detection_collate(batch):
    """Custom collate fn for dealing with batches of images that have a different
    number of associated object annotations (bounding boxes).

    Arguments:
        batch: (tuple) A tuple of tensor images and lists of annotations

    Return:
        A tuple containing:
            1) (tensor) batch of images stacked on their 0 dim
            2) (list of tensors) annotations for a given image are stacked on 0 dim
    """
    targets = []
    imgs = []
    for _, sample in enumerate(batch):
        for _, tup in enumerate(sample):
            if torch.is_tensor(tup):
                imgs.append(tup)
            elif isinstance(tup, type(np.empty(0))):
                annos = torch.from_numpy(tup).float()
                targets.append(annos)

    return (torch.stack(imgs, 0), targets)

from lib.utils.data_augment import preproc
import torch.utils.data as data

def load_data(cfg, phase):
    codeSize = 4+cfg.NUM_KEYPOINTS*2+cfg.NUM_EXTRA_FEATURE

    if cfg.DATASET_IMG_DIR != '' or cfg.DATASET_ANOT_DIR != '':
        annot_dir = cfg.DATASET_ANOT_DIR
        img_dir = cfg.DATASET_IMG_DIR
    else:
        annot_dir = os.path.join(cfg.DATASET_DIR, '')
        img_dir = os.path.join(cfg.DATASET_DIR, '')

    file_name = ''

    if phase == 'train':
      if len(cfg.FILE_NAME) >= 1:
        file_name = cfg.FILE_NAME[0]

    if phase == 'eval':
      if len(cfg.FILE_NAME) >= 2:
        file_name = cfg.FILE_NAME[1]

    if phase == 'test':
      if len(cfg.FILE_NAME) >= 3:
        file_name = cfg.FILE_NAME[2]

    class AuxParams:
        def displayArgs(self):
          print("==================================")
          print("dataset: ", self.dataset)
          print("classes: ", self.obj_classes)
          print("==================================")

        def override(self):
          return

    aux_params=AuxParams()
    aux_params.dataset = cfg.DATASET
    aux_params.obj_classes = cfg.OBJ_CLASSES
    aux_params.bgr_or_rgb = cfg.BGR_OR_RGB
    aux_params.displayArgs()


    if phase == 'train' or phase == 'visualize':

        if phase == 'train':
            shuffle_train = True
        else:
            shuffle_train = False

        preproc_obj = preproc(cfg.IMAGE_SIZE, cfg.PIXEL_MEANS, cfg.PROB, codeSize=codeSize + 1, aug_list=cfg.AUG_LIST,
                              center_crop=cfg.CENTER_CROP, numExtraFeatures=cfg.NUM_EXTRA_FEATURE,num_keypoints=cfg.NUM_KEYPOINTS,crop_aug_partial_obj=cfg.CROP_AUG_PARTIAL_OBJ)

        if cfg.DATASET == 'tiod':
            dataset = dataset_map[cfg.DATASET](annot_dir,img_dir, cfg.TRAIN_SETS,cfg.KEEP_DIFFICULT, preproc_obj,
                                               numKeyPoints=cfg.NUM_KEYPOINTS,numExtraFeatures=cfg.NUM_EXTRA_FEATURE, phase='train', file_name=file_name, aux_params=aux_params)
        else:
            dataset = dataset_map[cfg.DATASET](img_dir, cfg.TRAIN_SETS,preproc_obj,numKeyPoints=cfg.NUM_KEYPOINTS)


        # drop last to avoid giving the last batch(by data loader) if it is not multiple of batch size. Otherwise sometime it creates crash.
        # for training it is ok to drop the incomplete batch, as shuffle is set to true.
        data_loader = data.DataLoader(dataset, cfg.TRAIN_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=shuffle_train, collate_fn=detection_collate, pin_memory=True, drop_last = True)
    if phase == 'eval':
        preproc_obj = preproc(cfg.IMAGE_SIZE, cfg.PIXEL_MEANS, -2, codeSize=codeSize + 1)
        preproc(cfg.IMAGE_SIZE, cfg.PIXEL_MEANS, -2, codeSize=codeSize + 1)

        if cfg.DATASET == 'tiod':
            dataset = dataset_map[cfg.DATASET](annot_dir, img_dir, cfg.EVAL_SETS, cfg.KEEP_DIFFICULT, preproc_obj,
                                               numKeyPoints=cfg.NUM_KEYPOINTS, numExtraFeatures=cfg.NUM_EXTRA_FEATURE,
                                               phase='eval',
                                               file_name=file_name, eval_method=cfg.EVAL_METHOD, aux_params=aux_params)
        else:
            dataset = dataset_map[cfg.DATASET](img_dir, cfg.EVAL_SETS,preproc_obj,numKeyPoints=cfg.NUM_KEYPOINTS,phase='eval')


        data_loader = data.DataLoader(dataset, cfg.TEST_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=False, collate_fn=detection_collate, pin_memory=True)
    if phase == 'test':
        preproc_obj = preproc(cfg.IMAGE_SIZE, cfg.PIXEL_MEANS, -2, codeSize=codeSize + 1)

        # in test phase no need to give annot dir 'annot_dir'. But it is needed sometime to display the ground truth.
        if cfg.DATASET == 'tiod':
            dataset = dataset_map[cfg.DATASET]('', img_dir, cfg.TEST_SETS, cfg.KEEP_DIFFICULT, preproc_obj,
                                               numKeyPoints=cfg.NUM_KEYPOINTS, numExtraFeatures=cfg.NUM_EXTRA_FEATURE,
                                               phase='test',
                                               file_name=file_name, eval_method=cfg.EVAL_METHOD, aux_params=aux_params)
        else:
            dataset = dataset_map[cfg.DATASET](img_dir, cfg.TEST_SETS,preproc_obj,numKeyPoints=cfg.NUM_KEYPOINTS,phase='test')


        data_loader = data.DataLoader(dataset, cfg.TEST_BATCH_SIZE, num_workers=cfg.NUM_WORKERS,
                                  shuffle=False, collate_fn=detection_collate, pin_memory=True)
    return data_loader
