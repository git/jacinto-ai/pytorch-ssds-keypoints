# Texas Instruments (C) 2018-2019
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


import os
import pickle
import os.path
import sys
import torch
import torch.utils.data as data
import torchvision.transforms as transforms
from PIL import Image, ImageDraw, ImageFont
import cv2
import numpy as np
from .voc_eval import voc_eval
from .voc_eval import parse_rec
from .coco_kp_eval import coco_kp_eval
from collections import defaultdict
from lib.utils.pycocotools.coco import COCO
from lib.utils.pycocotools.cocoeval import COCOeval

if sys.version_info[0] == 2:
    import xml.etree.cElementTree as ET
else:
    import xml.etree.ElementTree as ET

# for making bounding boxes pretty
COLORS = ((255, 0, 0, 128), (0, 255, 0, 128), (0, 0, 255, 128),
          (0, 255, 255, 128), (255, 0, 255, 128), (255, 255, 0, 128))

class AnnotationTransform(object):

    """Transforms a VOC annotation into a Tensor of bbox coords and label index
    Initilized with a dictionary lookup of classnames to indexes

    Arguments:
        class_to_ind (dict, optional): dictionary lookup of classnames -> indexes
            (default: alphabetic indexing of VOC's 20 classes)
        keep_difficult (bool, optional): keep difficult instances or not
            (default: False)
        height (int): height
        width (int): width
    """

    def __init__(self, class_to_ind=None, keep_difficult=True, numKeyPoints=0, obj_classes=[] ,numExtraFeatures=0):

        self.class_to_ind = class_to_ind or dict(
            zip(obj_classes, range(len(obj_classes))))
        self.keep_difficult = keep_difficult
        self.numKeyPoints = numKeyPoints
        self.keyPointsPattern = []
        for i in range(numKeyPoints):
            self.keyPointsPattern.append('x'+str(i))
            self.keyPointsPattern.append('y' + str(i))
        self.numExtraFeatures= numExtraFeatures
        self.extraFeaturePattern = []
        for i in range(numExtraFeatures):
            self.extraFeaturePattern.append('extra_feat_'+str(i))

    def __call__(self, target):
        """
        Arguments:
            target (annotation) : the target annotation to be made usable
                will be an ET.Element
        Returns:
            a list containing lists of bounding boxes  [bbox coords, class name]
        """
        codeSize = 4 + 2*self.numKeyPoints + 1+self.numExtraFeatures
        res = np.empty((0,codeSize))
        #for elem in target.iter():
         #   print(elem.tag)
        for obj in target.iter('object'):

            #print(obj.attrib)

            difficult = int(obj.find('difficult').text) == 1
            if not self.keep_difficult and difficult:
                continue

            name = obj.find('name').text.lower().strip()

            if name not in self.class_to_ind.keys():
                continue

            bbox = obj.find('bndbox')
            key_points = obj.find('keypoints')
            extra_features = obj.find('extra_feature')

            #for fet in extra_features.iter():
             #   print(fet.tag, fet.attrib)

            #extra_features = obj.find('extra_feature')
            pts = ['xmin', 'ymin', 'xmax', 'ymax']
            key_pts = self.keyPointsPattern
            extr_ftrs = self.extraFeaturePattern
            bndbox = []

            for i, pt in enumerate(pts):
                cur_pt = float(bbox.find(pt).text)# why there is '-1' TII
                # scale height or width
                #cur_pt = cur_pt / width if i % 2 == 0 else cur_pt / height
                bndbox.append(cur_pt)

            for i, pt in enumerate(key_pts):
                cur_pt = float(key_points.find(pt).text)
                # scale height or width
                #cur_pt = cur_pt / width if i % 2 == 0 else cur_pt / height
                bndbox.append(cur_pt)

            for i, pt in enumerate(extr_ftrs):
                cur_pt = float(extra_features.find(pt).text)
                # scale height or width
                #cur_pt = cur_pt / width if i % 2 == 0 else cur_pt / height
                bndbox.append(cur_pt)

            label_idx = self.class_to_ind[name]
            bndbox.append(label_idx)
            res = np.vstack((res, bndbox))  # [xmin, ymin, xmax, ymax, label_ind]
            # img_id = target.find('filename').text[:-4]

        return res  # [[xmin, ymin, xmax, ymax, label_ind], ... ]


class TIODDetection(data.Dataset):

    """VOC Detection Dataset Object

    input is image, target is annotation

    Arguments:
        root (string): filepath annotation folder.
        img_root (string): filepath image folder.
        image_set (string): imageset to use (eg. 'train', 'val', 'test')
        transform (callable, optional): transformation to perform on the
            input image
        target_transform (callable, optional): transformation to perform on the
            target `annotation`
            (eg: take in caption string, return tensor of word indices)
        dataset_name (string, optional): which dataset to load
            (default: 'VOC2007')
    """

    def __init__(self, root, img_root, image_sets,keep_difficult, preproc=None, numKeyPoints=0,numExtraFeatures=0,phase ='train', file_name='names.txt',eval_method = 'voc', aux_params=[]):
        self.root = root
        self.image_set = image_sets
        self.preproc = preproc
        self.file_name=file_name
        self.eval_method = eval_method
        self.bgr_or_rgb = aux_params.bgr_or_rgb

        print("obj_classes in _init_: ", type(aux_params.obj_classes))
#should include numExtraFeatures

        if phase == 'train':
            self.target_transform = AnnotationTransform(keep_difficult=keep_difficult[0], numKeyPoints=numKeyPoints, obj_classes=aux_params.obj_classes, numExtraFeatures=numExtraFeatures)
        else :
            self.target_transform = AnnotationTransform(keep_difficult=keep_difficult[1], numKeyPoints=numKeyPoints, obj_classes=aux_params.obj_classes, numExtraFeatures=numExtraFeatures)

        self.numKeyPoints = numKeyPoints
        self.numExtraFeatures = numExtraFeatures

        if (self.root != ''):
          self._annopath = os.path.join(self.root , '%s.xml')
        else:
          self._annopath = '' # situation when annotation is not provided
          self.target_transform = None
          print(f'Annotation is not provided')

        self._imgpath = os.path.join(img_root, '%s')

        self.ids = list()
        for name in image_sets:
            fileNamesList = os.path.join(img_root, name, file_name)

            if os.path.isfile(fileNamesList) == True:
                for line in open(fileNamesList):
                    folder = os.path.dirname(line)
                    file = os.path.basename(line)
                    self.ids.append(os.path.join(name,folder,file.strip('\n')))
            else:
                for subdir, dirs, files in os.walk(os.path.join(img_root, name)):

                    baseFolder = subdir.replace(img_root,'')
                    # for os.path.join, '/' should not be there in second argument
                    if baseFolder[0] == '/':
                        baseFolder = baseFolder[1:]
                    for file in files:
                        if (file[-3:] =='png') or (file[-3:] =='jpg') or (file[-3:] =='bmp'):
                            self.ids.append(os.path.join(baseFolder, file.strip('\n')))

            #print(self.ids)
        self._classes = aux_params.obj_classes
        self.num_classes = len(self._classes)
        self._class_to_ind = dict(zip(self._classes, range(self.num_classes)))
        print(f'self._annopath = {self._annopath}')
        print(f'self._imgpath = {self._imgpath}')
        print(f'ids to be parsed is {self.ids}')
        print(f'total number of frames in the set {len(self.ids)}')

    def __getitem__(self, index):
        img_id = self.ids[index]

        if self._annopath != '':
          target = ET.parse(self._annopath % img_id[:-4]).getroot()
        else:
          target = []

        img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)

        if self.bgr_or_rgb == True:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        if img is None:
          print("File {} could not be opened".format(self._imgpath % img_id))
        height, width, _ = img.shape
        if self.target_transform is not None:
            target = self.target_transform(target)

        #if target.shape[0] == 0:
            #print(f'Image file {self.ids[index]} doesnt have any annotation')

        if self.preproc is not None:
            img, target, mask = self.preproc(img, target)

        #target[:,-2] = (target[:,-2]+45)/360#TODO AV

        return img, target

    def __len__(self):
        length = len(self.ids)
        return length

    def pull_image(self, index):
        '''Returns the original image object at index in PIL form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            PIL img
        '''
        img_id = self.ids[index]

        img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)

        if self.bgr_or_rgb == True:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        return img

    def pull_anno(self, index):
        '''Returns the original annotation of image at index

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to get annotation of
        Return:
            list:  [img_id, [(label, bbox coords),...]]
                eg: ('001718', [('dog', (96, 13, 438, 332))])
        '''
        img_id = self.ids[index]
        anno = ET.parse(self._annopath % img_id[:-4]).getroot()
        # gt = self.target_transform(anno, 1, 1)
        # gt = self.target_transform(anno)
        # return img_id[1], gt
        if self.target_transform is not None:
            anno = self.target_transform(anno)
        return anno


    def pull_img_anno(self, index):
        '''Returns the original annotation of image at index

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to get annotation of
        Return:
            list:  [img_id, [(label, bbox coords),...]]
                eg: ('001718', [('dog', (96, 13, 438, 332))])
        '''
        img_id = self.ids[index]
        img = cv2.imread(self._imgpath % img_id, cv2.IMREAD_COLOR)

        if self.bgr_or_rgb == True:
            img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

        anno = ET.parse(self._annopath % img_id).getroot()
        gt = self.target_transform(anno)
        height, width, _ = img.shape
        boxes = gt[:,:-1]
        labels = gt[:,-1]
        boxes[:, 0::2] /= width
        boxes[:, 1::2] /= height
        labels = np.expand_dims(labels,1)
        targets = np.hstack((boxes,labels))

        return img, targets

    def pull_tensor(self, index):
        '''Returns the original image at an index in tensor form

        Note: not using self.__getitem__(), as any transformations passed in
        could mess up this functionality.

        Argument:
            index (int): index of img to show
        Return:
            tensorized version of img, squeezed
        '''
        to_tensor = transforms.ToTensor()
        return torch.Tensor(self.pull_image(index)).unsqueeze_(0)

    def evaluate_detections(self, all_boxes, output_dir=None):
        """
        all_boxes is a list of length number-of-classes.
        Each list element is a list of length number-of-images.
        Each of those list elements is either an empty list []
        or a numpy array of detection.

        all_boxes[class][image] = [] or np.array of shape #dets x 5
        """
        self._write_voc_results_file(all_boxes,output_dir,num_extra_features=self.numExtraFeatures)

        aps,kp_errs, ef_errs = self._do_python_eval(output_dir)
        return aps, kp_errs, ef_errs

    def _get_voc_results_file_template(self,output_dir):
        filename = 'comp4_det_test' + '_{:s}.txt'
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        path = os.path.join(output_dir, filename)
        return path

    def _write_voc_results_file(self, all_boxes, output_dir ,num_extra_features , write_image=False, det_th=0.5):

        for cls_ind, cls in enumerate(self._classes):
            cls_ind = cls_ind
            if cls == '__background__':
                continue
            print('Writing {} VOC results file'.format(cls))
            filename = self._get_voc_results_file_template(output_dir).format(cls)
            with open(filename, 'wt') as f:
                for im_ind, image in enumerate(self.ids):
                    dets = all_boxes[cls_ind][im_ind]

                    if dets == []:
                        continue

                    for k in range(dets.shape[0]):
                        f.write('{:s} {:.3f} {:.2f} {:.2f} {:.2f} {:.2f}'.
                                format(image, dets[k, -1],
                                dets[k, 0] + 1, dets[k, 1] + 1,
                                dets[k, 2] + 1, dets[k, 3] + 1))
                        for kpt in range(int((dets.shape[1]-5-num_extra_features)/2)):
                            f.write(' {:.3f} {:.3f}'.format(dets[k, 4 + 2*kpt], dets[k, 4 + 2*kpt + 1]))
                        if num_extra_features>0:
                            for extra in range(num_extra_features):
                                f.write(' {:.3f}'.format(dets[k,-num_extra_features+extra-1]))
                        f.write("\n")


    def _do_python_eval(self, output_dir='output'):
        annopath = self._annopath
        imagesetfile = self._imgpath
        cachedir = os.path.join(self.root, 'annotations_cache')
        aps = []
        kp_errs = []
        ef_errs = []
        # The PASCAL VOC metric changed in 2010
        use_07_metric = True

        if output_dir is not None and not os.path.isdir(output_dir):
            os.mkdir(output_dir)

        fp = open(output_dir+"/results.txt", "w")
        print("\n")
        for i, cls in enumerate(self._classes):

            if cls == '__background__':
                continue

            filename = self._get_voc_results_file_template(output_dir).format(cls)

            if self.eval_method == 'voc':

                rec, prec, ap, kp_err, ef_err, num_boxes = voc_eval(
                                        filename, annopath, self.ids, cls, cachedir, ovthresh=0.5,
                                        use_07_metric=use_07_metric, num_keypoints=self.numKeyPoints,
                                        num_extra_features = self.numExtraFeatures,
                                        keep_difficult=self.target_transform.keep_difficult,
                                        log_dir=output_dir)

            else:

                _gts, _dts = self._get_coco_gt_det(filename, annopath, cls, cachedir,num_keypoints=self.numKeyPoints, keep_difficult=self.target_transform.keep_difficult)
                sigmas = np.array([.89, .89, .89, .89]) / 10.0
                kp_err = coco_kp_eval(_gts,_dts,sigmas,self.ids, [cls])
                ap = 0.0 # AP for box is not evaluated.
                num_boxes = 0

            aps += [ap]

            local_str = 'AP for {} = {:.4f}'.format(cls.ljust(45), ap)
            print(local_str)
            fp.write(local_str)

            if num_boxes > 0:
                kp_err = kp_err / num_boxes
                ef_err = np.sqrt(ef_err / num_boxes)
            else:
                kp_err = 50
                ef_err = np.full(1, 50)

            local_str = 'Key Point Error for {} = {:.4f}'.format(cls.ljust(32), kp_err)
            print(local_str)
            fp.write(local_str)

            if self.numExtraFeatures > 0:
                local_str = 'Extra Feature Error {} = {:.4f}'.format(cls.ljust(32), ef_err)
                print(local_str)
                fp.write(local_str)


            kp_errs += [kp_err]
            ef_errs += [ef_err]

            # disbled the results writing in pkl file. TII
            #if output_dir is not None:
            #    with open(os.path.join(output_dir, cls + '_pr.pkl'), 'wb') as f:
            #        pickle.dump({'rec': rec, 'prec': prec, 'ap': ap}, f)

        fp.close()
        print("\n")
        print('Mean AP                    = {:.4f}'.format(np.mean(aps)))
        print('Mean Key Point Error       = {:.4f}'.format(np.mean(kp_errs)))

        if self.numExtraFeatures > 0:
          print('{:.3f}'.format(np.mean(ef_errs)))

        print('')
        print('--------------------------------------------------------------')
        print('Results computed with the **unofficial** Python eval code.')
        print('Results should be very close to the official MATLAB eval code.')
        print('Recompute with `./tools/reval.py --matlab ...` for your paper.')
        print('-- Thanks, The Management')
        print('--------------------------------------------------------------')
        return aps,kp_errs,ef_errs

    def show(self, index):
        img, target = self.__getitem__(index)
        for obj in target:
            obj = obj.astype(np.int)
            cv2.rectangle(img, (obj[0], obj[1]), (obj[2], obj[3]), (255,0,0), 3)
        cv2.imwrite('./image.jpg', img)

    def _coco_results_one_category(self, boxes, keypoints, cat_id):
        results = []
        for im_ind, index in enumerate(self.image_indexes):
            dets = boxes[im_ind].astype(np.float)
            if dets == []:
                continue
            scores = dets[:, -1]
            xs = dets[:, 0]
            ys = dets[:, 1]
            ws = dets[:, 2] - xs + 1
            hs = dets[:, 3] - ys + 1

            keypoints_v = np.empty(keypints.shape)

            # making all keypoints visible for parking spot.
            for idx in range(self.numKeyPoints):
                keypoints_v[:,3*idx + 0] = keypoints[:,2*idx + 0]
                keypoints_v[:,3*idx + 1] = keypoints[:,2*idx + 1]
                keypoints_v[:,3*idx + 2] = 2

            for k in range(dets.shape[0]):

                results.extend(
                  [{'image_id' : index,
                    'category_id' : cat_id,
                    'bbox' : [xs[k], ys[k], ws[k], hs[k]],
                    'keypoints' : [keypoints_v[k]],
                    'score' : scores[k]}])

        return results

    def _get_coco_gt_det(self, detpath, annopath, cls, cachedir, num_keypoints=0, keep_difficult=False):

        if not os.path.isdir(cachedir):
            os.mkdir(cachedir)
        cachefile = os.path.join(cachedir, 'annots.pkl')
        # read list of images
        disable_cache = True
        if not os.path.isfile(cachefile) or disable_cache:
            # load annots
            recs = {}
            for i, imagename in enumerate(self.ids):

                recs[imagename] = parse_rec(annopath%imagename, num_keypoints) # TII need to correct this
                #if i % 100 == 0:
                #    print('Reading annotation for {:d}/{:d}'.format(
                #        i + 1, len(self.ids)))
            # save
            if disable_cache == False:
                print('Saving cached annotations to {:s}'.format(cachefile))
                with open(cachefile, 'wb') as f:
                    pickle.dump(recs, f)
        else:
            # load
            with open(cachefile, 'rb') as f:
                recs = pickle.load(f)

        # extract gt objects for this class
        class_recs_gt = {}
        for imagename in self.ids:

            R = [obj for obj in recs[imagename] if obj['name'] == cls]

            cur_img_annot = []
            for x in R:
                bbox = np.array(x['bbox'])
                keypoints = np.array(x['keypoints'])
                difficult = np.array(x['difficult']).astype(np.bool) and (not keep_difficult)

                keypoints_v = np.empty(3*num_keypoints)
                keypoints_v[0::3] = keypoints[0::2]
                keypoints_v[1::3] = keypoints[1::2]
                keypoints_v[2::3] = 2
                cur_annot = {   'id': imagename,
                                'bbox': bbox,
                                'ignore': difficult, #@TODO use the keep_difficult flag
                                'keypoints': keypoints_v,
                                'area' : (bbox[2] - bbox[0])*(bbox[3] - bbox[1]), #@TODO need to update annotation to get acctual area of groundtruth
                                'iscrowd' : False
                                     }
                cur_img_annot.append(cur_annot)

            class_recs_gt[imagename,cls] = cur_img_annot

        # read dets. It is possible for some images, detections might not be present for a given class
        class_recs_dt = {}

        detfile = detpath.format(cls)
        with open(detfile, 'r') as f:
            lines = f.readlines()

        splitlines = [x.strip().split(' ') for x in lines]
        imagenames = [x[0] for x in splitlines]
        confidence = np.array([float(x[1]) for x in splitlines])
        bbox = np.array([[float(z) for z in x[2:6]] for x in splitlines])
        keypoints = np.array([[float(z) for z in x[6:]] for x in splitlines])

        for gt_img in self.ids:

            indices = [i for i, det_img in enumerate(imagenames) if det_img == gt_img]
            cur_img_det = []

            for idx in indices:
                bbox_c = bbox[idx]
                keypoints_c = keypoints[idx]
                confidence_c = confidence[idx]
                keypoints_v = np.empty(3*num_keypoints)
                keypoints_v[0::3] = keypoints_c[0::2]
                keypoints_v[1::3] = keypoints_c[1::2]
                keypoints_v[2::3] = 2
                cur_det = {
                            'id': gt_img,
                            'bbox': bbox_c,
                            'keypoints': keypoints_v,
                            'score': confidence_c,
                            'area': (bbox_c[2] - bbox_c[0])*(bbox_c[3] - bbox_c[1]), #@TODO need to pixel level detection area
                            }
                cur_img_det.append(cur_det)

            class_recs_dt[gt_img,cls] = cur_img_det

        return class_recs_gt, class_recs_dt
