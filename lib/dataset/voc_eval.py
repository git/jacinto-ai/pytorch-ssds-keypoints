# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# --------------------------------------------------------
# Fast/er R-CNN
# Licensed under The MIT License [see LICENSE for details]
# Written by Bharath Hariharan
# --------------------------------------------------------

import xml.etree.ElementTree as ET
import os
import pickle
import numpy as np
import pdb
import operator
import matplotlib

matplotlib.use('Agg') # Must be before importing matplotlib.pyplot or pylab!
import matplotlib.pyplot as plt

def parse_rec(filename, num_keypoints = 0, num_extra_features=0):
    """ Parse a PASCAL VOC xml file """
    tree = ET.parse(filename)
    objects = []
    for obj in tree.findall('object'):
        obj_struct = {}
        obj_struct['name'] = obj.find('name').text
        obj_struct['pose'] = obj.find('pose').text
        obj_struct['truncated'] = int(obj.find('truncated').text)
        obj_struct['difficult'] = int(obj.find('difficult').text)
        bbox = obj.find('bndbox')
        obj_struct['bbox'] = [float(bbox.find('xmin').text),
                              float(bbox.find('ymin').text),
                              float(bbox.find('xmax').text),
                              float(bbox.find('ymax').text)]

        keypoints = obj.find('keypoints')
        keypoints_list = []

        for idx in range(num_keypoints):
            keypoints_list.append(int(keypoints.find('x' + str(idx)).text))
            keypoints_list.append(int(keypoints.find('y' + str(idx)).text))

        obj_struct['keypoints'] = keypoints_list

        extra_features = obj.find('extra_feature')
        extra_features_list = []

        for idx in range(num_extra_features):
            extra_features_list.append(float(extra_features.find('extra_feat_' + str(idx)).text))

        obj_struct['extra_feature'] = extra_features_list

        objects.append(obj_struct)

    return objects



def voc_ap(rec, prec, use_07_metric=False,log_dir=None, classname=None):
    """ ap = voc_ap(rec, prec, [use_07_metric])
    Compute VOC AP given precision and recall.
    If use_07_metric is true, uses the
    VOC 07 11 point method (default:False).
    """
    if use_07_metric:
        # 11 point metric
        ap = 0.

        if log_dir != None and classname != None:
            myfile = open('{}/ap_calc_stat{}.txt'.format(log_dir, classname), "w")

        for t in np.arange(0., 1.1, 0.1):
            if np.sum(rec >= t) == 0:
                p = 0
                myfile.write("t->{} p->{}\n".format(t,p))
            else:
                p = np.max(prec[rec >= t])
                myfile.write("t->{} p->{}\n".format(t,p))
            ap = ap + p / 11.

        myfile.close()
    else:
        # correct AP calculation
        # first append sentinel values at the end
        mrec = np.concatenate(([0.], rec, [1.]))
        mpre = np.concatenate(([0.], prec, [0.]))

        # compute the precision envelope
        for i in range(mpre.size - 1, 0, -1):
            mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

        # to calculate area under PR curve, look for points
        # where X axis (recall) changes value
        i = np.where(mrec[1:] != mrec[:-1])[0]

        # and sum (\Delta recall) * prec
        ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
    return ap

def voc_eval(detpath,
             annopath,
             imagesetfile,
             classname,
             cachedir,
             ovthresh=0.5,
             use_07_metric=False,
             num_keypoints=0,
             num_extra_features=0,
             keep_difficult=False,
             log_dir=None):
    """rec, prec, ap = voc_eval(detpath,
                                annopath,
                                imagesetfile,
                                classname,
                                [ovthresh],
                                [use_07_metric])

    Top level function that does the PASCAL VOC evaluation.

    detpath: Path to detections
        detpath.format(classname) should produce the detection results file.
    annopath: Path to annotations
        annopath.format(imagename) should be the xml annotations file.
    imagesetfile: Text file containing the list of images, one image per line.
    classname: Category name (duh)
    cachedir: Directory for caching the annotations
    [ovthresh]: Overlap threshold (default = 0.5)
    [use_07_metric]: Whether to use VOC07's 11 point AP computation
        (default False)
    """
    # assumes detections are in detpath.format(classname)
    # assumes annotations are in annopath.format(imagename)
    # assumes imagesetfile is a text file with each line an image name
    # cachedir caches the annotations in a pickle file

    # first load gt
    if not os.path.isdir(cachedir):
        os.mkdir(cachedir)
    cachefile = os.path.join(cachedir, 'annots.pkl')
    # read list of images
    disable_cache = True
    if not os.path.isfile(cachefile) or disable_cache:
        # load annots
        recs = {}
        for i, imagename in enumerate(imagesetfile):
            file = os.path.splitext(imagename)[0]
            recs[imagename] = parse_rec(annopath%file, num_keypoints,num_extra_features)
            #if i % 100 == 0:
            #    print('Reading annotation for {:d}/{:d}'.format(
            #        i + 1, len(imagesetfile)))

        # save
        if disable_cache == False:
          print('Saving cached annotations to {:s}'.format(cachefile))
          with open(cachefile, 'wb') as f:
              pickle.dump(recs, f)
    else:
        # load
        with open(cachefile, 'rb') as f:
            recs = pickle.load(f)

    # extract gt objects for this class
    class_recs = {}
    npos = 0

    # frame level total number of grond truth
    npos_list = []
    for imagename in imagesetfile:
        R = [obj for obj in recs[imagename] if obj['name'] == classname]
        bbox = np.array([x['bbox'] for x in R])
        keypoints = np.array([x['keypoints'] for x in R])
        extra_feature = np.array([x['extra_feature'] for x in R])
        difficult = np.array([x['difficult'] for x in R]).astype(np.bool)
        det = [False] * len(R)

        if keep_difficult == False:
            cur_npos = sum(~difficult)
        else:
            cur_npos = len(difficult)

        npos = npos + cur_npos
        npos_list.append(cur_npos)
        class_recs[imagename] = {'bbox': bbox,
                                 'difficult': difficult,
                                 'det': det,
                                 'keypoints': keypoints,
                                 'extra_feature' : extra_feature
                                 }

    # read dets
    detfile = detpath.format(classname)
    with open(detfile, 'r') as f:
        lines = f.readlines()

    splitlines = [x.strip().split(' ') for x in lines]
    image_ids = [x[0] for x in splitlines]
    confidence = np.array([float(x[1]) for x in splitlines])
    BB = np.array([[float(z) for z in x[2:]] for x in splitlines])

    # sort by confidence
    sorted_ind = np.argsort(-confidence)
    sorted_scores = np.sort(-confidence)
    BB = BB[sorted_ind, :]
    image_ids = [image_ids[x] for x in sorted_ind]

        # go down dets and mark TPs and FPs
    nd = len(image_ids)
    tp = np.zeros(nd)
    fp = np.zeros(nd)
    arbt_kp = False
    if arbt_kp == False:
        gt_pt_per_edge = int(num_keypoints / 4)
        ignore_x_y = []
    else:
        gt_pt_per_edge = 1
        ignore_x_y = [0.0,0.0]
    norm_i = 0
    num_boxes = 0
    extra_feature_error_i = np.zeros(num_extra_features)
    img_kp_err_dict = {}
    print_kp_err_log = False

    for d in range(nd):
        R = class_recs[image_ids[d]]
        bb = BB[d, :4].astype(float)
        kp = BB[d, 4:4+2*num_keypoints].astype(float)
        extra_feature = BB[d, 4+2*num_keypoints : 4+2*num_keypoints + num_extra_features].astype(float)

        ovmax = -np.inf
        BBGT = R['bbox'].astype(float)
        KPGT = R['keypoints'].astype(float)
        EFGT = R['extra_feature'].astype(float)
        conf = -sorted_scores[d]


        if BBGT.size > 0:
            # compute overlaps
            # intersection
            ixmin = np.maximum(BBGT[:, 0], bb[0])
            iymin = np.maximum(BBGT[:, 1], bb[1])
            ixmax = np.minimum(BBGT[:, 2], bb[2])
            iymax = np.minimum(BBGT[:, 3], bb[3])
            iw = np.maximum(ixmax - ixmin + 1., 0.)
            ih = np.maximum(iymax - iymin + 1., 0.)
            inters = iw * ih

                # union
            uni = ((bb[2] - bb[0] + 1.) * (bb[3] - bb[1] + 1.) +
                   (BBGT[:, 2] - BBGT[:, 0] + 1.) *
                   (BBGT[:, 3] - BBGT[:, 1] + 1.) - inters)

            overlaps = inters / uni
            ovmax = np.max(overlaps)
            jmax = np.argmax(overlaps)

        if ovmax > ovthresh:
            if ((R['difficult'][jmax] == False) or (keep_difficult==True)):
                if not R['det'][jmax]:
                    tp[d] = 1.
                    R['det'][jmax] = 1

                    ## keypoint error related code
                    boxPeriphery = 0.0
                    norm_box_i = 0.0
                    norm_box_i_rev = 0.0

                    # Boxes which arecounted for Kp or Ef Error calculation
                    if conf > 0.5:
                        num_boxes = num_boxes + 1

                    #Kp error calculation
                    if num_keypoints > 0 and conf > 0.5:
                        det_keypoints = kp.reshape([num_keypoints,2])
                        gt_keypoints =  KPGT[jmax].reshape([num_keypoints,2])

                        gt_keypoints_2 = np.concatenate((gt_keypoints, gt_keypoints))  # 1-2-3-4-1-2-3-4
                        gt_keypoints_2_rev = gt_keypoints_2[::-1]  # 4-3-2-1-4-3-2-1

                        for gt_pt, det_pt, next_gt_pt in zip(gt_keypoints, det_keypoints,gt_keypoints_2[1:]):  # for each keypoint in one detection
                            if (len(ignore_x_y) == 0) or ((len(ignore_x_y) == 2) and (ignore_x_y[0] !=gt_pt[0]) and (ignore_x_y[1] !=gt_pt[1])):
                                norm_box_i = norm_box_i + np.linalg.norm(gt_pt - det_pt)
                                boxPeriphery = boxPeriphery + np.linalg.norm(
                                    next_gt_pt - gt_pt)

                        revStartPoint = (num_keypoints-1) * gt_pt_per_edge

                        if arbt_kp == True:
                            boxPeriphery = 2*(bb[2] - bb[0]) + 2*(bb[3] - bb[1])

                        if gt_pt_per_edge == 1:
                            revStartPoint = revStartPoint - 1

                        for gt_pt, det_pt in zip(gt_keypoints_2_rev[revStartPoint:],det_keypoints):  # for each keypoint in one detection
                            if (len(ignore_x_y) == 0) or ((len(ignore_x_y) == 2) and (ignore_x_y[0] !=gt_pt[0]) and (ignore_x_y[1] !=gt_pt[1])):
                                norm_box_i_rev = norm_box_i_rev + np.linalg.norm(gt_pt - det_pt)  # gt_pt -- 2-1-4-3

                        if norm_box_i_rev < norm_box_i:
                            norm_box_i = norm_box_i_rev
                            print('key points has matched in reverse order')

                        #norm_box_i = norm_box_i / gt_pt_per_edge
                        norm_box_i = np.clip(norm_box_i / boxPeriphery, 0.0, 1.0)
                        # Extra feature mean square error

                        if norm_box_i >= 5.4: # test code
                            print(f'Current image in evolution is {image_ids[d]}')
                            print('deviation in 4 corner key points wrt periphery',norm_box_i)
                            print('gt_keypoints', gt_keypoints)
                            print('det_keypoints', det_keypoints)
                            print('det_score', conf)

                        norm_i = norm_i + norm_box_i
                        if print_kp_err_log == True:
                            if image_ids[d] in img_kp_err_dict.keys():
                                img_kp_err_dict[image_ids[d]].append(norm_box_i)
                            else:
                                img_kp_err_dict[image_ids[d]] = [norm_box_i]

                        if image_ids[d] == 'val/sequence0002_camera001_data_0000000490--.png': # test code
                            print(gt_keypoints)
                            print(det_keypoints)
                            print(norm_box_i)
                            print(boxPeriphery)

                        #Ef error calculation
                    if num_extra_features > 0 and conf > 0.5:
                        current_feature_error = np.zeros(num_extra_features)
                        for ef in range(num_extra_features):
                            current_feature_error[ef] += np.linalg.norm(EFGT[jmax][ef] - extra_feature[ef])**2 # extra_feature error is calcualterd here
                        for ef in range(num_extra_features):
                            extra_feature_error_i[ef] += current_feature_error[ef]
                else:
                    fp[d] = 1.
        else:
            fp[d] = 1.


    # compute frame wise true positive and false positive
    if log_dir != None:
        myfile = open('{}/tp_fp_gt_stat{}.txt'.format(log_dir,classname), "w")
        for img_id , img in enumerate(imagesetfile):
            img_indices = [i for i in range(len(image_ids)) if image_ids[i] == img]
            tot_tp = tp[img_indices].sum()
            tot_fp = fp[img_indices].sum()
            tot_gt = npos_list[img_id]
            myfile.write("{} -> tp->{} fp->{} tot_gt->{} \n".format(img, str(tot_tp), str(tot_fp), str(tot_gt)))
        myfile.close()


    # compute precision recall
    fp = np.cumsum(fp)
    tp = np.cumsum(tp)
    rec = tp / (float(npos) + np.finfo(np.float64).eps)
    # avoid divide by zero in case the first detection matches a difficult
    # ground truth
    prec = tp / np.maximum(tp + fp, np.finfo(np.float64).eps)

    if log_dir != None:
        plt.xlabel('Recall')
        plt.ylabel('Precision')
        plt.plot(rec, prec)
        plt.savefig('{}/rec_prec_{}.jpg'.format(log_dir,classname))
        plt.clf()
        plt.cla()
        plt.close()
    if print_kp_err_log == True:
        sorted_x = sorted(img_kp_err_dict.items(), key=operator.itemgetter(0))
        print(sorted_x)

    ap = voc_ap(rec, prec, use_07_metric, log_dir=log_dir,classname=classname)

    return rec, prec, ap, norm_i, np.array(extra_feature_error_i), num_boxes
