# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.


from google.protobuf.internal import enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from google.protobuf import reflection as _reflection
from google.protobuf import symbol_database as _symbol_database
# @@protoc_insertion_point(imports)

_sym_db = _symbol_database.Default()




DESCRIPTOR = _descriptor.FileDescriptor(
  name='mmdet_meta_arch.proto',
  package='tidl_meta_arch',
  syntax='proto2',
  serialized_options=None,
  serialized_pb=b'\n\x15mmdet_meta_arch.proto\x12\x0etidl_meta_arch\"\xf1\x01\n\x0cTIDLMetaArch\x12\x0c\n\x04name\x18\x01 \x01(\t\x12\x31\n\tcaffe_ssd\x18\x02 \x03(\x0b\x32\x1e.tidl_meta_arch.TidlMaCaffeSsd\x12\x37\n\rtf_od_api_ssd\x18\x03 \x03(\x0b\x32 .tidl_meta_arch.TidlMaTfOdApiSsd\x12+\n\x08tidl_ssd\x18\x04 \x03(\x0b\x32\x19.tidl_meta_arch.TidlMaSsd\x12:\n\x10tidl_faster_rcnn\x18\x05 \x03(\x0b\x32 .tidl_meta_arch.TidlMaFasterRcnn\"I\n\x0cTIDLNmsParam\x12\x1a\n\rnms_threshold\x18\x01 \x01(\x02:\x03\x30.3\x12\r\n\x05top_k\x18\x02 \x01(\x05\x12\x0e\n\x03\x65ta\x18\x03 \x01(\x02:\x01\x31\"\x94\x02\n\x11PriorBoxParameter\x12\x10\n\x08min_size\x18\x01 \x03(\x02\x12\x10\n\x08max_size\x18\x02 \x03(\x02\x12\x14\n\x0c\x61spect_ratio\x18\x03 \x03(\x02\x12\x12\n\x04\x66lip\x18\x04 \x01(\x08:\x04true\x12\x13\n\x04\x63lip\x18\x05 \x01(\x08:\x05\x66\x61lse\x12\x10\n\x08variance\x18\x06 \x03(\x02\x12\x10\n\x08img_size\x18\x07 \x01(\r\x12\r\n\x05img_h\x18\x08 \x01(\r\x12\r\n\x05img_w\x18\t \x01(\r\x12\x0c\n\x04step\x18\n \x01(\x02\x12\x0e\n\x06step_h\x18\x0b \x01(\x02\x12\x0e\n\x06step_w\x18\x0c \x01(\x02\x12\x13\n\x06offset\x18\r \x01(\x02:\x03\x30.5\x12\x17\n\x0cnum_keypoint\x18\x0e \x01(\r:\x01\x30\"\xbd\x01\n\x10TIDLOdSaveParams\x12\x18\n\x10output_directory\x18\x01 \x01(\t\x12\x1a\n\x12output_name_prefix\x18\x02 \x01(\t\x12\x15\n\routput_format\x18\x03 \x01(\t\x12\x16\n\x0elabel_map_file\x18\x04 \x01(\t\x12\x16\n\x0ename_size_file\x18\x05 \x01(\t\x12\x16\n\x0enum_test_image\x18\x06 \x01(\r\x12\x14\n\x0cresize_param\x18\x07 \x01(\t\"\xd1\x03\n\x0eTIDLOdPostProc\x12\x13\n\x0bnum_classes\x18\x01 \x01(\r\x12\x1c\n\x0eshare_location\x18\x02 \x01(\x08:\x04true\x12\x1e\n\x13\x62\x61\x63kground_label_id\x18\x03 \x01(\x05:\x01\x30\x12/\n\tnms_param\x18\x04 \x01(\x0b\x32\x1c.tidl_meta_arch.TIDLNmsParam\x12;\n\x11save_output_param\x18\x05 \x01(\x0b\x32 .tidl_meta_arch.TIDLOdSaveParams\x12:\n\tcode_type\x18\x06 \x01(\x0e\x32\x1f.tidl_meta_arch.TIDLBoxCodeType:\x06\x43ORNER\x12)\n\x1avariance_encoded_in_target\x18\x08 \x01(\x08:\x05\x66\x61lse\x12\x16\n\nkeep_top_k\x18\x07 \x01(\x05:\x02-1\x12\x1c\n\x14\x63onfidence_threshold\x18\t \x01(\x02\x12\x18\n\tvisualize\x18\n \x01(\x08:\x05\x66\x61lse\x12\x1b\n\x13visualize_threshold\x18\x0b \x01(\x02\x12\x11\n\tsave_file\x18\x0c \x01(\t\x12\x17\n\x0cnum_keypoint\x18\r \x01(\r:\x01\x30\"\xb8\x02\n\tTidlMaSsd\x12\x11\n\tbox_input\x18\x01 \x03(\t\x12\x13\n\x0b\x63lass_input\x18\x02 \x03(\t\x12\x0e\n\x06output\x18\x03 \x01(\t\x12\x0c\n\x04name\x18\x04 \x01(\t\x12\x10\n\x08in_width\x18\x05 \x01(\r\x12\x11\n\tin_height\x18\x06 \x01(\r\x12\x44\n\x0fscore_converter\x18\x07 \x01(\x0e\x32\".tidl_meta_arch.TIDLScoreConverter:\x07SOFTMAX\x12:\n\x0fprior_box_param\x18\x08 \x03(\x0b\x32!.tidl_meta_arch.PriorBoxParameter\x12>\n\x16\x64\x65tection_output_param\x18\t \x01(\x0b\x32\x1e.tidl_meta_arch.TIDLOdPostProc\"\xbd\x02\n\x0eTidlMaCaffeSsd\x12\x11\n\tbox_input\x18\x01 \x03(\t\x12\x13\n\x0b\x63lass_input\x18\x02 \x03(\t\x12\x0e\n\x06output\x18\x03 \x01(\t\x12\x0c\n\x04name\x18\x04 \x01(\t\x12\x10\n\x08in_width\x18\x05 \x01(\r\x12\x11\n\tin_height\x18\x06 \x01(\r\x12\x44\n\x0fscore_converter\x18\x07 \x01(\x0e\x32\".tidl_meta_arch.TIDLScoreConverter:\x07SOFTMAX\x12:\n\x0fprior_box_param\x18\x08 \x03(\x0b\x32!.tidl_meta_arch.PriorBoxParameter\x12>\n\x16\x64\x65tection_output_param\x18\t \x01(\x0b\x32\x1e.tidl_meta_arch.TIDLOdPostProc\" \n\x10TidlMaTfOdApiSsd\x12\x0c\n\x04name\x18\x01 \x01(\t\" \n\x10TidlMaFasterRcnn\x12\x0c\n\x04name\x18\x01 \x01(\t*?\n\x0fTIDLBoxCodeType\x12\n\n\x06\x43ORNER\x10\x01\x12\x0f\n\x0b\x43\x45NTER_SIZE\x10\x02\x12\x0f\n\x0b\x43ORNER_SIZE\x10\x03*<\n\x12TIDLScoreConverter\x12\x0c\n\x08IDENTITY\x10\x00\x12\x0b\n\x07SIGMOID\x10\x01\x12\x0b\n\x07SOFTMAX\x10\x02'
)

_TIDLBOXCODETYPE = _descriptor.EnumDescriptor(
  name='TIDLBoxCodeType',
  full_name='tidl_meta_arch.TIDLBoxCodeType',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='CORNER', index=0, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CENTER_SIZE', index=1, number=2,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='CORNER_SIZE', index=2, number=3,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=2002,
  serialized_end=2065,
)
_sym_db.RegisterEnumDescriptor(_TIDLBOXCODETYPE)

TIDLBoxCodeType = enum_type_wrapper.EnumTypeWrapper(_TIDLBOXCODETYPE)
_TIDLSCORECONVERTER = _descriptor.EnumDescriptor(
  name='TIDLScoreConverter',
  full_name='tidl_meta_arch.TIDLScoreConverter',
  filename=None,
  file=DESCRIPTOR,
  values=[
    _descriptor.EnumValueDescriptor(
      name='IDENTITY', index=0, number=0,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SIGMOID', index=1, number=1,
      serialized_options=None,
      type=None),
    _descriptor.EnumValueDescriptor(
      name='SOFTMAX', index=2, number=2,
      serialized_options=None,
      type=None),
  ],
  containing_type=None,
  serialized_options=None,
  serialized_start=2067,
  serialized_end=2127,
)
_sym_db.RegisterEnumDescriptor(_TIDLSCORECONVERTER)

TIDLScoreConverter = enum_type_wrapper.EnumTypeWrapper(_TIDLSCORECONVERTER)
CORNER = 1
CENTER_SIZE = 2
CORNER_SIZE = 3
IDENTITY = 0
SIGMOID = 1
SOFTMAX = 2



_TIDLMETAARCH = _descriptor.Descriptor(
  name='TIDLMetaArch',
  full_name='tidl_meta_arch.TIDLMetaArch',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='tidl_meta_arch.TIDLMetaArch.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='caffe_ssd', full_name='tidl_meta_arch.TIDLMetaArch.caffe_ssd', index=1,
      number=2, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='tf_od_api_ssd', full_name='tidl_meta_arch.TIDLMetaArch.tf_od_api_ssd', index=2,
      number=3, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='tidl_ssd', full_name='tidl_meta_arch.TIDLMetaArch.tidl_ssd', index=3,
      number=4, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='tidl_faster_rcnn', full_name='tidl_meta_arch.TIDLMetaArch.tidl_faster_rcnn', index=4,
      number=5, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=42,
  serialized_end=283,
)


_TIDLNMSPARAM = _descriptor.Descriptor(
  name='TIDLNmsParam',
  full_name='tidl_meta_arch.TIDLNmsParam',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='nms_threshold', full_name='tidl_meta_arch.TIDLNmsParam.nms_threshold', index=0,
      number=1, type=2, cpp_type=6, label=1,
      has_default_value=True, default_value=float(0.3),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='top_k', full_name='tidl_meta_arch.TIDLNmsParam.top_k', index=1,
      number=2, type=5, cpp_type=1, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='eta', full_name='tidl_meta_arch.TIDLNmsParam.eta', index=2,
      number=3, type=2, cpp_type=6, label=1,
      has_default_value=True, default_value=float(1),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=285,
  serialized_end=358,
)


_PRIORBOXPARAMETER = _descriptor.Descriptor(
  name='PriorBoxParameter',
  full_name='tidl_meta_arch.PriorBoxParameter',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='min_size', full_name='tidl_meta_arch.PriorBoxParameter.min_size', index=0,
      number=1, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='max_size', full_name='tidl_meta_arch.PriorBoxParameter.max_size', index=1,
      number=2, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='aspect_ratio', full_name='tidl_meta_arch.PriorBoxParameter.aspect_ratio', index=2,
      number=3, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='flip', full_name='tidl_meta_arch.PriorBoxParameter.flip', index=3,
      number=4, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='clip', full_name='tidl_meta_arch.PriorBoxParameter.clip', index=4,
      number=5, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='variance', full_name='tidl_meta_arch.PriorBoxParameter.variance', index=5,
      number=6, type=2, cpp_type=6, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='img_size', full_name='tidl_meta_arch.PriorBoxParameter.img_size', index=6,
      number=7, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='img_h', full_name='tidl_meta_arch.PriorBoxParameter.img_h', index=7,
      number=8, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='img_w', full_name='tidl_meta_arch.PriorBoxParameter.img_w', index=8,
      number=9, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='step', full_name='tidl_meta_arch.PriorBoxParameter.step', index=9,
      number=10, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='step_h', full_name='tidl_meta_arch.PriorBoxParameter.step_h', index=10,
      number=11, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='step_w', full_name='tidl_meta_arch.PriorBoxParameter.step_w', index=11,
      number=12, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='offset', full_name='tidl_meta_arch.PriorBoxParameter.offset', index=12,
      number=13, type=2, cpp_type=6, label=1,
      has_default_value=True, default_value=float(0.5),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='num_keypoint', full_name='tidl_meta_arch.PriorBoxParameter.num_keypoint', index=13,
      number=14, type=13, cpp_type=3, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=361,
  serialized_end=637,
)


_TIDLODSAVEPARAMS = _descriptor.Descriptor(
  name='TIDLOdSaveParams',
  full_name='tidl_meta_arch.TIDLOdSaveParams',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='output_directory', full_name='tidl_meta_arch.TIDLOdSaveParams.output_directory', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output_name_prefix', full_name='tidl_meta_arch.TIDLOdSaveParams.output_name_prefix', index=1,
      number=2, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output_format', full_name='tidl_meta_arch.TIDLOdSaveParams.output_format', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='label_map_file', full_name='tidl_meta_arch.TIDLOdSaveParams.label_map_file', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name_size_file', full_name='tidl_meta_arch.TIDLOdSaveParams.name_size_file', index=4,
      number=5, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='num_test_image', full_name='tidl_meta_arch.TIDLOdSaveParams.num_test_image', index=5,
      number=6, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='resize_param', full_name='tidl_meta_arch.TIDLOdSaveParams.resize_param', index=6,
      number=7, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=640,
  serialized_end=829,
)


_TIDLODPOSTPROC = _descriptor.Descriptor(
  name='TIDLOdPostProc',
  full_name='tidl_meta_arch.TIDLOdPostProc',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='num_classes', full_name='tidl_meta_arch.TIDLOdPostProc.num_classes', index=0,
      number=1, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='share_location', full_name='tidl_meta_arch.TIDLOdPostProc.share_location', index=1,
      number=2, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=True,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='background_label_id', full_name='tidl_meta_arch.TIDLOdPostProc.background_label_id', index=2,
      number=3, type=5, cpp_type=1, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='nms_param', full_name='tidl_meta_arch.TIDLOdPostProc.nms_param', index=3,
      number=4, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='save_output_param', full_name='tidl_meta_arch.TIDLOdPostProc.save_output_param', index=4,
      number=5, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='code_type', full_name='tidl_meta_arch.TIDLOdPostProc.code_type', index=5,
      number=6, type=14, cpp_type=8, label=1,
      has_default_value=True, default_value=1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='variance_encoded_in_target', full_name='tidl_meta_arch.TIDLOdPostProc.variance_encoded_in_target', index=6,
      number=8, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='keep_top_k', full_name='tidl_meta_arch.TIDLOdPostProc.keep_top_k', index=7,
      number=7, type=5, cpp_type=1, label=1,
      has_default_value=True, default_value=-1,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='confidence_threshold', full_name='tidl_meta_arch.TIDLOdPostProc.confidence_threshold', index=8,
      number=9, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='visualize', full_name='tidl_meta_arch.TIDLOdPostProc.visualize', index=9,
      number=10, type=8, cpp_type=7, label=1,
      has_default_value=True, default_value=False,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='visualize_threshold', full_name='tidl_meta_arch.TIDLOdPostProc.visualize_threshold', index=10,
      number=11, type=2, cpp_type=6, label=1,
      has_default_value=False, default_value=float(0),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='save_file', full_name='tidl_meta_arch.TIDLOdPostProc.save_file', index=11,
      number=12, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='num_keypoint', full_name='tidl_meta_arch.TIDLOdPostProc.num_keypoint', index=12,
      number=13, type=13, cpp_type=3, label=1,
      has_default_value=True, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=832,
  serialized_end=1297,
)


_TIDLMASSD = _descriptor.Descriptor(
  name='TidlMaSsd',
  full_name='tidl_meta_arch.TidlMaSsd',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='box_input', full_name='tidl_meta_arch.TidlMaSsd.box_input', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='class_input', full_name='tidl_meta_arch.TidlMaSsd.class_input', index=1,
      number=2, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output', full_name='tidl_meta_arch.TidlMaSsd.output', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='tidl_meta_arch.TidlMaSsd.name', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='in_width', full_name='tidl_meta_arch.TidlMaSsd.in_width', index=4,
      number=5, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='in_height', full_name='tidl_meta_arch.TidlMaSsd.in_height', index=5,
      number=6, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='score_converter', full_name='tidl_meta_arch.TidlMaSsd.score_converter', index=6,
      number=7, type=14, cpp_type=8, label=1,
      has_default_value=True, default_value=2,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='prior_box_param', full_name='tidl_meta_arch.TidlMaSsd.prior_box_param', index=7,
      number=8, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='detection_output_param', full_name='tidl_meta_arch.TidlMaSsd.detection_output_param', index=8,
      number=9, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1300,
  serialized_end=1612,
)


_TIDLMACAFFESSD = _descriptor.Descriptor(
  name='TidlMaCaffeSsd',
  full_name='tidl_meta_arch.TidlMaCaffeSsd',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='box_input', full_name='tidl_meta_arch.TidlMaCaffeSsd.box_input', index=0,
      number=1, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='class_input', full_name='tidl_meta_arch.TidlMaCaffeSsd.class_input', index=1,
      number=2, type=9, cpp_type=9, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='output', full_name='tidl_meta_arch.TidlMaCaffeSsd.output', index=2,
      number=3, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='name', full_name='tidl_meta_arch.TidlMaCaffeSsd.name', index=3,
      number=4, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='in_width', full_name='tidl_meta_arch.TidlMaCaffeSsd.in_width', index=4,
      number=5, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='in_height', full_name='tidl_meta_arch.TidlMaCaffeSsd.in_height', index=5,
      number=6, type=13, cpp_type=3, label=1,
      has_default_value=False, default_value=0,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='score_converter', full_name='tidl_meta_arch.TidlMaCaffeSsd.score_converter', index=6,
      number=7, type=14, cpp_type=8, label=1,
      has_default_value=True, default_value=2,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='prior_box_param', full_name='tidl_meta_arch.TidlMaCaffeSsd.prior_box_param', index=7,
      number=8, type=11, cpp_type=10, label=3,
      has_default_value=False, default_value=[],
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
    _descriptor.FieldDescriptor(
      name='detection_output_param', full_name='tidl_meta_arch.TidlMaCaffeSsd.detection_output_param', index=8,
      number=9, type=11, cpp_type=10, label=1,
      has_default_value=False, default_value=None,
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1615,
  serialized_end=1932,
)


_TIDLMATFODAPISSD = _descriptor.Descriptor(
  name='TidlMaTfOdApiSsd',
  full_name='tidl_meta_arch.TidlMaTfOdApiSsd',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='tidl_meta_arch.TidlMaTfOdApiSsd.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1934,
  serialized_end=1966,
)


_TIDLMAFASTERRCNN = _descriptor.Descriptor(
  name='TidlMaFasterRcnn',
  full_name='tidl_meta_arch.TidlMaFasterRcnn',
  filename=None,
  file=DESCRIPTOR,
  containing_type=None,
  fields=[
    _descriptor.FieldDescriptor(
      name='name', full_name='tidl_meta_arch.TidlMaFasterRcnn.name', index=0,
      number=1, type=9, cpp_type=9, label=1,
      has_default_value=False, default_value=b"".decode('utf-8'),
      message_type=None, enum_type=None, containing_type=None,
      is_extension=False, extension_scope=None,
      serialized_options=None, file=DESCRIPTOR),
  ],
  extensions=[
  ],
  nested_types=[],
  enum_types=[
  ],
  serialized_options=None,
  is_extendable=False,
  syntax='proto2',
  extension_ranges=[],
  oneofs=[
  ],
  serialized_start=1968,
  serialized_end=2000,
)

_TIDLMETAARCH.fields_by_name['caffe_ssd'].message_type = _TIDLMACAFFESSD
_TIDLMETAARCH.fields_by_name['tf_od_api_ssd'].message_type = _TIDLMATFODAPISSD
_TIDLMETAARCH.fields_by_name['tidl_ssd'].message_type = _TIDLMASSD
_TIDLMETAARCH.fields_by_name['tidl_faster_rcnn'].message_type = _TIDLMAFASTERRCNN
_TIDLODPOSTPROC.fields_by_name['nms_param'].message_type = _TIDLNMSPARAM
_TIDLODPOSTPROC.fields_by_name['save_output_param'].message_type = _TIDLODSAVEPARAMS
_TIDLODPOSTPROC.fields_by_name['code_type'].enum_type = _TIDLBOXCODETYPE
_TIDLMASSD.fields_by_name['score_converter'].enum_type = _TIDLSCORECONVERTER
_TIDLMASSD.fields_by_name['prior_box_param'].message_type = _PRIORBOXPARAMETER
_TIDLMASSD.fields_by_name['detection_output_param'].message_type = _TIDLODPOSTPROC
_TIDLMACAFFESSD.fields_by_name['score_converter'].enum_type = _TIDLSCORECONVERTER
_TIDLMACAFFESSD.fields_by_name['prior_box_param'].message_type = _PRIORBOXPARAMETER
_TIDLMACAFFESSD.fields_by_name['detection_output_param'].message_type = _TIDLODPOSTPROC
DESCRIPTOR.message_types_by_name['TIDLMetaArch'] = _TIDLMETAARCH
DESCRIPTOR.message_types_by_name['TIDLNmsParam'] = _TIDLNMSPARAM
DESCRIPTOR.message_types_by_name['PriorBoxParameter'] = _PRIORBOXPARAMETER
DESCRIPTOR.message_types_by_name['TIDLOdSaveParams'] = _TIDLODSAVEPARAMS
DESCRIPTOR.message_types_by_name['TIDLOdPostProc'] = _TIDLODPOSTPROC
DESCRIPTOR.message_types_by_name['TidlMaSsd'] = _TIDLMASSD
DESCRIPTOR.message_types_by_name['TidlMaCaffeSsd'] = _TIDLMACAFFESSD
DESCRIPTOR.message_types_by_name['TidlMaTfOdApiSsd'] = _TIDLMATFODAPISSD
DESCRIPTOR.message_types_by_name['TidlMaFasterRcnn'] = _TIDLMAFASTERRCNN
DESCRIPTOR.enum_types_by_name['TIDLBoxCodeType'] = _TIDLBOXCODETYPE
DESCRIPTOR.enum_types_by_name['TIDLScoreConverter'] = _TIDLSCORECONVERTER
_sym_db.RegisterFileDescriptor(DESCRIPTOR)

TIDLMetaArch = _reflection.GeneratedProtocolMessageType('TIDLMetaArch', (_message.Message,), {
  'DESCRIPTOR' : _TIDLMETAARCH,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TIDLMetaArch)
  })
_sym_db.RegisterMessage(TIDLMetaArch)

TIDLNmsParam = _reflection.GeneratedProtocolMessageType('TIDLNmsParam', (_message.Message,), {
  'DESCRIPTOR' : _TIDLNMSPARAM,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TIDLNmsParam)
  })
_sym_db.RegisterMessage(TIDLNmsParam)

PriorBoxParameter = _reflection.GeneratedProtocolMessageType('PriorBoxParameter', (_message.Message,), {
  'DESCRIPTOR' : _PRIORBOXPARAMETER,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.PriorBoxParameter)
  })
_sym_db.RegisterMessage(PriorBoxParameter)

TIDLOdSaveParams = _reflection.GeneratedProtocolMessageType('TIDLOdSaveParams', (_message.Message,), {
  'DESCRIPTOR' : _TIDLODSAVEPARAMS,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TIDLOdSaveParams)
  })
_sym_db.RegisterMessage(TIDLOdSaveParams)

TIDLOdPostProc = _reflection.GeneratedProtocolMessageType('TIDLOdPostProc', (_message.Message,), {
  'DESCRIPTOR' : _TIDLODPOSTPROC,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TIDLOdPostProc)
  })
_sym_db.RegisterMessage(TIDLOdPostProc)

TidlMaSsd = _reflection.GeneratedProtocolMessageType('TidlMaSsd', (_message.Message,), {
  'DESCRIPTOR' : _TIDLMASSD,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TidlMaSsd)
  })
_sym_db.RegisterMessage(TidlMaSsd)

TidlMaCaffeSsd = _reflection.GeneratedProtocolMessageType('TidlMaCaffeSsd', (_message.Message,), {
  'DESCRIPTOR' : _TIDLMACAFFESSD,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TidlMaCaffeSsd)
  })
_sym_db.RegisterMessage(TidlMaCaffeSsd)

TidlMaTfOdApiSsd = _reflection.GeneratedProtocolMessageType('TidlMaTfOdApiSsd', (_message.Message,), {
  'DESCRIPTOR' : _TIDLMATFODAPISSD,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TidlMaTfOdApiSsd)
  })
_sym_db.RegisterMessage(TidlMaTfOdApiSsd)

TidlMaFasterRcnn = _reflection.GeneratedProtocolMessageType('TidlMaFasterRcnn', (_message.Message,), {
  'DESCRIPTOR' : _TIDLMAFASTERRCNN,
  '__module__' : 'mmdet_meta_arch_pb2'
  # @@protoc_insertion_point(class_scope:tidl_meta_arch.TidlMaFasterRcnn)
  })
_sym_db.RegisterMessage(TidlMaFasterRcnn)


# @@protoc_insertion_point(module_scope)
