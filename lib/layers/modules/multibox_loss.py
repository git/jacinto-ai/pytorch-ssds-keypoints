# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

# -*- coding: utf-8 -*-
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
from lib.utils.box_utils import match, log_sum_exp
import numpy as np


class MultiBoxLoss(nn.Module):
    """SSD Weighted Loss Function
    Compute Targets:
        1) Produce Confidence Target Indices by matching  ground truth boxes
           with (default) 'priorboxes' that have jaccard index > threshold parameter
           (default threshold: 0.5).
        2) Produce localization target by 'encoding' variance into offsets of ground
           truth boxes and their matched  'priorboxes'.
        3) Hard negative mining to filter the excessive number of negative examples
           that comes with using a large number of default bounding boxes.
           (default negative:positive ratio 3:1)
    Objective Loss:
        L(x,c,l,g) = (Lconf(x, c) + αLloc(x,l,g)) / N
        Where, Lconf is the CrossEntropy Loss and Lloc is the SmoothL1 Loss
        weighted by α which is set to 1 by cross val.
        Args:
            c: class confidences,
            l: predicted boxes,
            g: ground truth boxes
            N: number of matched default boxes
        See: https://arxiv.org/pdf/1512.02325.pdf for more details.
    """

    def __init__(self, cfg, priors, use_gpu=True, codeSize=4, boxes_from_kp = False,num_extra_feature=0,softmax_or_sigmoid=False):
        super(MultiBoxLoss, self).__init__()
        self.use_gpu = use_gpu
        self.num_classes = cfg.NUM_CLASSES
        self.background_label = cfg.BACKGROUND_LABEL
        self.negpos_ratio = cfg.NEGPOS_RATIO
        self.matched_threshold = cfg.MATCHED_THRESHOLD
        self.unmatched_threshold = cfg.UNMATCHED_THRESHOLD
        self.alone_gt_boost_th = cfg.ALONE_GT_BOOST_TH
        self.variance = cfg.VARIANCE
        self.priors = priors
        self.boxes_from_kp = boxes_from_kp
        self.mimic_box_loss = False # this can be false as well when boxes from kp. It is a choice.
        if self.mimic_box_loss == True:
            self.codeSize = codeSize + 4 # additional four parameter for box because box parameter is mimiced from kps
        else:
            self.codeSize = codeSize
        self.num_extra_feature = num_extra_feature

        self.softmax_or_sigmoid = softmax_or_sigmoid

        self.polygon_match = cfg.POLYGON_MATCH

        pytorch_ver_0_4 = True

        if pytorch_ver_0_4 == True:
            # This flow is only used for import tool for model migration from pytorch to caffe. As mmdnn tool supports only 0.4.0 pytorch version and so pytorch model need to be saved with 0.4.0 version
            if softmax_or_sigmoid == True:
                self.confidence_loss = nn.CrossEntropyLoss(size_average=False)
            else:
                self.confidence_loss = nn.BCEWithLogitsLoss(size_average=False)
        else:
            if softmax_or_sigmoid == True:
                self.confidence_loss = nn.CrossEntropyLoss(reduction='sum')
            else:
                self.confidence_loss = nn.BCEWithLogitsLoss(reduction='sum')

    def dump_torch_data(self, data, file_name):
        x = data.data.cpu().numpy()
        np.save(file_name, x)

    def forward(self, predictions, targets):
        """Multibox Loss
        Args:
            predictions (tuple): A tuple containing loc preds, conf preds,
            and prior boxes from SSD net.
                conf shape: torch.size(batch_size,num_priors,num_classes)
                loc shape: torch.size(batch_size,num_priors,4)
                priors shape: torch.size(num_priors,4)
            ground_truth (tensor): Ground truth boxes and labels for a batch,
                shape: [batch_size,num_objs,5] (last idx is the label).
        """
        loc_data, conf_data = predictions

        #self.dump_torch_data(conf_data,'conf_data.npy')

        num = loc_data.size(0)
        priors = self.priors
        # priors = priors[:loc_data.size(1), :]
        num_priors = (priors.size(0))
        num_classes = self.num_classes

        # for coco data set of key points, there are 17 kps. code size is 17*2+4(box).
        # however the visibility flag 'v' is present in target after 17*2+4(bbox)+1(class) elements
        # for other scenario, extra_truth_elm should be zero
        if self.boxes_from_kp == False:
            num_extra_truth_elm =targets[0].shape[1] - (self.codeSize + 1)
        else:
            #in this scenario 4 box parameter is not part of code size
            num_extra_truth_elm = targets[0].shape[1] - (self.codeSize + 4 + 1)

        # It is assumed that num_extra_truth_elm is for key points and extra features
        # hence box parameters are given weight of 1
        if num_extra_truth_elm > 0:
            extra_truth_elm = torch.Tensor(num, num_priors, self.codeSize)
            extra_truth_elm = Variable(extra_truth_elm, requires_grad=False)


        if self.mimic_box_loss == True:
            # inserting 4 columns at start for xmin,ymin,xmax,ymax, to mimic the box parameter from key points
            # order of insertion is ymax,xmax,ymin,xmin
            loc_data = torch.cat((torch.max(loc_data[:,:,1::2], dim=2,keepdim=True)[0], loc_data), dim=2)
            loc_data = torch.cat((torch.max(loc_data[:,:,1::2], dim=2,keepdim=True)[0], loc_data), dim=2)
            loc_data = torch.cat((torch.min(loc_data[:,:,3::2], dim=2,keepdim=True)[0], loc_data), dim=2)
            loc_data = torch.cat((torch.min(loc_data[:,:,3::2], dim=2,keepdim=True)[0], loc_data), dim=2)

            loc_data_cx = (loc_data[:,:,0] + loc_data[:,:,2])/2 # cx = (xmin + xmax)/2
            loc_data_cy = (loc_data[:,:,1] + loc_data[:,:,3]) / 2  # cy = (ymin + ymax)/2
            loc_data[:, :, 2] = (loc_data[:,:,2] - loc_data[:,:,0]) # w = xmax - xmin
            loc_data[:, :, 3] = (loc_data[:,:,3] - loc_data[:,:,1]) # h = ymax - ymin
            loc_data[:, :, 0] = loc_data_cx
            loc_data[:, :, 1] = loc_data_cy

        # match priors (default boxes) and ground truth boxes
        loc_t = torch.Tensor(num, num_priors, self.codeSize)
        conf_t = torch.Tensor(num, num_priors)
        for idx in range(num):
            truths = targets[idx][:,:-(1 + num_extra_truth_elm)].data
            labels = targets[idx][:,-(1 + num_extra_truth_elm)].data
            defaults = priors.data

            # check if the current idx has any ground truth or not
            if truths.shape[0] != 0:
                best_truth_idx=match(self.matched_threshold, self.unmatched_threshold,
                                     truths, defaults, self.variance, labels, loc_t, conf_t,
                                     idx, (self.boxes_from_kp and (self.mimic_box_loss == False)),
                                     numextrafeatures=self.num_extra_feature,
                                     alone_gt_boost_th=self.alone_gt_boost_th,
                                     polygon_match=self.polygon_match)
            else:
                conf_t[idx] = 0

            if num_extra_truth_elm > 0:
                extra_truth_elm[idx][:, 0] = 1.0
                extra_truth_elm[idx][:, 1] = 1.0
                extra_truth_elm[idx][:, 2] = 1.0
                extra_truth_elm[idx][:, 3] = 1.0
                extra_truth_elm[idx][:, 4::2] = targets[idx][best_truth_idx,(self.codeSize + 1):].data
                extra_truth_elm[idx][:, 5::2] = targets[idx][best_truth_idx, (self.codeSize + 1):].data

        if self.use_gpu:
            loc_t = loc_t.cuda()
            conf_t = conf_t.cuda()

            if num_extra_truth_elm > 0:
                extra_truth_elm = extra_truth_elm.cuda()

        # wrap targets
        loc_t = Variable(loc_t, requires_grad=False)
        conf_t = Variable(conf_t,requires_grad=False)

        # all the position having class other than background
        # 0 - 0.1- 1 - 2 - 3 ... 0 -> negative, 1,2,3 -> positive, 0.1,1,2,3 -> non negative
        pos = conf_t >= 1

        # num_pos = pos.sum()
        # only negative class is zero, other than that anything is non negative
        non_neg = conf_t > 0

        #neg = conf_t == 0

        # after this ambiguity state(0.1) is treated as 0 ie background/negative
        conf_t = conf_t.long()

        # Localization Loss (Smooth L1)
        # Shape: [batch,num_priors,4]
        pos_idx = pos.unsqueeze(pos.dim()).expand_as(loc_data)
        loc_p = loc_data[pos_idx].view(-1,self.codeSize)
        loc_t = loc_t[pos_idx].view(-1,self.codeSize)
        if num_extra_truth_elm > 0:
            extra_truth_elm = extra_truth_elm[pos_idx].view(-1,self.codeSize)

        if num_extra_truth_elm > 0:
            loc_p = loc_p * extra_truth_elm
            loc_t = loc_t * extra_truth_elm

        enable_split_loss=False
        if self.num_extra_feature>0 and enable_split_loss==True:
            loss_la = F.smooth_l1_loss(loc_p[:,:-self.num_extra_feature], loc_t[:,:-self.num_extra_feature], size_average=False)
            loss_lb = F.smooth_l1_loss(loc_p[:,-self.num_extra_feature:], loc_t[:,-self.num_extra_feature:], size_average=False)
            loss_l=loss_la+2*loss_lb
        elif self.num_extra_feature>0 and enable_split_loss==False:
            # In this scenario loss_lb is not used for back propagation. It is just used for logging at console
            # make prediction zero whereever kp gt is 0.0
            #loc_p[(loc_t[:,:-self.num_extra_feature] == 0.0).nonzero()] = 0.0
            loss_l = F.smooth_l1_loss(loc_p, loc_t, size_average=False)
            loss_lb= F.smooth_l1_loss(loc_p[:,-self.num_extra_feature:], loc_t[:,-self.num_extra_feature:], size_average=False)
        else :
            loss_l = F.smooth_l1_loss(loc_p, loc_t, size_average=False)
            loss_lb=0

        # Compute max conf across batch for hard negative mining
        batch_conf = conf_data.view(-1, self.num_classes)
        softmax_score = log_sum_exp(batch_conf) - batch_conf.gather(1, conf_t.view(-1,1))

        # Hard Negative Mining
        non_neg1 = non_neg.view(-1, 1)

        # print('pos shape', pos.size(), '\n')
        softmax_score[non_neg1] = 0 # filter out non negative boxes for now
        softmax_score = softmax_score.view(num, -1)
        _,loss_idx = softmax_score.sort(1, descending=True)
        _,idx_rank = loss_idx.sort(1)
        num_pos = pos.long().sum(1,keepdim=True) #new sum needs to keep the same dim
        num_neg = torch.clamp(self.negpos_ratio*num_pos, max=pos.size(1)-1)
        neg = idx_rank < num_neg.expand_as(idx_rank)

        # Confidence Loss Including Positive and Negative Examples
        pos_idx = pos.unsqueeze(2).expand_as(conf_data)
        neg_idx = neg.unsqueeze(2).expand_as(conf_data)
        conf_p = conf_data[(pos_idx+neg_idx).gt(0)].view(-1,self.num_classes)
        targets_weighted = conf_t[(pos+neg).gt(0)]

        if self.softmax_or_sigmoid == False:
            targets_weighted = F.one_hot(targets_weighted,num_classes=self.num_classes)
            targets_weighted = targets_weighted.float()

        loss_c = self.confidence_loss(conf_p, targets_weighted)

        # Sum of losses: L(x,c,l,g) = (Lconf(x, c) + αLloc(x,l,g)) / N

        N = num_pos.data.sum().float()
        loss_l/=N
        loss_c/=N
        loss_lb/=N
        return loss_l,loss_c,loss_lb
