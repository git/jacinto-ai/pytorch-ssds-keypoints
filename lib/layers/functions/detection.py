# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Our implementation is based on work : https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import torch
import torch.nn as nn
import torch.backends.cudnn as cudnn
from torch.autograd import Function
from torch.autograd import Variable
from lib.utils.box_utils import decode,nms
# from lib.utils.nms.nms_wrapper import nms
from lib.utils.timer import Timer
import numpy as np

class Detect(Function):
    """At test time, Detect is the final layer of SSD.  Decode location preds,
    apply non-maximum suppression to location predictions based on conf
    scores and threshold to a top_k number of output predictions for both
    confidence score and locations.
    """
    def __init__(self, cfg, priors, numKeyPoints=4,num_extra_feature=0, boxes_from_kp=False, tag_det_prior_box_id=False,poly_nms=False, inter_class_nms=False):
        self.num_classes = cfg.NUM_CLASSES
        self.background_label = cfg.BACKGROUND_LABEL
        self.conf_thresh =cfg.SCORE_THRESHOLD
        self.nms_thresh = cfg.IOU_THRESHOLD
        self.viz_threshold  = cfg.VIZ_THRESHOLD
        self.top_k = cfg.MAX_DETECTIONS 
        self.variance = cfg.VARIANCE
        self.priors = priors
        self.boxes_from_kp = boxes_from_kp
        self.num_keypoints = numKeyPoints
        self.tag_det_prior_box_id = tag_det_prior_box_id
        self.poly_nms = poly_nms
        self.inter_class_nms = inter_class_nms

        if boxes_from_kp == True:
          self.box_prm_size = 0
        else:
          self.box_prm_size = 4

        self.num_extra_feature = num_extra_feature

        if self.tag_det_prior_box_id == True:
            self.num_extra_det_prm = 1 # in this flow , each detection is tagged with prior box id and that is treated as extra feature
        else:
            self.num_extra_det_prm = 0

        self.codeSize = self.box_prm_size + 2*self.num_keypoints + self.num_extra_det_prm + self.num_extra_feature

    # def forward(self, predictions, prior):
    #     """
    #     Args:
    #         loc_data: (tensor) Loc preds from loc layers
    #             Shape: [batch,num_priors*4]
    #         conf_data: (tensor) Shape: Conf preds from conf layers
    #             Shape: [batch*num_priors,num_classes]
    #         prior_data: (tensor) Prior boxes and variances from priorbox layers
    #             Shape: [1,num_priors,4]
    #     """
    #     loc, conf = predictions

    #     loc_data = loc.data
    #     conf_data = conf.data
    #     prior_data = prior.data

    #     num = loc_data.size(0)  # batch size
    #     num_priors = prior_data.size(0)
    #     self.boxes = torch.zeros(1, num_priors, 4)
    #     self.scores = torch.zeros(1, num_priors, self.num_classes)

    #     if num == 1:
    #         # size batch x num_classes x num_priors
    #         conf_preds = conf_data.unsqueeze(0)

    #     else:
    #         conf_preds = conf_data.view(num, num_priors,
    #                                     self.num_classes)
    #         self.boxes.expand_(num, num_priors, 4)
    #         self.scores.expand_(num, num_priors, self.num_classes)

    #     # Decode predictions into bboxes.
    #     for i in range(num):
    #         decoded_boxes = decode(loc_data[i], prior_data, self.variance)
    #         # For each class, perform nms
    #         conf_scores = conf_preds[i].clone()
    #         '''
    #         c_mask = conf_scores.gt(self.thresh)
    #         decoded_boxes = decoded_boxes[c_mask]
    #         conf_scores = conf_scores[c_mask]
    #         '''

    #         conf_scores = conf_preds[i].clone()
    #         num_det = 0
    #         for cl in range(1, self.num_classes):
    #             c_mask = conf_scores[cl].gt(self.conf_thresh)
    #             scores = conf_scores[cl][c_mask]
    #             if scores.dim() == 0:
    #                 continue
    #             l_mask = c_mask.unsqueeze(1).expand_as(decoded_boxes)
    #             boxes = decoded_boxes[l_mask].view(-1, 4)
    #             ids, count = nms(boxes, scores, self.nms_thresh, self.top_k)
    #             self.output[i, cl, :count] = \
    #                 torch.cat((scores[ids[:count]].unsqueeze(1),
    #                            boxes[ids[:count]]), 1)

    #     return self.output


    def forward(self, predictions):
        """
        Args:
            loc_data: (tensor) Loc preds from loc layers
                Shape: [batch,num_priors*4]
            conf_data: (tensor) Shape: Conf preds from conf layers
                Shape: [batch*num_priors,num_classes]
            prior_data: (tensor) Prior boxes and variances from priorbox layers
                Shape: [1,num_priors,4]
        """
        loc, conf = predictions

        extra_feature = loc[:,:,self.codeSize - (self.num_extra_feature):]
        loc = loc[:,:,:self.codeSize - (self.num_extra_feature+ self.num_extra_det_prm)]

        loc_data = loc.data
        conf_data = conf.data
        prior_data = self.priors.data

        num = loc_data.size(0)  # batch size
        num_priors = prior_data.size(0)
        #self.output.zero_()
        if num == 1:
            # size batch x num_classes x num_priors
            conf_preds = conf_data.t().contiguous().unsqueeze(0)
        else:
            conf_preds = conf_data.view(num, num_priors,
                                        self.num_classes).transpose(2, 1)
            #self.output.expand_(num, self.num_classes, self.top_k, 5)

        if self.boxes_from_kp == True:
            output = torch.zeros(num, self.num_classes, self.top_k, 4 + self.codeSize+1)
        else:
            output = torch.zeros(num, self.num_classes, self.top_k, self.codeSize + 1)

        _t = {'decode': Timer(), 'misc': Timer(), 'box_mask':Timer(), 'score_mask':Timer(),'nms':Timer(), 'cpu':Timer(),'sort':Timer()}
        gpunms_time = 0
        scores_time=0
        box_time=0
        cpu_tims=0
        sort_time=0
        decode_time=0
        _t['misc'].tic()
        # Decode predictions into bboxes.
        for i in range(num):

            _t['decode'].tic()
            decoded_boxes = decode(loc_data[i], prior_data, self.variance, self.num_keypoints, self.boxes_from_kp)
            decode_time += _t['decode'].toc()
            # For each class, perform nms
            conf_scores = conf_preds[i].clone()
            num_det = 0
            class_count = []
            for cl in range(1, self.num_classes):
                _t['cpu'].tic()
                c_mask = conf_scores[cl].gt(self.conf_thresh).nonzero().view(-1)
                cpu_tims+=_t['cpu'].toc()
                if c_mask.dim() == 0:
                    continue
                _t['score_mask'].tic()
                scores = conf_scores[cl][c_mask]
                scores_time+=_t['score_mask'].toc()
                if scores.dim() == 0:
                    continue
                _t['box_mask'].tic()
                # l_mask = c_mask.unsqueeze(1).expand_as(decoded_boxes)
                # boxes = decoded_boxes[l_mask].view(-1, 4)
                boxes = decoded_boxes[c_mask, :]
                if self.num_extra_feature>0:
                    extra_feature_mask =  extra_feature[i][c_mask,:]
                box_time+=_t['box_mask'].toc()
                # idx of highest scoring and non-overlapping boxes per class
                _t['nms'].tic()
                # cls_dets = torch.cat((boxes, scores), 1)
                # _, order = torch.sort(scores, 0, True)
                # cls_dets = cls_dets[order]
                # keep = nms(cls_dets, self.nms_thresh)
                # cls_dets = cls_dets[keep.view(-1).long()]
                #ids, count = nms(boxes, scores, self.nms_thresh, self.top_k)

                if len(boxes) > 1:
                  ids, count = nms(boxes[:, :self.box_prm_size + 2*self.num_keypoints], scores, self.nms_thresh, self.top_k, self.viz_threshold, self.poly_nms)
                elif  len(boxes) == 1:
                  ids, count = nms(boxes[:self.box_prm_size + 2*self.num_keypoints], scores, self.nms_thresh, self.top_k, self.viz_threshold, self.poly_nms)
                else:
                  count = 0
                  ids = []

                gpunms_time += _t['nms'].toc()
                class_count.append(count)
                if len(ids) > 0:
                    if self.num_extra_feature>0 :
                        if self.tag_det_prior_box_id == True:
                            output[i, cl, :count] = \
                                torch.cat((scores[ids[:count]].unsqueeze(1),
                                           boxes[ids[:count]],c_mask[ids[:count]].float().unsqueeze(1), extra_feature_mask[ids[:count]]), 1)
                        else:
                            output[i, cl, :count] = \
                                torch.cat((scores[ids[:count]].unsqueeze(1),
                                           boxes[ids[:count]],extra_feature_mask[ids[:count]]), 1)
                    else:
                        if self.tag_det_prior_box_id == True:
                            output[i, cl, :count] = \
                                torch.cat((scores[ids[:count]].unsqueeze(1),
                                           boxes[ids[:count]],c_mask[ids[:count]].float().unsqueeze(1)), 1)
                        else:
                            output[i, cl, :count] = \
                                torch.cat((scores[ids[:count]].unsqueeze(1),
                                           boxes[ids[:count]]), 1)
            if self.inter_class_nms == True:
                ids, count = nms(output[i, :, :, 1:self.box_prm_size + 2*self.num_keypoints].view(-1,self.box_prm_size + 2*self.num_keypoints),
                                scores[i, :, :, 0].view(-1,1), self.nms_thresh,
                                 self.top_k*self.num_classes, self.viz_threshold, self.poly_nms)
                for cl in range(1, self.num_classes):
                    cur_cl_valid_ids = ids < class_count[cl]
                    cur_cl_count = cur_cl_valid_ids.sum()
                    output[i, cl, :cur_cl_count, 0] = output[i, cl, :cur_cl_valid_ids, 0][cur_cl_valid_ids]
                    output[i, cl, cur_cl_count:class_count[cl], 0] = 0 # clear out previous scores
                    output[i, cl, :cur_cl_count, cccc] = output[i, cl, :cur_cl_valid_ids, 1:self.codeSize][cur_cl_valid_ids]



        nms_time= _t['misc'].toc()
        # print(nms_time, cpu_tims, scores_time,box_time,gpunms_time)
        # flt = self.output.view(-1, 5)
        # _, idx = flt[:, 0].sort(0)
        # _, rank = idx.sort(0)
        # flt[(rank >= self.top_k).unsqueeze(1).expand_as(flt)].fill_(0)
        return output
