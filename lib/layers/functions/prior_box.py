# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Our implementation is based on work : https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from __future__ import division
import torch
from math import sqrt as sqrt
from itertools import product as product
import numpy as np

class PriorBox(object):
    """Compute priorbox coordinates in center-offset form for each source
    feature map.
    """
    def __init__(self, image_size, feature_maps, aspect_ratios, scale, archor_stride=None, archor_offest=None, clip=True, head0_ar1_sksk1_scale = 0.0):
        super(PriorBox, self).__init__()
        self.image_size = image_size #[height, width]
        self.feature_maps = feature_maps #[(height, width), ...]
        self.aspect_ratios = aspect_ratios
        # number of priors for feature map location (either 4 or 6)
        self.num_priors = len(aspect_ratios)
        self.clip = clip
        self.head0_ar1_sksk1_scale = head0_ar1_sksk1_scale
        # scale value
        if isinstance(scale[0], list):
            # get min of the result
            self.scales = [min(s[0] / self.image_size[0], s[1] / self.image_size[1]) for s in scale]
        elif isinstance(scale[0], float) and len(scale) == 2:
            num_layers = len(feature_maps)
            min_scale, max_scale = scale
            self.scales = [min_scale + (max_scale - min_scale) * i / (num_layers - 1) for i in range(num_layers)] + [1.0]
        elif isinstance(scale, list): # raw list of scales
            self.scales = scale

        if archor_stride:
            self.steps = [(steps[0] / self.image_size[0], steps[1] / self.image_size[1]) for steps in archor_stride] 
        else:
            self.steps = [[1/f_h, 1/f_w] for f_h, f_w in feature_maps]

        if archor_offest:
            self.offset = [[offset[0] / self.image_size[0], offset[1] * self.image_size[1]] for offset in archor_offest] 
        else:
            self.offset = [[steps[0] * 0.5, steps[1] * 0.5] for steps in self.steps] 

    def forward(self):
        mean = []
        mean_map = np.zeros((0,4))
        # l = 0
        en_print_anchor_box = True

        for k, f in enumerate(self.feature_maps):
            if (en_print_anchor_box == True):
                cur_line_print = str(f[1]) + ' ' + str(f[0]) + ' '
            for i, j in product(range(f[0]), range(f[1])):
                cx = j * self.steps[k][1] + self.offset[k][1]
                cy = i * self.steps[k][0] + self.offset[k][0]
                s_k = self.scales[k]
                s_k_y = s_k * (self.image_size[1]/self.image_size[0])

                if (en_print_anchor_box == True) and (i == 0) and (j == 0):
                    cur_line_print += "{:6.2f}".format(self.steps[k][1] * self.image_size[1]) + ' ' + "{:6.2f}".format(self.steps[k][0] * self.image_size[0]) + ' '
                    #print(f'Step sizes in x & y direction are {self.steps[k][1] * self.image_size[1]} , {self.steps[k][0] * self.image_size[0]}')

                # rest of aspect ratios
                for ar in self.aspect_ratios[k]:
                    #if isinstance(ar, int):
                        if ar == 1:
                            # aspect_ratio: 1 Min size
                            mean += [cx, cy, s_k, s_k_y]
                            mean_map = np.append(mean_map, [[cx*self.image_size[1], cy*self.image_size[0], k, 1.0]], axis=0)

                            if (en_print_anchor_box == True) and (i == 0) and (j == 0):
                                cur_line_print += "{:6.2f}".format(
                                    s_k * self.image_size[1]) + ' ' + "{:6.2f}".format(
                                    s_k_y * self.image_size[0]) + ' '

                                #print(f'ar=1 Box size is {s_k * self.image_size[1]} , {s_k_y * self.image_size[0]}')

                            # aspect_ratio: 1 Max size
                            # rel size: sqrt(s_k * s_(k+1))
                            s_k_prime = sqrt(s_k * self.scales[k+1])

                            if self.head0_ar1_sksk1_scale != 0.0 and k == 0:
                              s_k_prime = self.head0_ar1_sksk1_scale

                            s_k_prime_y = s_k_prime * (self.image_size[1] / self.image_size[0])

                            mean += [cx, cy, s_k_prime, s_k_prime_y]
                            mean_map = np.append(mean_map,[[cx*self.image_size[1], cy*self.image_size[0], k, 1.1]], axis=0) # another ar =1, is denoted by 1.1

                            if (en_print_anchor_box == True) and (i == 0) and (j == 0):
                                cur_line_print += "{:6.2f}".format(
                                    s_k_prime * self.image_size[1]) + ' ' + "{:6.2f}".format(
                                    s_k_prime_y * self.image_size[0]) + ' '

                                #print(f'ar=1 Box size is {s_k_prime * self.image_size[1]} , {s_k_prime_y * self.image_size[0]}')
                        else:
                            ar_sqrt = sqrt(ar)
                            mean += [cx, cy, s_k*ar_sqrt, s_k_y/ar_sqrt]
                            mean += [cx, cy, s_k/ar_sqrt, s_k_y*ar_sqrt]

                            mean_map = np.append(mean_map, [[cx*self.image_size[1], cy*self.image_size[0], k, ar]], axis=0)
                            mean_map = np.append(mean_map, [[cx*self.image_size[1], cy*self.image_size[0], k, 1/ar]], axis=0)

                            if (en_print_anchor_box == True) and (i == 0) and (j == 0):

                                cur_line_print += "{:6.2f}".format(
                                    (s_k * ar_sqrt) * self.image_size[1]) + ' ' + "{:6.2f}".format(
                                    (s_k_y / ar_sqrt) * self.image_size[0]) + ' '

                                cur_line_print += "{:6.2f}".format(
                                    (s_k / ar_sqrt) * self.image_size[1]) + ' ' + "{:6.2f}".format(
                                    (s_k_y * ar_sqrt) * self.image_size[0]) + ' '

                                #print(f'ar={ar} Box size is {(s_k * ar_sqrt) * self.image_size[1]} , {(s_k_y / ar_sqrt) * self.image_size[0]}')
                                #print(f'ar={ar} Box size is {(s_k / ar_sqrt) * self.image_size[1]} , {(s_k_y * ar_sqrt) * self.image_size[0]}')

            print(cur_line_print)

                    #elif isinstance(ar, list):
                     #   mean += [cx, cy, s_k*ar[0], s_k*ar[1]]
        #     print(f, self.aspect_ratios[k])
        # assert False
        # back to torch land
        output = torch.Tensor(mean).view(-1, 4)

        if self.clip:
            output.clamp_(max=1, min=0)

        return (output,mean_map)
