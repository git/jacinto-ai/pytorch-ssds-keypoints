# loging of the file based on image file name, and collecting various statistics
import numpy as np
import glob, os
import sys, getopt
import csv
from dataset.voc_eval import voc_eval

def print_help():
  print ('Calculates pascal voc accuracy for given detection and ground truth')
  print ('python calc_pascal_voc_acc.py -i <detection file> -g <ground truth files> -n <input file names> -d <detection thresold> -u <iou threshold>')
#################################################################################
try:
  argv = sys.argv[1:]
  opts, args = getopt.getopt(argv,"hi:g:n:d:u:",["ipDetFile=",  "ipGroundTruthFolder=", "ipFileNames=", "ipDetTh=", "ipIOUTh="])


except getopt.GetoptError:
  print("\n\n**Something went wrong in passing the arguments**\n\n")
  print ("argv: ", argv)
  print ('syntax error')
  print_help()
  sys.exit(2)
###################################################################################    
class Params:
  #Class for Params
  def __init__(self):
    self.ipDetFile = ''
    self.ipGroundTruthFolder = ''
    self.ipFileNames = 'names.txt'
    self.ipDetTh = 0.5
    self.ipIOUTh = 0.5
    self.ipNumClasses = 2
    

  def displayArgs(self):
    print ("==================================")
    print ("ipDetFile    " , self.ipDetFile)
    print ("ipGroundTruthFolder    " , self.ipGroundTruthFolder)
    print ("ipFileNames    " , self.ipFileNames)
    print ("ipDetTh    " , self.ipDetTh)
    print ("ipIOUTh    " , self.ipIOUTh)
    print ("ipNumClasses    " , self.ipNumClasses)
    print ("==================================")

###################################################################################    
params=Params()

for opt, arg in opts:
  if opt == '-h':
    print_help()
    sys.exit()
  elif opt in ("-i", "--ipDetFile"):
    params.ipDetFile = arg
  elif opt in ("-g", "--ipGroundTruthFolder"):
    params.ipGroundTruthFolder = arg
  elif opt in ("-n", "--ipFileNames"):
    params.ipFileNames = arg
  elif opt in ("-n", "--ipDetTh"):
    params.ipDetTh = float(arg)
  elif opt in ("-u", "--ipIOUTh"):
    params.ipIOUTh = float(arg)
  else:
    print('Unknown parameter is detected')
    
params.displayArgs()
CLASSES = ['__background__','parking_empty', 'parking_occupied']
aps = []
cachedir = os.path.join('./', 'annotations_cache')
NUM_KEYPOINTS=4

for i, cls in enumerate(CLASSES):
  if cls == '__background__':
      continue

  rec, prec, ap, kp_err, num_boxes = voc_eval(
                          params.ipDetFile+cls+'.txt', params.ipGroundTruthFolder+'/%s.xml', params.ipFileNames, cls, cachedir, ovthresh=0.5,
                          use_07_metric=True, num_keypoints=NUM_KEYPOINTS)
  aps += [ap]
  print('AP for {} = {:.4f}'.format(cls, ap))
  if num_boxes > 0:
      print('kp_error',kp_err/num_boxes)
  else:
      print('kp_error', 50)

print('Mean AP = {:.4f}'.format(np.mean(aps)))
print('~~~~~~~~')
print('Results:')
for ap in aps:
    print('{:.3f}'.format(ap))
print('{:.3f}'.format(np.mean(aps)))
print('~~~~~~~~')
print('')
print('--------------------------------------------------------------')
print('Results computed with the **unofficial** Python eval code.')
print('Results should be very close to the official MATLAB eval code.')
print('Recompute with `./tools/reval.py --matlab ...` for your paper.')
print('-- Thanks, The Management')
print('--------------------------------------------------------------')


