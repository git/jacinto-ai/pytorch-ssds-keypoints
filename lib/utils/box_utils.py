import torch
import torch.nn as nn
import math
import numpy as np
from shapely.geometry import Polygon

if torch.cuda.is_available():
    import torch.backends.cudnn as cudnn
    torch.set_default_tensor_type('torch.cuda.FloatTensor')


def point_form(boxes):
    """ Convert prior_boxes to (xmin, ymin, xmax, ymax)
    representation for comparison to point form ground truth data.
    Args:
        boxes: (tensor) center-size default boxes from priorbox layers.
    Return:
        boxes: (tensor) Converted xmin, ymin, xmax, ymax form of boxes.
    """
    return torch.cat((boxes[:, :2] - boxes[:, 2:]/2,     # xmin, ymin
                     boxes[:, :2] + boxes[:, 2:]/2), 1)  # xmax, ymax


def center_size(boxes):
    """ Convert prior_boxes to (cx, cy, w, h)
    representation for comparison to center-size form ground truth data.
    Args:
        boxes: (tensor) point_form boxes
    Return:
        boxes: (tensor) Converted xmin, ymin, xmax, ymax form of boxes.
    """
    return torch.cat((boxes[:, 2:] + boxes[:, :2])/2,  # cx, cy
                     boxes[:, 2:] - boxes[:, :2], 1)  # w, h


def intersect(box_a, box_b):
    """ We resize both tensors to [A,B,2] without new malloc:
    [A,2] -> [A,1,2] -> [A,B,2]
    [B,2] -> [1,B,2] -> [A,B,2]
    Then we compute the area of intersect between box_a and box_b.
    Args:
      box_a: (tensor) bounding boxes, Shape: [A,4].
      box_b: (tensor) bounding boxes, Shape: [B,4].
    Return:
      (tensor) intersection area, Shape: [A,B].
    """
    A = box_a.size(0)
    B = box_b.size(0)
    max_xy = torch.min(box_a[:, 2:4].unsqueeze(1).expand(A, B, 2),
                       box_b[:, 2:4].unsqueeze(0).expand(A, B, 2))
    min_xy = torch.max(box_a[:, :2].unsqueeze(1).expand(A, B, 2),
                       box_b[:, :2].unsqueeze(0).expand(A, B, 2))
    inter = torch.clamp((max_xy - min_xy), min=0)
    return inter[:, :, 0] * inter[:, :, 1]


def jaccard(box_a, box_b):
    """Compute the jaccard overlap of two sets of boxes.  The jaccard overlap
    is simply the intersection over union of two boxes.  Here we operate on
    ground truth boxes and default boxes.
    E.g.:
        A ∩ B / A ∪ B = A ∩ B / (area(A) + area(B) - A ∩ B)
    Args:
        box_a: (tensor) Ground truth bounding boxes, Shape: [num_objects,4]
        box_b: (tensor) Prior boxes from priorbox layers, Shape: [num_priors,4]
    Return:
        jaccard overlap: (tensor) Shape: [box_a.size(0), box_b.size(0)]
    """
    inter = intersect(box_a, box_b)
    area_a = ((box_a[:, 2]-box_a[:, 0]) *
              (box_a[:, 3]-box_a[:, 1])).unsqueeze(1).expand_as(inter)  # [A,B]
    area_b = ((box_b[:, 2]-box_b[:, 0]) *
              (box_b[:, 3]-box_b[:, 1])).unsqueeze(0).expand_as(inter)  # [A,B]
    union = area_a + area_b - inter
    return inter / union , inter # [A,B]

def intersect_kp(box_a, box_b, iou_corse,inter_corse,poly_iou_calc_rect_iou_th=0.5):
    """ We resize both tensors to [A,B,2] without new malloc:
    [A,2] -> [A,1,2] -> [A,B,2]
    [B,2] -> [1,B,2] -> [A,B,2]
    Then we compute the area of intersect between box_a and box_b.
    Args:
      box_a: (tensor) bounding boxes, Shape: [A,4].
      box_b: (tensor) bounding boxes, Shape: [B,4].
    Return:
      (tensor) intersection area, Shape: [A,B].
    """
    A = box_a.size(0)
    B = box_b.size(0)

    np_truth = box_a.cpu().numpy()
    np_anchor_box = box_b.cpu().numpy()
    np_iou_corse = iou_corse.cpu().numpy()
    np_inter = inter_corse.cpu().numpy()

    for i in range(A):
        polygon_truth = Polygon(np.reshape(np_truth[i,:],(-1,2))) # 4 point polygon
        #print(np.reshape(np_truth[i,4:],(4,2)))
        for j in range(B):
            if (np_iou_corse[i,j] >=poly_iou_calc_rect_iou_th) and polygon_truth.is_valid:
                polygon_anchor_box = Polygon([[np_anchor_box[j,0],np_anchor_box[j,1]],[np_anchor_box[j,0],np_anchor_box[j,3]],[np_anchor_box[j,2],np_anchor_box[j,3]],[np_anchor_box[j,2],np_anchor_box[j,1]]])
                #print([[np_anchor_box[j,0],np_anchor_box[j,1]],[np_anchor_box[j,0],np_anchor_box[j,3]],[np_anchor_box[j,2],np_anchor_box[j,3]],[np_anchor_box[j,2],np_anchor_box[j,1]]])
                new_intersect = polygon_anchor_box.intersection(polygon_truth).area
                np_inter[i, j] = new_intersect

    return torch.tensor(np_inter, dtype=torch.float)

def jaccard_kp(box_a, box_b, poly_iou_calc_rect_iou_th=0.4):
    """Compute the jaccard overlap of two sets of boxes.  The jaccard overlap
    is simply the intersection over union of two boxes.  Here we operate on
 )   ground truth boxes and default boxes.
    E.g.:
        A ∩ B / A ∪ B = A ∩ B / (area(A) + area(B) - A ∩ B)
    Args:
        box_a: (tensor) Ground truth bounding boxes, Shape: [num_objects,4]
        box_b: (tensor) Prior boxes from priorbox layers, Shape: [num_priors,4]
    Return:
        jaccard overlap: (tensor) Shape: [box_a.size(0), box_b.size(0)]
    """

    iou_corse, inter_corse = jaccard(box_a[:,:4], box_b)

    area_a_rect = ((box_a[:, 2]-box_a[:, 0]) *
                  (box_a[:, 3]-box_a[:, 1])).unsqueeze(1).expand_as(inter_corse)  # [A,B]

    inter = intersect_kp(box_a[:,4:], box_b, iou_corse,inter_corse,poly_iou_calc_rect_iou_th)

    box_a = torch.cat((box_a[:,4:], box_a[:, 4:6]), dim=1)

    mask = iou_corse >= poly_iou_calc_rect_iou_th

    area_a_poly = (torch.abs(torch.sum(box_a[:, 0:8:2]*box_a[:, 3:10:2],dim=1,keepdim=True) -
              torch.sum(box_a[:, 1:9:2] * box_a[:, 2:9:2], dim=1, keepdim=True))*0.5).expand_as(inter)  # [A,B]


    area_a =  torch.where(mask,area_a_poly,area_a_rect)

    area_b = ((box_b[:, 2]-box_b[:, 0]) *
              (box_b[:, 3]-box_b[:, 1])).unsqueeze(0).expand_as(inter)  # [A,B]
    union = area_a + area_b - inter
    return inter / union  # [A,B]

def matrix_iou(a,b):
    """
    return iou of a and b, numpy version for data augenmentation
    """
    lt = np.maximum(a[:, np.newaxis, :2], b[:, :2])
    rb = np.minimum(a[:, np.newaxis, 2:4], b[:, 2:4])

    area_i = np.prod(rb - lt, axis=2) * (lt < rb).all(axis=2)
    area_a = np.prod(a[:, 2:4] - a[:, :2], axis=1)
    area_b = np.prod(b[:, 2:4] - b[:, :2], axis=1)
    return area_i / (area_a[:, np.newaxis] + area_b - area_i)


def match(matched_threshold, unmatched_threshold, truths, priors, variances, labels, loc_t, conf_t, idx,
          boxes_from_kp=False, numextrafeatures=0, alone_gt_boost_th=2.0, polygon_match=False):
    """Match each prior box with the ground truth box of the highest jaccard
    overlap, encode the bounding boxes, then return the matched indices
    corresponding to both confidence and location preds.
    Args:
        threshold: (float) The overlap threshold used when mathing boxes.
        truths: (tensor) Ground truth boxes, Shape: [num_obj, num_priors].
        priors: (tensor) Prior boxes from priorbox layers, Shape: [n_priors,4].
        variances: (tensor) Variances corresponding to each prior coord,
            Shape: [num_priors, 4].
        labels: (tensor) All the class labels for the image, Shape: [num_obj].
        loc_t: (tensor) Tensor to be filled w/ endcoded location targets.
        conf_t: (tensor) Tensor to be filled w/ matched indices for conf preds.
        idx: (int) current batch index
    Return:
        The matched indices corresponding to 1)location and 2)confidence preds.
    """
    # jaccard index
    if polygon_match == False:
        overlaps,_ = jaccard(
            truths,
            point_form(priors)
        )
    else:
        overlaps = jaccard_kp(
            truths,
            point_form(priors)
        )

    # (Bipartite Matching)
    # [1,num_objects] best prior for each ground truth
    best_prior_overlap, best_prior_idx = overlaps.max(1, keepdim=True)
    # [1,num_priors] best ground truth for each prior
    best_truth_overlap, best_truth_idx = overlaps.max(0, keepdim=True)
    best_truth_idx.squeeze_(0)
    best_truth_overlap.squeeze_(0)
    best_prior_idx.squeeze_(1)
    best_prior_overlap.squeeze_(1)

    # TODO refactor: index  best_prior_idx with long tensor
    # ensure every gt matches with its prior of max overlap
    for j in range(best_prior_idx.size(0)):
        best_anchor_id = best_prior_idx[j] # best anchor id for ground truth j
        if best_truth_overlap[best_anchor_id] <= (best_prior_overlap[j] + alone_gt_boost_th):
            # give boost to the best match of ground truth
            boosted_iou = best_prior_overlap[j] + alone_gt_boost_th
            best_truth_overlap[best_anchor_id] = boosted_iou
            best_truth_idx[best_anchor_id] = j
            best_prior_overlap[j] = boosted_iou


    matches = truths[best_truth_idx]          # Shape: [num_priors,4]
    conf = labels[best_truth_idx]          # Shape: [num_priors]

    # These boxes are below matched threshold, but abodve unmatched threshold.So dont penalize these anchor boxes. It is not zero, but
    #better than zero
    # 0 - 0.1- 1 - 2 - 3 ... 0 -> negative, 1,2,3 -> positive, 0.1,1,2,3 -> non negative
    # 0.1 because, after converting this tensor from float to int, 0.1 will get converted into 0
    conf[best_truth_overlap < matched_threshold] = 0.1

    # label as background, surely penalize these anchor boxes
    conf[best_truth_overlap < unmatched_threshold] = 0

    if (torch.is_tensor(loc_t) == True) and (torch.is_tensor(conf_t) == True):
        if numextrafeatures==0:
            loc = encode(matches, priors, variances, boxes_from_kp)
            loc_t[idx] = loc  # [num_priors,4] encoded offsets to learn
            conf_t[idx] = conf  # [num_priors] top class label for each prior
        else :
            loc = encode(matches[:,:-numextrafeatures], priors, variances,boxes_from_kp)
            loc_t[idx] = torch.cat((loc,matches[:,-numextrafeatures:]),1)    # [num_priors,4] encoded offsets to learn
            conf_t[idx] = conf  # [num_priors] top class label for each prior

    return best_truth_idx,best_prior_idx,best_truth_overlap,best_prior_overlap # return indicies for every prior box for which truth has matched

def encode(matched, priors, variances, boxes_from_kp=False):
    """Encode the variances from the priorbox layers into the ground truth boxes
    we have matched (based on jaccard overlap) with the prior boxes.
    Args:
        matched: (tensor) Coords of ground truth for each prior in point-form
            Shape: [num_priors, 4].
        priors: (tensor) Prior boxes in center-offset form
            Shape: [num_priors,4].
        variances: (list[float]) Variances of priorboxes
    Return:
        encoded boxes (tensor), Shape: [num_priors, 4]
    """

    # dist b/t match center and prior's center
    # prior boxes are in [center_x, center_y, width, height] format
    # encoded box parameters are in [center_x, center_y, width, height, kp0_x, kp0_y, ..]
    # matched box parameters are in [tl_x, tl_y, br_x, br_y]

    g_cxcy = (matched[:, :2] + matched[:, 2:4])/2 - priors[:, :2]
    # encode variance
    g_cxcy /= (variances[0] * priors[:, 2:4])

    numKeyPts = (matched.size()[1] - 4)/2
    if numKeyPts > 0:

        concat_prior_cx_cy = priors[:, :2]
        concat_prior_w_h   = priors[:, 2:4]

        for _ in range(int(numKeyPts) - 1):
          concat_prior_cx_cy = torch.cat((concat_prior_cx_cy,priors[:, :2]),dim=1)
          concat_prior_w_h   = torch.cat((concat_prior_w_h,priors[:, 2:4]),dim=1)

        #keypoint encoding
        g_pxpy = matched[:, 4:]  - concat_prior_cx_cy
        # encode variance
        g_pxpy /= (variances[2] * concat_prior_w_h)

    # match wh / prior wh
    g_wh = (matched[:, 2:4] - matched[:, :2]) / priors[:, 2:4]
    g_wh = torch.log(g_wh) / variances[1]
    # return target for smooth_l1_loss
    if numKeyPts > 0 :
        if boxes_from_kp == False:
            return torch.cat([g_cxcy, g_wh, g_pxpy], 1)  # [num_priors,4 + 2*num_keypoint]
        else:
            return g_pxpy  # [num_priors,2*num_keypoint]
    else:
        return torch.cat([g_cxcy, g_wh], 1)  # [num_priors,4]


def encode_multi(matched, priors, offsets, variances):
    """Encode the variances from the priorbox layers into the ground truth boxes
    we have matched (based on jaccard overlap) with the prior boxes.
    Args:
        matched: (tensor) Coords of ground truth for each prior in point-form
            Shape: [num_priors, 4].
        priors: (tensor) Prior boxes in center-offset form
            Shape: [num_priors,4].
        variances: (list[float]) Variances of priorboxes
    Return:
        encoded boxes (tensor), Shape: [num_priors, 4]
    """
    print('this call should not happen')
    # dist b/t match center and prior's center
    g_cxcy = (matched[:, :2] + matched[:, 2:])/2 - priors[:, :2] - offsets[:,:2]
    # encode variance
    #g_cxcy /= (variances[0] * priors[:, 2:])
    g_cxcy.div_(variances[0] * offsets[:, 2:])
    # match wh / prior wh
    g_wh = (matched[:, 2:] - matched[:, :2]) / priors[:, 2:]
    g_wh = torch.log(g_wh) / variances[1]
    # return target for smooth_l1_loss
    return torch.cat([g_cxcy, g_wh], 1)  # [num_priors,4]

# Adapted from https://github.com/Hakuyume/chainer-ssd
def decode(loc, priors, variances, num_keypoint = 0, boxes_from_kp=False):
    """Decode locations from predictions using priors to undo
    the encoding we did for offset regression at train time.
    Args:
        loc (tensor): location predictions for loc layers,
            Shape: [num_priors,4]
        priors (tensor): Prior boxes in center-offset form.
            Shape: [num_priors,4].
        variances: (list[float]) Variances of priorboxes
    Return:
        decoded bounding box predictions
    """


    if num_keypoint == 0:
        boxes = torch.cat((
            priors[:, :2] + loc[:, :2] * variances[0] * priors[:, 2:],
            priors[:, 2:] * torch.exp(loc[:, 2:4] * variances[1])), 1)
    else :
        if boxes_from_kp == False:
          boxes = torch.cat((
              priors[:, :2] + loc[:, :2] * variances[0] * priors[:, 2:],
              priors[:, 2:] * torch.exp(loc[:, 2:4] * variances[1])),
              1)
          for kp_id in range(int(num_keypoint)):
              boxes = torch.cat((boxes, priors[:, :2] + loc[:, 4+kp_id*2:4+(kp_id+1)*2] * variances[2] * priors[:, 2:]), 1)

        else:
          # format of prior boxes are same whether box parameter is derived from kp or not.
          boxes = torch.empty((loc.size(0),4))
          for kp_id in range(int(num_keypoint)):
              boxes = torch.cat((boxes, priors[:, :2] + loc[:, kp_id*2:(kp_id+1)*2] * variances[2] * priors[:, 2:]), 1)


    # Boxes needs to be converted from [cx,cy,w,h] format to [tl_x,tl_y,tr_x,tr_y]
    if boxes_from_kp == False:
        boxes[:, :2] -= boxes[:, 2:4] / 2
        boxes[:, 2:4] += boxes[:, :2]
    else:
        boxes[:,0] = torch.min(boxes[:,4:4 + 2*num_keypoint:2],1)[0]
        boxes[:,1] = torch.min(boxes[:,5:4 + 2*num_keypoint:2],1)[0]
        boxes[:,2] = torch.max(boxes[:,4:4 + 2*num_keypoint:2],1)[0]
        boxes[:,3] = torch.max(boxes[:,5:4 + 2*num_keypoint:2],1)[0]

    return boxes

def decode_multi(loc, priors, offsets, variances):
    """Decode locations from predictions using priors to undo
    the encoding we did for offset regression at train time.
    Args:
        loc (tensor): location predictions for loc layers,
            Shape: [num_priors,4]
        priors (tensor): Prior boxes in center-offset form.
            Shape: [num_priors,4].
        variances: (list[float]) Variances of priorboxes
    Return:
        decoded bounding box predictions
    """
    print('this call should not happen')
    boxes = torch.cat((
        priors[:, :2] + offsets[:,:2]+ loc[:, :2] * variances[0] * offsets[:, 2:],
        priors[:, 2:] * torch.exp(loc[:, 2:] * variances[1])), 1)
    boxes[:, :2] -= boxes[:, 2:] / 2
    boxes[:, 2:] += boxes[:, :2]
    return boxes

def log_sum_exp(x):
    """Utility function for computing log_sum_exp while determining
    This will be used to determine unaveraged confidence loss across
    all examples in a batch.
    Args:
        x (Variable(tensor)): conf_preds from conf layers
    """
    x_max = x.data.max()
    return torch.log(torch.sum(torch.exp(x-x_max), 1, keepdim=True)) + x_max


def one_hot_embedding(labels, num_classes):
    '''Embedding labels to one-hot form.
    Args:
      labels: (LongTensor) class labels, sized [N,].
      num_classes: (int) number of classes.
    Returns:
      (tensor) encoded labels, sized [N,#classes].
    '''
    y = torch.eye(num_classes)  # [D,D]
    return y[labels]            # [N,D]

# Original author: Francisco Massa:
# https://github.com/fmassa/object-detection.torch
# Ported to PyTorch by Max deGroot (02/01/2017)
def nms(boxes, scores, overlap=0.5, top_k=200, viz_th=0.5, poly_nms=False):
    """Apply non-maximum suppression at test time to avoid detecting too many
    overlapping bounding boxes for a given object.
    Args:
        boxes: (tensor) The location preds for the img, Shape: [num_priors,4].
        scores: (tensor) The class predscores for the img, Shape:[num_priors].
        overlap: (float) The overlap thresh for suppressing unnecessary boxes.
        top_k: (int) The Maximum number of box preds to consider.
    Return:
        The indices of the kept boxes with respect to num_priors.
    """

    keep = torch.Tensor(scores.size(0)).fill_(0).long()
    if boxes.numel() == 0:
        return keep

    if poly_nms == False:
        x1 = boxes[:, 0]
        y1 = boxes[:, 1]
        x2 = boxes[:, 2]
        y2 = boxes[:, 3]

        area = torch.mul(x2 - x1, y2 - y1)
        xx1 = boxes[:, :4].new()
        yy1 = boxes[:, :4].new()
        xx2 = boxes[:, :4].new()
        yy2 = boxes[:, :4].new()
        w = boxes[:, :4].new()
        h = boxes[:, :4].new()
    else:
        kp = boxes[:, 4:]
        area = []
        for id in range(kp.shape[0]):
            area.append(Polygon(kp[id].view(-1,2)).area)
        area = torch.tensor(area)
        cur_kp = kp.new()


    v, idx = scores.sort(0)  # sort in ascending order
    # I = I[v >= 0.01]
    idx = idx[-top_k:]  # indices of the top-k largest vals
    if ((scores[idx][0] > viz_th) and scores.shape[0] > top_k) :
      print('Max detection value {} is not capturing the detection higher than visualization threshold {}'.format(top_k, viz_th))
      print('Lowest score captured is {}'.format(scores[idx][0]))
    # keep = torch.Tensor()
    count = 0
    while idx.numel() > 0:
        i = idx[-1]  # index of current largest val
        # keep.append(i)
        keep[count] = i
        count += 1
        if idx.size(0) == 1:
            break
        idx = idx[:-1]  # remove kept element from view
        # load bboxes of next highest vals
        # store element-wise max with next highest score
        if poly_nms == False:

            torch.index_select(x1, 0, idx, out=xx1)
            torch.index_select(y1, 0, idx, out=yy1)
            torch.index_select(x2, 0, idx, out=xx2)
            torch.index_select(y2, 0, idx, out=yy2)

            xx1 = torch.clamp(xx1, min=x1[i])
            yy1 = torch.clamp(yy1, min=y1[i])
            xx2 = torch.clamp(xx2, max=x2[i])
            yy2 = torch.clamp(yy2, max=y2[i])
            w.resize_as_(xx2)
            h.resize_as_(yy2)
            w = xx2 - xx1
            h = yy2 - yy1
            # check sizes of xx1 and xx2.. after each iteration
            w = torch.clamp(w, min=0.0)
            h = torch.clamp(h, min=0.0)
            inter = w*h
            # IoU = i / (area(a) + area(b) - i)
        else:
            torch.index_select(kp, 0, idx, out=cur_kp)
            master_poly = Polygon(kp[i].view(-1,2))
            inter = []

            for poly_id in range(cur_kp.shape[0]):
                inter.append(Polygon(cur_kp[poly_id].view(-1,2)).intersection(master_poly).area)
            inter = torch.tensor(inter)

        rem_areas = torch.index_select(area, 0, idx)  # load remaining areas)
        union = (rem_areas - inter) + area[i]
        IoU = inter/union  # store result in iou
        # keep only elements with an IoU <= overlap
        idx = idx[IoU.le(overlap)]
    return keep, count
