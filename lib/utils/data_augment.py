"""Data augmentation functionality. Passed as callable transformations to
Dataset classes.

The data augmentation procedures were interpreted from @weiliu89's SSD paper
http://arxiv.org/abs/1512.02325

Ellis Brown, Max deGroot
"""

import torch
import cv2
import numpy as np
import random
import math
from lib.utils.box_utils import matrix_iou

def _crop(image, boxes, labels, center_crop=False, aug_debug=False,img_id=0,num_keypoints=0,crop_aug_partial_obj=True):

    height, width, _ = image.shape

    if len(boxes)== 0:
        return image, boxes, labels, np.ones(len(boxes),dtype=bool)

    reg_iou_th = True

    while True:
        if reg_iou_th == True:
            mode = random.choice((
                None,
                (0.1, None),
                (0.3, None),
                (0.5, None),
                (0.7, None),
                (0.9, None),
                (None, None),
            ))
        else:
            mode = random.choice((
                None,
                (0.7, None),
                (0.9, None),
                (None, None),
            ))

        if mode is None:
            return image, boxes, labels, np.ones(len(boxes),dtype=bool)

        min_iou, max_iou = mode
        if min_iou is None:
            min_iou = float('-inf')
        if max_iou is None:
            max_iou = float('inf')

        for _ in range(50):
            scale = random.uniform(0.3,1.)
            min_ratio = max(0.5, scale*scale)
            max_ratio = min(2, 1. / scale / scale)
            ratio = math.sqrt(random.uniform(min_ratio, max_ratio))
            w = int(scale * ratio * width)
            h = int((scale / ratio) * height)

            if center_crop == True:
              l = (int)((width - w)/2)
              t = (int)((height - h)/2)
            else:
              l = random.randrange(width - w)
              t = random.randrange(height - h)

            roi = np.array((l, t, l + w, t + h))

            iou = matrix_iou(boxes[:, 0:4], roi[np.newaxis])

            if not (min_iou <= iou.min() and iou.max() <= max_iou):
                continue

            image_t = image[roi[1]:roi[3], roi[0]:roi[2]]

            if crop_aug_partial_obj == True:
                centers = (boxes[:, :2] + boxes[:, 2:4]) / 2
                mask = np.logical_and(roi[:2] < centers, centers < roi[2:]).all(axis=1)
            else:
                # check if complete box is in ROI or not
                mask_tl = np.logical_and(roi[:2] < boxes[:, :2], boxes[:, :2] < roi[2:])
                mask_br = np.logical_and(roi[:2] < boxes[:, 2:4], boxes[:, 2:4] < roi[2:])
                mask = np.logical_and(mask_tl,mask_br).all(axis=1)

            boxes_t = boxes[mask].copy()
            labels_t = labels[mask].copy()
            if len(boxes_t) == 0:
                continue

            boxes_t[:, :2] = np.maximum(boxes_t[:, :2], roi[:2])
            boxes_t[:, :2] -= roi[:2]
            boxes_t[:, 2:4] = np.minimum(boxes_t[:, 2:4], roi[2:])
            boxes_t[:, 2:4] -= roi[:2]

            for id in range(num_keypoints):
              boxes_t[:, 4+2*id : 4+2*id + 2]  = np.maximum(boxes_t[:, 4+2*id : 4+2*id + 2], roi[:2])
              boxes_t[:, 4+2*id : 4+2*id + 2]  = np.minimum(boxes_t[:, 4+2*id : 4+2*id + 2], roi[2:])
              boxes_t[:, 4+2*id : 4+2*id + 2] -= roi[:2]

            if aug_debug == True:
                cv2.rectangle(image, (int(boxes[0][0]), int(boxes[0][1])), (int(boxes[0][2]), int(boxes[0][3])), (0, 255, 0), 2)
                cv2.imwrite("orig_{}.jpg".format(img_id), image)

                cv2.rectangle(image_t, (int(boxes_t[0][0]), int(boxes_t[0][1])), (int(boxes_t[0][2]), int(boxes_t[0][3])), (0, 255, 0), 2)
                cv2.imwrite("crop_out_{}.jpg".format(img_id), image_t)

                cv2.imwrite("crop_out_512x512.jpg", cv2.resize(image_t,(512,512)))
            return image_t, boxes_t,labels_t,mask


def _distort(image):
    def _convert(image, alpha=1, beta=0):
        tmp = image.astype(float) * alpha + beta
        tmp[tmp < 0] = 0
        tmp[tmp > 255] = 255
        image[:] = tmp

    image = image.copy()
    #print('distort augmentation is happening')
    if random.randrange(2):
        _convert(image, beta=random.uniform(-32, 32))

    if random.randrange(2):
        _convert(image, alpha=random.uniform(0.5, 1.5))

    image = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)

    if random.randrange(2):
        tmp = image[:, :, 0].astype(int) + random.randint(-18, 18)
        tmp %= 180
        image[:, :, 0] = tmp

    if random.randrange(2):
        _convert(image[:, :, 1], alpha=random.uniform(0.5, 1.5))

    image = cv2.cvtColor(image, cv2.COLOR_HSV2BGR)

    return image


def _expand(image, boxes, fill, p, center_crop=False,aug_debug=False,img_id=0,num_keypoints=0):
    if random.random() > p:
        return image, boxes

    height, width, depth = image.shape
    #print('expand augmentation is happening')

    for _ in range(50):
        scale = random.uniform(1,4)

        min_ratio = max(0.5, 1./scale/scale)
        max_ratio = min(2, scale*scale)
        ratio = math.sqrt(random.uniform(min_ratio, max_ratio))
        ws = scale*ratio
        hs = scale/ratio
        if ws < 1 or hs < 1:
            continue
        w = int(ws * width)
        h = int(hs * height)

        if center_crop == False:
          left = random.randint(0, w - width)
          top = random.randint(0, h - height)
        else:
          left = int((w - width)/2)
          top  = int((h - height)/2)

        boxes_t = boxes.copy()

        boxes_t[:, :2] += (left, top) #TL
        boxes_t[:, 2:4] += (left, top) #BR
        for id in range(num_keypoints):
          boxes_t[:, 4+2*id : 4+2*id + 2]  += (left, top)

        expand_image = np.empty(
            (h, w, depth),
            dtype=image.dtype)
        expand_image[:, :] = fill
        expand_image[top:top + height, left:left + width] = image
        image = expand_image

        if aug_debug == True:
            cv2.rectangle(image, (int(boxes_t[0][0]), int(boxes_t[0][1])), (int(boxes_t[0][2]), int(boxes_t[0][3])),
                          (0, 255, 0), 6)
            cv2.imwrite("expand_out_{}.jpg".format(img_id),image)
            cv2.imwrite("expand_out_{}_512x512.jpg".format(img_id), cv2.resize(image, (512, 512)))

        return image, boxes_t


def _mirror(image, boxes,aug_debug=False,img_id=0,num_keypoints=0):
    _, width, _ = image.shape

    boxes_t = boxes.copy()  # can we use np.empty(boxes.shape), to speed up this augmentation
    #print('mirror augmentation is happening')

    if random.randrange(2):
        image_t = image[:, ::-1]
        boxes_t[:, 0] = width - boxes[:, 2]
        boxes_t[:, 2] = width - boxes[:, 0]

        perm = [1, 0, 3, 2]

        for id in range(num_keypoints):
            boxes_t[:, 4 + 2*id] = width - boxes[:, 4 + 2*perm[id]]
            boxes_t[:, 4 + 2 * id + 1] = boxes[:, 4 + 2 * perm[id] + 1]
    else :
        image_t = image

    if aug_debug == True:
        cv2.rectangle(image_t, (int(boxes_t[0][0]), int(boxes_t[0][1])), (int(boxes_t[0][2]), int(boxes_t[0][3])),
                      (0, 255, 0), 6)
        cv2.imwrite("mirror_out_{}.jpg".format(img_id),image_t)
        cv2.imwrite("mirror_out_{}_512x512.jpg".format(img_id), cv2.resize(image_t, (512, 512)))

    return image_t, boxes_t


def _grey(image, boxes,aug_debug=False,img_id=0):
    _, width, _ = image.shape

    boxes_t = boxes.copy()  # can we use np.empty(boxes.shape), to speed up this augmentation
    #print('grey augmentation is happening')

    if random.randrange(2):
        image_t = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # below is just done to make it 3 channel. This is nothing about input to n/w.  cv2.COLOR_BGR2GRAY makes output as one channel
        image_t = cv2.cvtColor(image_t, cv2.COLOR_GRAY2RGB)

    else :
        image_t = image

    if aug_debug == True:
        cv2.rectangle(image_t, (int(boxes_t[0][0]), int(boxes_t[0][1])), (int(boxes_t[0][2]), int(boxes_t[0][3])),
                      (0, 255, 0), 6)
        cv2.imwrite("grey_out_{}.jpg".format(img_id),image_t)

    return image_t, boxes_t

def _elastic(image, p, alpha=None, sigma=None, random_state=None):

    print('elastic augmentation is happening')

    """Elastic deformation of images as described in [Simard2003]_ (with modifications).
    .. [Simard2003] Simard, Steinkraus and Platt, "Best Practices for
         Convolutional Neural Networks applied to Visual Document Analysis", in Proc. of the International Conference on Document Analysis and
         Recognition, 2003.
     Based on https://gist.github.com/erniejunior/601cdf56d2b424757de5
     From:
     https://www.kaggle.com/bguberfain/elastic-transform-for-data-augmentation
    """
    if random.random() > p:
        return image
    if alpha == None:
        alpha = image.shape[0] * random.uniform(0.5,2)
    if sigma == None:
        sigma = int(image.shape[0] * random.uniform(0.5,1))
    if random_state is None:
        random_state = np.random.RandomState(None)

    shape = image.shape[:2]

    dx, dy = [cv2.GaussianBlur((random_state.rand(*shape) * 2 - 1) * alpha, (sigma|1, sigma|1), 0) for _ in range(2)]
    x, y = np.meshgrid(np.arange(shape[1]), np.arange(shape[0]))
    x, y = np.clip(x+dx, 0, shape[1]-1).astype(np.float32), np.clip(y+dy, 0, shape[0]-1).astype(np.float32)
    return cv2.remap(image, x, y, interpolation=cv2.INTER_LINEAR, borderValue= 0, borderMode=cv2.BORDER_REFLECT)


def preproc_for_test(image, insize, mean):

    #print('preproc_for_test augmentation is happening')

    #interp_methods = [cv2.INTER_LINEAR, cv2.INTER_CUBIC, cv2.INTER_AREA, cv2.INTER_NEAREST, cv2.INTER_LANCZOS4]
    #interp_method = interp_methods[random.randrange(5)]

    dump_img_data = False

    interp_method = cv2.INTER_AREA # For TIDL matching it has to be changes to INTER_AREA from INTER_cubic. INTER_AREA given better accuracy also

    if image.shape != (insize[1], insize[0],3): # do resizing only if the sizes are different
        image = cv2.resize(image, (insize[1], insize[0]),interpolation=interp_method)

    image = image.astype(np.float32)

    if dump_img_data == True: # dump of the original image data
        image.transpose(2, 0, 1).astype('uint8').tofile('/user/a0393749/deepak_files/logs/log_exp_config48_quant/debug/420.bin')
        image = image.astype('int32') # for mean subtraction bring data to int32 in this flow to mimic TIDL

    image -= mean

    if dump_img_data == True: # dump of mean subtracted data
        image.astype('int8').tofile('/user/a0393749/deepak_files/logs/log_exp_config48_quant/debug/420_mean_subtracted.bin')
        image = image.astype('float32') # bring back data to float 32 for rest of the network.

    return image.transpose(2, 0, 1)

def draw_bbox(image, bbxs, color=(0, 255, 0), drawKeyPts=False):
    img = image.copy()
    bbxs = np.array(bbxs).astype(np.int32)

    for bbx in bbxs:
        cv2.rectangle(img, (bbx[0], bbx[1]), (bbx[2], bbx[3]), color, 5)

        if drawKeyPts == True:
            numKeyPts = (bbx.size()[1] - 4)/2
            for ptIdx in range(numKeyPts):
                cv2.circle(img, (bbx[4 + ptIdx*2 + 0], bbx[4 + ptIdx*2 + 1]), color, 5)

    return img

class preproc(object):

    def __init__(self, resize, rgb_means, p, writer=None,codeSize = 5, aug_list =[], center_crop=False,numExtraFeatures=0,num_keypoints=0,crop_aug_partial_obj=True):
        self.means = rgb_means
        self.resize = resize
        self.p = p
        self.writer = writer # writer used for tensorboard visualization
        self.epoch = 0
        self.codeSize = codeSize # Here code size is already coming with extra features. No need to add it again
        self.aug_list = aug_list
        self.center_crop = center_crop
        self.numExtraFeatures=numExtraFeatures
        self.num_keypoints = num_keypoints
        self.aug_debug = False
        self.crop_aug_partial_obj=crop_aug_partial_obj
        self.img_id = 0
    def __call__(self, image, targets=[]):
        # some bugs

        mask =  np.ones(len(targets),dtype=bool)

        if self.p == -2: # abs_test
            targets = np.zeros((1,self.codeSize))
            targets[0] = image.shape[0]
            targets[0] = image.shape[1]
            image = preproc_for_test(image, self.resize, self.means)
            return torch.from_numpy(image), targets, mask

        labels = targets[:,-1].copy()
        extra_features=targets[:,-(1+self.numExtraFeatures):-1].copy()

        #targets  = np.append(targets[:,:-(1+self.numExtraFeatures)],np.expand_dims(labels, 1),axis=1)

        boxes = targets[:,:-(1+self.numExtraFeatures)].copy()

#        print(extra_features)
        if len(boxes) == 0:
            #targets = np.zeros((1,self.codeSize)) # previously one box of zeros used to be inserted but lets not insert that
            image = preproc_for_test(image, self.resize, self.means) # some ground truth in coco do not have bounding box! weird!
            return torch.from_numpy(image), targets,mask
        if self.p == -1: # eval
            height, width, _ = image.shape
            boxes[:, 0::2] /= width
            boxes[:, 1::2] /= height
            labels = np.expand_dims(labels,1)
            targets = np.hstack((boxes,labels))
            image = preproc_for_test(image, self.resize, self.means)
            return torch.from_numpy(image), targets, mask

        # _o indicates zero box scenario. This needs to be corrected for extra features
        image_o = image.copy()
        targets_o = targets.copy()
        height_o, width_o, _ = image_o.shape
        boxes_o = targets_o[:,:-1]
        labels_o = targets_o[:,-1]
        boxes_o[:, 0::2] /= width_o
        boxes_o[:, 1::2] /= height_o
        labels_o = np.expand_dims(labels_o,1)
        targets_o = np.hstack((boxes_o,labels_o))

        if self.writer is not None:
            image_show = draw_bbox(image, boxes)
            self.writer.add_image('preprocess/input_image', image_show, self.epoch)

        image_t = image

        if 'crop' in self.aug_list:
            image_t, boxes, labels, mask = _crop(image, boxes, labels,self.center_crop,self.aug_debug,self.img_id,num_keypoints=self.num_keypoints,crop_aug_partial_obj=self.crop_aug_partial_obj)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/crop_image', image_show, self.epoch)

        if 'distort' in self.aug_list:
            image_t = _distort(image_t)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/distort_image', image_show, self.epoch)

        if 'elastic' in self.aug_list:
            image_t = _elastic(image_t, self.p)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/elastic_image', image_show, self.epoch)

        if 'expand' in self.aug_list:
            image_t, boxes = _expand(image_t, boxes, self.means, self.p, self.center_crop,self.aug_debug,self.img_id,num_keypoints=self.num_keypoints)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/expand_image', image_show, self.epoch)

        if 'mirror' in self.aug_list:
            image_t, boxes = _mirror(image_t, boxes,self.aug_debug,self.img_id,num_keypoints=self.num_keypoints)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/mirror_image', image_show, self.epoch)

        if 'grey' in self.aug_list:
            image_t, boxes = _grey(image_t, boxes,self.aug_debug,self.img_id)
            if self.writer is not None:
                image_show = draw_bbox(image_t, boxes)
                self.writer.add_image('preprocess/grey_image', image_show, self.epoch)

        if self.aug_debug == True:
            self.img_id = self.img_id + 1

        # only write the preprocess step for the first image
        if self.writer is not None:
            # print('image adding')
            self.release_writer()

        height, width, _ = image_t.shape
        image_t = preproc_for_test(image_t, self.resize, self.means)
        boxes = boxes.copy()
        boxes[:, 0::2] /= width
        boxes[:, 1::2] /= height
        b_w = (boxes[:, 2] - boxes[:, 0])*1.
        b_h = (boxes[:, 3] - boxes[:, 1])*1.
        mask_b= np.minimum(b_w, b_h) > 0.01
        boxes_t = boxes[mask_b]
        labels_t = labels[mask_b].copy()

        #consolodating two masks (mask_b, mask) in original dimension of input target. mask_b may be smaller than mask
        mask_b_idx=0
        for idx,m_b in enumerate(mask):
            if m_b == True:
                mask[idx]  = mask_b[mask_b_idx]
                mask_b_idx = mask_b_idx + 1

        if len(boxes_t)==0:
            # we dont want any frame to remain with zero bounding boxes
            image = preproc_for_test(image_o, self.resize, self.means)
            mask = np.ones(len(targets), dtype=bool)
            return torch.from_numpy(image),targets_o,mask

        labels_t = np.expand_dims(labels_t,1)
        if self.numExtraFeatures > 0:
            boxes_t  = np.hstack((boxes_t,extra_features))

        targets_t = np.hstack((boxes_t,labels_t))

        return torch.from_numpy(image_t), targets_t,mask

    def add_writer(self, writer, epoch=None):
        self.writer = writer
        self.epoch = epoch if epoch is not None else self.epoch + 1

    def release_writer(self):
        self.writer = None
