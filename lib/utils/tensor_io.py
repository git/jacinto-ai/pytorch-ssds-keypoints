import os
import time
import sys
import math
import copy

import torch
import torch.nn.parallel
import torch.backends.cudnn as cudnn
import torch.optim
import torch.utils.data
import datetime
import numpy as np
import random
import cv2
import matplotlib.pyplot as plt

from pytorch_jacinto_ai import xnn

def shape_as_string(shape=[]):
    shape_str = ''
    for dim in shape:
        shape_str += '_' + str(dim)
    return shape_str


def write_tensor_int(m=[], tensor=[], suffix='op', bitwidth=8, power2_scaling=True, file_format='bin',
                     rnd_type='rnd_sym'):

    if hasattr(m,'get_clips_act'):
        mn,mx = m.get_clips_act()
    else:
        mn = tensor.min()
        mx = tensor.max()

    print(
        '{:6}, {:32}, {:10}, {:7.2f}, {:7.2f}'.format(suffix, m.name, m.__class__.__name__, tensor.min(), tensor.max()),
        end=" ")

    [tensor_scale, clamp_limits] = xnn.quantize.compute_tensor_scale(tensor, mn, mx, bitwidth, power2_scaling)
    print("{:30} : {:15} : {:8.2f}".format(str(tensor.shape), str(tensor.dtype), tensor_scale), end=" ")

    print_weight_bias = False
    if rnd_type == 'rnd_sym':
        # use best rounding for offline quantities
        if suffix == 'weight' and print_weight_bias:
            no_idx = 0
            torch.set_printoptions(precision=32)
            print("tensor_scale: ", tensor_scale)
            print(tensor[no_idx])
        if tensor.dtype != torch.int64:
            tensor = xnn.quantize.symmetric_round_tensor(tensor * tensor_scale)
        if suffix == 'weight' and print_weight_bias:
            print(tensor[no_idx])
    else:
        # for activation use HW friendly rounding
        if tensor.dtype != torch.int64:
            tensor = xnn.quantize.upward_round_tensor(tensor * tensor_scale)
    tensor = tensor.clamp(clamp_limits[0], clamp_limits[1]).float()

    if bitwidth == 8:
        data_type = np.int8
    elif bitwidth == 16:
        data_type = np.int16
    elif bitwidth == 32:
        data_type = np.int32
    else:
        exit("Bit width other 8,16,32 not supported for writing layer level op")

    tensor = tensor.cpu().numpy().astype(data_type)

    print("{:7} : {:7d} : {:7d}".format(str(tensor.dtype), tensor.min(), tensor.max()))

    tensor_dir = './data/checkpoints/debug/test_vecs/' + '{}_{}_{}_scale_{:010.4f}'.format(m.name,
                                                                                            m.__class__.__name__,
                                                                                            suffix, tensor_scale)

    if not os.path.exists(tensor_dir):
        os.makedirs(tensor_dir)

    if file_format == 'bin':
        tensor_name = tensor_dir + "/{}_shape{}.bin".format(m.name, shape_as_string(shape=tensor.shape))
        tensor.tofile(tensor_name)
    elif file_format == 'npy':
        tensor_name = tensor_dir + "/{}_shape{}.npy".format(m.name, shape_as_string(shape=tensor.shape))
        np.save(tensor_name, tensor)
    else:
        warnings.warn('unknown file_format for write_tensor - no file written')
    #

    # utils_hist.comp_hist_tensor3d(x=tensor, name=m.name, en=True, dir=m.name, log=True, ch_dim=0)


def write_tensor_float(m=[], tensor=[], suffix='op'):
    mn = tensor.min()
    mx = tensor.max()

    print(
        '{:6}, {:32}, {:10}, {:7.2f}, {:7.2f}'.format(suffix, m.name, m.__class__.__name__, tensor.min(), tensor.max()))
    root = os.getcwd()
    tensor_dir = root + '/checkpoints/debug/test_vecs/' + '{}_{}_{}'.format(m.name, m.__class__.__name__, suffix)

    if not os.path.exists(tensor_dir):
        os.makedirs(tensor_dir)

    tensor_name = tensor_dir + "/{}_shape{}.npy".format(m.name, shape_as_string(shape=tensor.shape))
    np.save(tensor_name, tensor.data)


def write_tensor(data_type='int', m=[], tensor=[], suffix='op', bitwidth=8, power2_scaling=True, file_format='bin',
                 rnd_type='rnd_sym'):

    if data_type == 'int':
        write_tensor_int(m=m, tensor=tensor, suffix=suffix, rnd_type=rnd_type, file_format=file_format)
    elif data_type == 'float':
        write_tensor_float(m=m, tensor=tensor, suffix=suffix)


enable_hook_function = True
def write_tensor_hook_function(m, inp, out, file_format='bin'):
    if not enable_hook_function:
        return

    #Output
    if isinstance(out, (torch.Tensor)):
        write_tensor(m=m, tensor=out, suffix='op', rnd_type ='rnd_up', file_format=file_format)

    #Input(s)
    if type(inp) is tuple:
        #if there are more than 1 inputs
        for index, sub_ip in enumerate(inp[0]):
            if isinstance(sub_ip, (torch.Tensor)):
                write_tensor(m=m, tensor=sub_ip, suffix='ip_{}'.format(index), rnd_type ='rnd_up', file_format=file_format)
    elif isinstance(inp, (torch.Tensor)):
         write_tensor(m=m, tensor=inp, suffix='ip', rnd_type ='rnd_up', file_format=file_format)

    #weights
    if hasattr(m, 'weight'):
        if isinstance(m.weight,torch.Tensor):
            write_tensor(m=m, tensor=m.weight, suffix='weight', rnd_type ='rnd_sym', file_format=file_format)

    #bias
    if hasattr(m, 'bias'):
        if m.bias is not None:
            write_tensor(m=m, tensor=m.bias, suffix='bias', rnd_type ='rnd_sym', file_format=file_format)
