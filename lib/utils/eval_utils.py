import torch
import numpy as np

def iou_gt(detect, ground_turths):
    det_size = (detect[2] - detect[0])*(detect[3] - detect[1])
    detect = detect[0:4].resize_(1,4)
    iou = []
    ioa = []

    for gt in ground_turths:
        # print(ground_turths)
        # print('gt', gt)
        # print(detect)
        
        gt = gt[0:4].resize_(1,4)
        # print('gt', gt)
        gt_size = (gt[0][2] - gt[0][0])*(gt[0][3] - gt[0][1])

        inter_max = torch.max(detect, gt)
        inter_min = torch.min(detect, gt)
        # print(inter_max)
        # print(inter_min)
        inter_size = max(inter_min[0][2] - inter_max[0][0], 0.) * max(inter_min[0][3] - inter_max[0][1], 0.)

        _iou = inter_size / (det_size + gt_size - inter_size)
        _ioa = inter_size / gt_size

        iou.append(_iou)
        ioa.append(_ioa)

        # print(inter_size)
        # print(det_size, gt_size)
        # print(iou)
    return iou, ioa
    

## TODO: currently, it is super time cost. any ideas to improve it?
# def cal_tp_fp(detects, ground_turths, label, score, npos, iou_threshold=0.5, conf_threshold=0.01):
#     '''
#     '''
#     for det, gt in zip(detects, ground_turths):
#         for i, det_c in enumerate(det):            
#             gt_c = [_gt[:4].data.resize_(1,4) for _gt in gt if int(_gt[4]) == i+1]  # only 20 in det
#             if len(det_c) == 0:cls_dets
#                 npos[i] += len(gt_c)
#                 continue
#             # print(det_c)
#             # assert(False)
#             iou_c = []
#             ioa_c = []
#             score_c = []
#             num=0
#             for det_c_n in det_c:
#                 if len(gt_c) > 0:
#                     _iou, _ioa = iou_gt(det_c_n[:4], gt_c)
#                     iou_c.append(_iou)
#                     ioa_c.append(_ioa)
#                 score_c.append(det_c_n[4])
#                 num+=1
            
#             # No detection 
#             if len(iou_c) == 0:
#                 npos[i] += len(gt_c)
#                 continue

#             labels_c = [0] * len(score_c)
#             # TODO: currently ignore the difficulty & ignore the group of boxes.
#             # Tp-fp evaluation for non-group of boxes (if any).
#             if len(gt_c) > 0:
#                 # groundtruth_nongroup_of_is_difficult_list = groundtruth_is_difficult_list[
#                 #     ~groundtruth_is_group_of_list]
#                 max_overlap_gt_ids = np.argmax(np.array(iou_c), axis=1)
#                 is_gt_box_detected = np.zeros(len(gt_c), dtype=bool)
#                 for iters in range(len(labels_c)):
#                     gt_id = max_overlap_gt_ids[iters]
#                     if iou_c[iters][gt_id] >= iou_threshold:
#                         # if not groundtruth_nongroup_of_is_difficult_list[gt_id]:
#                         if not is_gt_box_detected[gt_id]:
#                             labels_c[iters] = 1
#                             is_gt_box_detected[gt_id] = True
#                         # else:
#                         #     is_matched_to_difficult_box[i] = True

#             # append to the global label, score
#             npos[i] += len(gt_c)
#             label[i].extend(labels_c)
#             score[i].extend(score_c)
        
#     return label, score, npos
def cal_tp_fp(detects, ground_turths, label, score, npos, gt_label, iou_threshold=0.5, conf_threshold=0.01,codeSize=4, img_shape = [1.0,1.0]):
    '''
    '''
    num_classes   = detects.shape[1]-1
    max_det       = detects.shape[2]

    groudntruth_dist_batch = np.zeros(num_classes)
    num_boxes_det_match_batch = np.zeros(num_classes)
    num_keypoints = int((codeSize - 4)/2)

    scale = np.empty([0,2])
    print(img_shape)
    for idx in range(num_keypoints):
        scale = np.append(scale, [img_shape], axis=0)

    print(scale)

    for det, gt in zip(detects, ground_turths): # for each image of batch

      tp_fp_labels = [ np.empty([0], dtype=int) for _ in range(num_classes)]
      tp_det_gt_idx = [ np.empty([0,2],dtype=int) for _ in range(num_classes)]
      detected_keypoints = np.empty([0, 4, 2])
      groundtruth_keypoints = np.empty([0, 4, 2])
      detected_class_labels = np.empty([0],dtype=int)
      groundtruth_class_labels = np.empty([0],dtype=int)
      detected_scores = np.empty([0],dtype=int)

      for i, det_c in enumerate(det):         # for each class
            gt_c = [_gt[:codeSize].data.resize_(1,codeSize) for _gt in gt if int(_gt[codeSize]) == i]
            iou_c = []
            ioa_c = []
            score_c = []

            for det_c_n in det_c: # this loop is for each object in given image and given class. size of det_c_n=[N][12], maximum value of N is 100
                if det_c_n[0] < conf_threshold: # scores are in descending order, hence once conf goes down then no need to look for all objects
                    break
                if len(gt_c) > 0: # check if for given image, is there any object for given 'c' class
                    _iou, _ioa = iou_gt(det_c_n[1:], gt_c) # find iou for given detected object with all the ground truth (of same class) in the image
                    iou_c.append(_iou)
                    ioa_c.append(_ioa)
                score_c.append(det_c_n[0])
                if codeSize > 4 : # if key points are there
                  detected_scores = np.append(detected_scores,det_c_n[0].data.cpu())
                  detected_keypoints = np.append(detected_keypoints,[det_c_n[5:].data.cpu().numpy().reshape([4,2])*scale], axis=0)
                  detected_class_labels = np.append(detected_class_labels,i-1)

            if codeSize > 4 and i> 0:  # if key points are there
              if len(gt_c) > 0:
                for gt__ in gt_c:
                  cur_gt = gt__.data.cpu()[:,4:12].numpy().reshape([4,2])*scale
                  groundtruth_keypoints = np.append(groundtruth_keypoints,[cur_gt], axis=0)
                groundtruth_class_labels = np.append(groundtruth_class_labels,([i-1] * len(gt_c)))

            # while det_c[num,0] > conf_threshold:
            #     if len(gt_c) > 0:
            #         _iou, _ioa = iou_gt(det_c[num,1:], gt_c)
            #         iou_c.append(_iou)
            #         ioa_c.append(_ioa)
            #     score_c.append(det_c[num, 0])
            #     num+=1

            # No detection
            if len(iou_c) == 0:
                npos[i] += len(gt_c)
                if len(gt_c) > 0:
                    is_gt_box_detected = np.zeros(len(gt_c), dtype=bool)
                    gt_label[i] += is_gt_box_detected.tolist()

                # defualt values in case of no detection
                if i > 0 and codeSize > 4: # if key points are there
                  for iters in range(len(score_c)):
                      tp_fp_labels[i-1] = np.append(tp_fp_labels[i-1], False)
                      tp_det_gt_idx[i-1] = np.append(tp_det_gt_idx[i-1], [[int(iters), int(len(gt_c) + 1)]], axis = 0)

                continue

            labels_c = [0] * len(score_c)

            # TODO: currently ignore the difficulty & ignore the group of boxes.
            # Tp-fp evaluation for non-group of boxes (if any).
            if len(gt_c) > 0:
                # groundtruth_nongroup_of_is_difficult_list = groundtruth_is_difficult_list[
                #     ~groundtruth_is_group_of_list]
                max_overlap_gt_ids = np.argmax(np.array(iou_c), axis=1) # find for each detection, which ground truth has overlapped most
                is_gt_box_detected = np.zeros(len(gt_c), dtype=bool)
                for iters in range(len(labels_c)):
                    gt_id = max_overlap_gt_ids[iters] #gt id overlapped maximum with current detection

                    if codeSize > 4:
                      tp_fp_labels[i-1] = np.append(tp_fp_labels[i-1], False)
                      tp_det_gt_idx[i-1] = np.append(tp_det_gt_idx[i-1], [[int(iters), int(gt_id)]], axis=0)

                    if iou_c[iters][gt_id] >= iou_threshold:
                        # if not groundtruth_nongroup_of_is_difficult_list[gt_id]:
                        if codeSize > 4:
                          tp_fp_labels[i-1] = np.append(tp_fp_labels[i-1], True)

                        if not is_gt_box_detected[gt_id]:
                            labels_c[iters] = 1              # list of detections, 1 indicates that it has matched with ground truth, 0 means not matched with gt
                            is_gt_box_detected[gt_id] = True # list for ground truth, True means that this groundtruth has been detected
                        # else:
                        #     is_matched_to_difficult_box[i] = True

            # append to the global label, score
            npos[i] += len(gt_c)
            label[i] += labels_c
            score[i] += score_c
            gt_label[i] += is_gt_box_detected.tolist()

      #print('detected_scores',detected_scores)
      #print('tp_fp_labels',tp_fp_labels)
      #print('tp_det_gt_idx',tp_det_gt_idx)
      #print('gt_c_all',groundtruth_keypoints)
      #print('detected_class_labels',detected_class_labels)
      #print('groundtruth_class_labels',groundtruth_class_labels)

      if codeSize > 4:
        groudntruth_dist, num_boxes_det_match = compute_key_point_error(tp_fp_labels, tp_det_gt_idx,
                                                                        detected_keypoints,
                                                                        groundtruth_keypoints,
                                                                        detected_class_labels.astype(int),
                                                                        groundtruth_class_labels.astype(int),
                                                                        detected_scores,
                                                                        num_classes, 0.5)


        groudntruth_dist_batch += groudntruth_dist
        num_boxes_det_match_batch += num_boxes_det_match

    return label, score, npos, gt_label, groudntruth_dist_batch, num_boxes_det_match_batch


def cal_size(detects, ground_turths, size):
    for det, gt in zip(detects, ground_turths):
        for i, det_c in enumerate(det):  
            gt_c = [_gt[:4].data.resize_(1,4) for _gt in gt if int(_gt[4]) == i] 
            if len(gt_c) == 0:
                continue
            gt_size_c = [ [(_gt[0][2] - _gt[0][0]), (_gt[0][3] - _gt[0][1])] for _gt in gt_c ]
            # scale_c = [ min(_size) for _size in gt_size_c ]
            size[i] += gt_size_c
    return size

# def get_correct_detection(detects, ground_turths, iou_threshold=0.5, conf_threshold=0.01):
#     detected = list()
#     for det, gt in zip(detects, ground_turths):
#         for i, det_c in enumerate(det):            
#             gt_c = [_gt[:4].data.resize_(1,4) for _gt in gt if int(_gt[4]) == i] 
#             iou_c = []
#             ioa_c = []
#             detected_c = []
#             # num=0
#             for det_c_n in det_c:
#                 if det_c_n[0] < conf_threshold:
#                     break
#                 if len(gt_c) > 0:
#                     _iou, _ioa = iou_gt(det_c_n[1:], gt_c)
#                     if _iou > iou_threshold:
#                         iou_c.append(_iou)
#                         ioa_c.append(_ioa)
#                 detected_c.append(det_c_n[1:])

#             if len(iou_c) == 0:
#                 npos[i] += len(gt_c)
#                 continue

#             labels_c = [0] * len(detected_c)
#             # TODO: currently ignore the difficulty & ignore the group of boxes.
#             # Tp-fp evaluation for non-group of boxes (if any).
#             if len(gt_c) > 0:
#                 # groundtruth_nongroup_of_is_difficult_list = groundtruth_is_difficult_list[
#                 #     ~groundtruth_is_group_of_list]
#                 max_overlap_gt_ids = np.argmax(np.array(iou_c), axis=1)
#                 is_gt_box_detected = np.zeros(len(gt_c), dtype=bool)
#                 for iters in range(len(labels_c)):
#                     gt_id = max_overlap_gt_ids[iters]
#                     if iou_c[iters][gt_id] >= iou_threshold:
#                         # if not groundtruth_nongroup_of_is_difficult_list[gt_id]:
#                         if not is_gt_box_detected[gt_id]:
#                             labels_c[iters] = 1
#                             is_gt_box_detected[gt_id] = True
#             detected_c = detected_c[labels_c]
#             detected.append(detected_c)
        
#     return detected


def cal_pr(_label, _score, _npos):
    recall = []
    precision = []
    ap = []
    for labels, scores, npos in zip(_label[1:], _score[1:], _npos[1:]):
        sorted_indices = np.argsort(scores)
        sorted_indices = sorted_indices[::-1]
        labels = np.array(labels).astype(int)
        true_positive_labels = labels[sorted_indices]
        false_positive_labels = 1 - true_positive_labels
        tp = np.cumsum(true_positive_labels)
        fp = np.cumsum(false_positive_labels)

        rec = tp.astype(float) / float(npos)
        prec = tp.astype(float) / np.maximum(tp + fp, np.finfo(np.float64).eps)
        ap += [compute_average_precision(prec, rec)]
        recall+=[rec]
        precision+=[prec]
    mAP = np.nanmean(ap)
    return precision, recall, mAP


# def voc_ap(rec, prec, use_07_metric=False):
#     """ ap = voc_ap(rec, prec, [use_07_metric])
#     Compute VOC AP given precision and recall.
#     If use_07_metric is true, uses the
#     VOC 07 11 point method (default:False).
#     """
#     if use_07_metric:
#         # 11 point metric
#         ap = 0.
#         for t in np.arange(0., 1.1, 0.1):
#             if np.sum(rec >= t) == 0:
#                 p = 0
#             else:
#                 p = np.max(prec[rec >= t])
#             ap = ap + p / 11.
#     else:
#         # correct AP calculation
#         # first append sentinel values at the end
#         mrec = np.concatenate(([0.], rec, [1.]))
#         mpre = np.concatenate(([0.], prec, [0.]))

#         # compute the precision envelope
#         for i in range(mpre.size - 1, 0, -1):
#             mpre[i - 1] = np.maximum(mpre[i - 1], mpre[i])

#         # to calculate area under PR curve, look for points
#         # where X axis (recall) changes value
#         i = np.where(mrec[1:] != mrec[:-1])[0]

#         # and sum (\Delta recall) * prec
#         ap = np.sum((mrec[i + 1] - mrec[i]) * mpre[i + 1])
#     return ap

def compute_average_precision(precision, recall):
  """Compute Average Precision according to the definition in VOCdevkit.

  Precision is modified to ensure that it does not decrease as recall
  decrease.

  Args:
    precision: A float [N, 1] numpy array of precisions
    recall: A float [N, 1] numpy array of recalls

  Raises:
    ValueError: if the input is not of the correct format

  Returns:
    average_precison: The area under the precision recall curve. NaN if
      precision and recall are None.

  """
  if precision is None:
    if recall is not None:
      raise ValueError("If precision is None, recall must also be None")
    return np.NAN

  if not isinstance(precision, np.ndarray) or not isinstance(recall,
                                                             np.ndarray):
    raise ValueError("precision and recall must be numpy array")
  if precision.dtype != np.float or recall.dtype != np.float:
    raise ValueError("input must be float numpy array.")
  if len(precision) != len(recall):
    raise ValueError("precision and recall must be of the same size.")
  if not precision.size:
    return 0.0
  if np.amin(precision) < 0 or np.amax(precision) > 1:
    raise ValueError("Precision must be in the range of [0, 1].")
  if np.amin(recall) < 0 or np.amax(recall) > 1:
    raise ValueError("recall must be in the range of [0, 1].")
  if not all(recall[i] <= recall[i + 1] for i in range(len(recall) - 1)):
    raise ValueError("recall must be a non-decreasing array")

  recall = np.concatenate([[0], recall, [1]])
  precision = np.concatenate([[0], precision, [0]])

  # Preprocess precision to be a non-decreasing array
  for i in range(len(precision) - 2, -1, -1):
    precision[i] = np.maximum(precision[i], precision[i + 1])

  indices = np.where(recall[1:] != recall[:-1])[0] + 1
  average_precision = np.sum(
      (recall[indices] - recall[indices - 1]) * precision[indices])
  return average_precision

def compute_key_point_error(tp_fp_labels, tp_det_gt_idx,
                            detected_keypoints, groundtruth_keypoints,
                            detected_class_labels, groundtruth_class_labels, detected_scores,
                            num_classes, matching_iou_threshold):

  groudntruth_dist = np.zeros(num_classes)
  num_boxes_det_match = np.zeros(num_classes)

  for i in range(num_classes):  # for each class

    norm_i = 0.0
    num_boxes = 0
    selected_detections = (detected_class_labels == i)
    detected_keypoints_at_ith_class = detected_keypoints[selected_detections]
    detected_scores_at_ith_class = detected_scores[selected_detections]

    gt_selected_detections = (groundtruth_class_labels == i)
    groundtruth_keypoints_at_ith_class = groundtruth_keypoints[gt_selected_detections]

    #print('procesing the {}th class',i)
    #print('selected_detections',selected_detections)
    #print('detected_keypoints_at_ith_class',detected_keypoints_at_ith_class)
    #print('detected_scores_at_ith_class',detected_scores_at_ith_class)
    #print('gt_selected_detections',gt_selected_detections)
    #print('groundtruth_keypoints_at_ith_class',groundtruth_keypoints_at_ith_class)

    for j, (cur_tp_det_gt_idx, cur_tp_fp) in enumerate(zip(tp_det_gt_idx[i], tp_fp_labels[i])):  # for each detection
      norm_box_i = 0.0
      norm_box_i_rev = 0.0

      #print('cur_tp_det_gt_idx',cur_tp_det_gt_idx)
      #print('cur_tp_fp',cur_tp_fp)
      if cur_tp_fp == True and detected_scores_at_ith_class[
        cur_tp_det_gt_idx[0]] > matching_iou_threshold:  # True positive can be with low detection score as well
        print('True Positive is detected')
        gt_keypoints = groundtruth_keypoints_at_ith_class[cur_tp_det_gt_idx[1]]
        gt_pt_per_edge = int(len(gt_keypoints) / 4)
        det_keypoints = detected_keypoints_at_ith_class[cur_tp_det_gt_idx[0]]
        gt_keypoints_2 = np.concatenate((gt_keypoints, gt_keypoints))  # 1-2-3-4-1-2-3-4
        gt_keypoints_2_rev = gt_keypoints_2[::-1]  # 4-3-2-1-4-3-2-1
        #print('gt_keypoints',gt_keypoints)
        #print('det_keypoints',det_keypoints)

        boxPeriphery = 0.0
        num_boxes = num_boxes + 1
        for gt_pt, det_pt, next_gt_pt in zip(gt_keypoints, det_keypoints,
                                             gt_keypoints_2[1:]):  # for each keypoint in one detection
          norm_box_i = norm_box_i + np.linalg.norm(gt_pt - det_pt)
          boxPeriphery = boxPeriphery + np.linalg.norm(
            next_gt_pt - gt_pt)  # next_pt -- 2-3-4-1, gt_pt -- 1-2-3-4, det_pt -- 1-2-3-4

        revStartPoint = 3 * gt_pt_per_edge
        if gt_pt_per_edge == 1:
          revStartPoint = revStartPoint - 1

        for gt_pt, det_pt in zip(gt_keypoints_2_rev[revStartPoint:],
                                 det_keypoints):  # for each keypoint in one detection
          norm_box_i_rev = norm_box_i_rev + np.linalg.norm(gt_pt - det_pt)  # gt_pt -- 2-1-4-3

        if norm_box_i_rev < norm_box_i:
          norm_box_i = norm_box_i_rev
          print('key points has matched in reverse order')

        norm_box_i = norm_box_i / gt_pt_per_edge
        norm_box_i = np.clip(norm_box_i / boxPeriphery, 0.0, 1.0)
        if norm_box_i >= 0.25:
          print('More than 25% deviation in 4 corner key points wrt periphery')
          print('cur_tp_det_gt_idx, class', cur_tp_det_gt_idx, i)
          print('gt_keypoints', gt_keypoints)
          print('det_keypoints', det_keypoints)
          #print('gt_bbox', groundtruth_boxes[cur_tp_det_gt_idx[1]])
          #print('det_bbox', detected_boxes[cur_tp_det_gt_idx[0]])
          print('det_score', detected_scores[cur_tp_det_gt_idx[0]])

        norm_i = norm_i + norm_box_i

    groudntruth_dist[i] = norm_i
    num_boxes_det_match[i] = num_boxes

    if num_boxes > 0:
      print("  Keypoints_dist_error_{} :: {}".format(i, groudntruth_dist[i]))
      print("  NumBoxes_{} :: {}".format(i, num_boxes))
      print("  AvgKeypoints_dist_error_{} :: {}".format(i, groudntruth_dist[i] / num_boxes))

  return groudntruth_dist, num_boxes_det_match
