# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
from __future__ import print_function
import numpy as np
import os
import sys
import cv2
import random
import pickle

import torch
import torch.backends.cudnn as cudnn
from torch.autograd import Variable
import torch.optim as optim
from torch.optim import lr_scheduler
import torch.utils.data as data
import torch.nn.init as init

from tensorboardX import SummaryWriter

from lib.layers import *
from lib.utils.timer import Timer
from lib.utils.data_augment import preproc
from lib.modeling.model_builder import create_model
from lib.dataset.dataset_factory import load_data
from lib.utils.config_parse import cfg
from lib.utils.eval_utils import *
from lib.utils.visualize_utils import *
from lib.utils.box_utils import *
from lib.modeling.ssds.ssd_lite import post_forward
from google.protobuf import text_format

from torch.autograd import Variable
import torch.onnx
import torchvision

from initHSVTable import initHSVTable

from .proto import mmdet_meta_arch_pb2

def dets(args):
    pass

class Solver(object):
    """
    A wrapper class for the training process
    """
    def __init__(self):
        self.cfg = cfg
        # cfg parameter check
        if cfg.DATASET.NUM_KEYPOINTS != cfg.MODEL.NUM_KEYPOINTS:
            print('#### Dataset number of keypoints is not same as the model number of keypoints######')

        # Load data
        print('===> Loading data')
        self.train_loader = load_data(cfg.DATASET, 'train') if 'train' in cfg.PHASE else None
        self.eval_loader = load_data(cfg.DATASET, 'eval') if 'eval' in cfg.PHASE else None
        self.test_loader = load_data(cfg.DATASET, 'test') if 'test' in cfg.PHASE else None
        self.visualize_loader = load_data(cfg.DATASET, 'visualize') if 'visualize' in cfg.PHASE else None

        # Build modelcfg
        print('===> Building model')
        self.model, self.priorbox = create_model(cfg.MODEL)

        if cfg.MODEL.BOXES_FROM_KP == True:
            self.box_prm_size = 0
            self.boxes_from_kp = True
        else :
            self.box_prm_size = 4
            self.boxes_from_kp = False

        self.code_size = self.box_prm_size + cfg.MODEL.NUM_KEYPOINTS*2 + cfg.MODEL.NUM_EXTRA_FEATURE
        self.num_keypoints = cfg.MODEL.NUM_KEYPOINTS
        self.num_classes = cfg.MODEL.NUM_CLASSES
        self.start_epoch = 0
        self.num_ExtraFeatures = cfg.MODEL.NUM_EXTRA_FEATURE

        with torch.no_grad():
            self.priors = Variable(self.priorbox.forward()[0])

        self.prior_box_map = self.priorbox.forward()[1]

        # debugging flag to enable taggin of each detection with head and aspect ratio
        self.tag_det_prior_box_id = False

        self.detector = Detect(cfg.POST_PROCESS, self.priors, self.num_keypoints,self.num_ExtraFeatures,
                               self.boxes_from_kp, self.tag_det_prior_box_id,
                               cfg.POST_PROCESS.POLY_NMS,
                               cfg.POST_PROCESS.INTER_CLASS_NMS)

        # Utilize GPUs for computation
        if cfg.TEST.WRITE_LAYER_IP_OP == False:
            self.num_gpu = torch.cuda.device_count()
            self.use_gpu = torch.cuda.is_available()
        else:
            self.use_gpu = False # Tensor dump only in cpu flow, as some mismatch is observed in GPU flow

        trainable_param = self.trainable_param(cfg.TRAIN.TRAINABLE_SCOPE)
            # if torch.cuda.device_count() > 1:
                # self.model = torch.nn.DataParallel(self.model).module

        # Print the model architecture and parameters
        print('Model architectures:\n{}\n'.format(self.model))

        # print('Parameters and size:')
        # for name, param in self.model.named_parameters():
        #     print('{}: {}'.format(name, list(param.size())))

        # print trainable scope
        print('Trainable scope: {}'.format(cfg.TRAIN.TRAINABLE_SCOPE))

        self.optimizer = self.configure_optimizer(trainable_param, cfg.TRAIN.OPTIMIZER)
        self.exp_lr_scheduler = self.configure_lr_scheduler(self.optimizer, cfg.TRAIN.LR_SCHEDULER)
        self.max_epochs = cfg.TRAIN.MAX_EPOCHS

        # metric
        self.criterion = MultiBoxLoss(cfg.MATCHER, self.priors, self.use_gpu, self.code_size, self.boxes_from_kp,num_extra_feature=self.num_ExtraFeatures,softmax_or_sigmoid=self.cfg.MODEL.SOFTMAX_OR_SIGMOID)

        # Set the logger
        self.train_writer = SummaryWriter(log_dir=cfg.LOG_DIR + '/train_log/')
        self.eval_writer  = SummaryWriter(log_dir=cfg.LOG_DIR + '/eval_log/')
        self.viz_writer  = SummaryWriter(log_dir=cfg.LOG_DIR + '/viz_log/')


        self.output_dir = cfg.EXP_DIR + '/test_out/' # output after test is stored here
        self.output_viz_dir = cfg.EXP_DIR + '/viz_log/'  # output after test is stored here
        self.checkpoint_dir = cfg.EXP_DIR + '/train_chkpts/' # training related model check points are saved here
        print('check point dir is {}'.format(self.checkpoint_dir))
        self.checkpoint = cfg.RESUME_CHECKPOINT
        self.checkpoint_prefix = cfg.CHECKPOINTS_PREFIX

    #write PyTorch model in ONNX format
    def write_model_in_onnx_format(self, model_to_save, input_size=[512,512], input_names=None, output_names=None, output_dir= '',epochs=0, iters=None):
        dummy_input = Variable(torch.randn(1, 3, input_size[0], input_size[1])).cuda()

        if iters:
            filename = self.checkpoint_prefix + '_epoch_{:d}_iter_{:d}'.format(epochs, iters) + '.onnx'
        else:
            filename = self.checkpoint_prefix + '_epoch_{:d}'.format(epochs) + '.onnx'

        abs_filename = os.path.join(output_dir, filename)

        torch.onnx.export(model_to_save, dummy_input, abs_filename, verbose=False, input_names=input_names, output_names=output_names)

    # Detection layer information is saved as prototxt file


    def write_model_od_proto(self, model_to_save, input_size=[512,512], input_names=None, output_names=None, output_dir='',
                          name='model.prototxt'):
        num_output_names = len(output_names) // 2
        reg_output_names = output_names[:num_output_names]
        cls_output_names = output_names[num_output_names:]
        prior_box = self.priorbox
        prior_box_param = []
        variance = [self.cfg.MATCHER.VARIANCE[0],self.cfg.MATCHER.VARIANCE[0],self.cfg.MATCHER.VARIANCE[1],self.cfg.MATCHER.VARIANCE[1]]

        for _ in range(2*self.cfg.MODEL.NUM_KEYPOINTS):
            variance.append(self.cfg.MATCHER.VARIANCE[2])

        for h_idx in range(num_output_names):

            prior_box_param.append(mmdet_meta_arch_pb2.PriorBoxParameter(min_size=[prior_box.scales[h_idx]*input_size[1]],
                                                                         max_size=[prior_box.scales[h_idx+1]*input_size[1]],
                                                                         aspect_ratio=prior_box.aspect_ratios[h_idx][1:], # aspect ration 1 is default, hence need not to put
                                                                         step_h=prior_box.steps[h_idx][0]*input_size[0],
                                                                         step_w=prior_box.steps[h_idx][1]*input_size[1],
                                                                         variance=variance,
                                                                         flip=True,
                                                                         clip=False,
                                                                         num_keypoint=cfg.MODEL.NUM_KEYPOINTS,
                                                                         offset=0.5))

        nms_param = mmdet_meta_arch_pb2.TIDLNmsParam(top_k=200,
                                                     nms_threshold=self.cfg.POST_PROCESS.IOU_THRESHOLD)

        detection_output_param = mmdet_meta_arch_pb2.TIDLOdPostProc(num_classes=len(self.cfg.DATASET.OBJ_CLASSES),
                                                                    share_location=True,
                                                                    background_label_id=0,
                                                                    code_type=mmdet_meta_arch_pb2.CENTER_SIZE,
                                                                    keep_top_k=100,
                                                                    nms_param=nms_param,
                                                                    confidence_threshold=self.cfg.POST_PROCESS.VIZ_THRESHOLD,
                                                                    num_keypoint=cfg.MODEL.NUM_KEYPOINTS)

        if self.cfg.MODEL.SOFTMAX_OR_SIGMOID:
            score_converter = mmdet_meta_arch_pb2.SOFTMAX
        else:
            score_converter = mmdet_meta_arch_pb2.SIGMOID

        ssd = mmdet_meta_arch_pb2.TidlMaCaffeSsd(name='ssd_post_proc',
                                                 box_input=reg_output_names, class_input=cls_output_names,
                                                 output='output', prior_box_param=prior_box_param,
                                                 detection_output_param=detection_output_param,
                                                 in_height=input_size[0], in_width=input_size[1],
                                                 score_converter=score_converter)

        arch = mmdet_meta_arch_pb2.TIDLMetaArch(name='ssd', caffe_ssd=[ssd])
        with open(os.path.join(output_dir, name), 'wt') as pfile:
            txt_message = text_format.MessageToString(arch)
            pfile.write(txt_message)

    def save_checkpoints(self, epochs, iters=None):
        if not os.path.exists(self.checkpoint_dir):
            os.makedirs(self.checkpoint_dir)
        if iters:
            filename = self.checkpoint_prefix + '_epoch_{:d}_iter_{:d}'.format(epochs, iters) + '.pth'
        else:
            filename = self.checkpoint_prefix + '_epoch_{:d}'.format(epochs) + '.pth'

        abs_filename = os.path.join(self.checkpoint_dir, filename)

        # Providing input and output names sets the display names for values
        # within the model's graph. Setting these does not change the semantics
        # of the graph; it is only for readability.
        #
        # The inputs to the network consist of the flat list of inputs (i.e.
        # the values you would pass to the forward() method) followed by the
        # flat list of parameters. You can partially specify names, i.e. provide
        # a list here shorter than the number of inputs to the model, and we will
        # only set that subset of names, starting from the beginning.
        #input_names = [ "actual_input_1" ] + [ "learned_%d" % i for i in range(50) ]

        input_names = [ "input_image_3_{}_{}".format(cfg.MODEL.IMAGE_SIZE[0], cfg.MODEL.IMAGE_SIZE[1])]

        output_names = []

        for cls_idx in range(len(self.priorbox.scales)-1):
            output_names.append(f'loc_{cls_idx}')
        #
        for reg_idx in range(len(self.priorbox.scales)-1):
            output_names.append(f'conf_{reg_idx}')

        input_size = [cfg.MODEL.IMAGE_SIZE[0],cfg.MODEL.IMAGE_SIZE[1]]

        model_to_save = self.model
        if self.cfg.TRAIN.QUANTIZE == True:
            model_to_save = model_to_save.module

        if isinstance(model_to_save, torch.nn.DataParallel):
            model_to_save = model_to_save.module

        torch.save(model_to_save.state_dict(), abs_filename)
        #original torch.save(model_to_save, abs_filename)

        with open(os.path.join(self.checkpoint_dir, 'checkpoint_list.txt'), 'a') as f:
            f.write('epoch {epoch:d}: {filename}\n'.format(epoch=epochs, filename=filename))
        print('Wrote snapshot to: {:s}'.format(os.path.join(self.checkpoint_dir,filename)))

        if self.cfg.POST_PROCESS.SAVE_OD_PROTOTXT  == True: # it will write prototxt at every time whenever onnx is saved
            self.write_model_od_proto(model_to_save, input_size=input_size, input_names=input_names,
                                      output_names=output_names, output_dir=self.checkpoint_dir, name='model.prototxt')

        if self.cfg.POST_PROCESS.SAVE_ONNX == True:
            self.write_model_in_onnx_format(model_to_save, input_size=input_size, input_names=input_names,
                                            output_names=output_names, output_dir=self.checkpoint_dir, epochs=epochs, iters=iters)

    def resume_checkpoint(self, resume_checkpoint):
        if resume_checkpoint == '' or not os.path.isfile(resume_checkpoint):
            print(("=> no checkpoint found at '{}'".format(resume_checkpoint)))
            return False
        print(("=> loading checkpoint '{:s}'".format(resume_checkpoint)))

        checkpoint = torch.load(resume_checkpoint)

        if hasattr(checkpoint, 'state_dict'):
            checkpoint = checkpoint.state_dict()

        if hasattr(self.model, 'module'):
            model = self.model.module
        else:
            model = self.model

        loading_type = 'CONTROLLED_LOADING'
        #Various loading types are supported for e.g. load only base n/w
        if loading_type == 'CONTROLLED_LOADING':
          # remove the module in the parrallel model
          if 'module.' in list(checkpoint.items())[0][0]:
              pretrained_dict = {'.'.join(k.split('.')[1:]): v for k, v in list(checkpoint.items())}
              checkpoint = pretrained_dict

          resume_scope = self.cfg.TRAIN.RESUME_SCOPE
          # extract the weights based on the resume scope
          if resume_scope != '':
              pretrained_dict = {}
              for k, v in list(checkpoint.items()):
                  for resume_key in resume_scope.split(','):
                      if resume_key in k:
                          pretrained_dict[k] = v
                          break
              checkpoint = pretrained_dict

          #pretrained_dict = {k: v for k, v in checkpoint.items() if k in self.model.state_dict()}
          pretrained_dict = {}
          for k, v in checkpoint.items():
              if k in model.state_dict() and v.size() == model.state_dict()[k].size():
                  pretrained_dict[k] = v

          print("=> Resume weigths:")
          print([k for k, v in list(pretrained_dict.items())])

          checkpoint = model.state_dict()

          unresume_dict = set(checkpoint)-set(pretrained_dict)
          if len(unresume_dict) != 0:
              print("=> UNResume weigths:")
              print(unresume_dict)

          checkpoint.update(pretrained_dict)

          model.load_state_dict(checkpoint)
          return self.model
        else:
          #basic loading.
          self.load_weights_check(model, checkpoint)
          return self.model

    def find_previous(self):
        if not os.path.exists(os.path.join(self.checkpoint_dir, 'checkpoint_list.txt')):
            return False

        with open(os.path.join(self.checkpoint_dir, 'checkpoint_list.txt'), 'r') as f:
            lineList = f.readlines()

        epoches, resume_checkpoints = [list() for _ in range(2)]
        for line in lineList:
            epoch = int(line[line.find('epoch ') + len('epoch '): line.find(':')])
            checkpoint = line[line.find(':') + 2:-1]
            epoches.append(epoch)
            checkpoint = os.path.join(self.checkpoint_dir,os.path.split(checkpoint)[1])
            resume_checkpoints.append(checkpoint)
        return epoches, resume_checkpoints

    def load_weights_check(self, model, data, change_names_dict=None):
        if data is None:
            return model

        load_error = False
        data = data['state_dict'] if ((data is not None) and 'state_dict' in data) else data

        try:
            model.load_state_dict(data, strict=True)
        except:
            load_error = True

        if load_error:
            # model did not load correctly. do any translation required.
            model_dict = model.state_dict()

            # align the prefix 'module.' between model and data
            model_prefix = 'module.' if 'module.' in list(model_dict.keys())[0] else ''
            data_prefix = 'module.' if 'module.' in list(data.keys())[0] else ''
            data = {k.replace(data_prefix,model_prefix):v for k,v in data.items()} if data_prefix != '' \
                else {model_prefix+k:v for k,v in data.items()}

            # translate data name to model name
            if change_names_dict is not None:
                for data_str, model_str in change_names_dict.items():
                    data = {re.sub(data_str, model_str, k):v for k, v in data.items() if model_str not in k}

            missing_weights, extra_weights = self.check_model_data(model, data)

            # num_batches_tracked was newly added to batchnorm in track_running_stats_mode (default)
            # if it is not present in the data, use a reasonably good value
            # so that batch norm stats won't change suddenly
            for mw in missing_weights:
                if 'num_batches_tracked' in mw:
                    data[mw] = torch.tensor(100)

            model.load_state_dict(data, strict=False)

        return model


    def check_model_data(self, model, data, verbose = True):
        model_dict = model.state_dict()
        missing_weights = [k for k, v in model_dict.items() if k not in list(data.keys())]
        extra_weights = [k for k, v in data.items() if k not in list(model_dict.keys())]

        if verbose and missing_weights:
            print("=> The following layers in the model could not be loaded from pre-trained: ", missing_weights)
        if verbose and extra_weights:
            print("=> The following weights in pre-trained were not used: ", extra_weights)

        return missing_weights, extra_weights

        def weights_init(self, m):
            for key in m.state_dict():
                if key.split('.')[-1] == 'weight':
                    if 'conv' in key:
                        init.kaiming_normal(m.state_dict()[key], mode='fan_out')
                    if 'bn' in key:
                        m.state_dict()[key][...] = 1
                elif key.split('.')[-1] == 'bias':
                    m.state_dict()[key][...] = 0


    def initialize(self):
        # TODO: ADD INIT ways
        # raise ValueError("Fan in and fan out can not be computed for tensor with less than 2 dimensions")
        # for module in self.cfg.TRAIN.TRAINABLE_SCOPE.split(','):
        #     if hasattr(self.model, module):
        #         getattr(self.model, module).apply(self.weights_init)
        if self.checkpoint:
            print('Loading initial model weights from {:s}'.format(self.checkpoint))
            self.resume_checkpoint(self.checkpoint)
        else:
            print("Found no 'Initial model weight'")

        start_epoch = 0
        return start_epoch

    def trainable_param(self, trainable_scope):
        for param in self.model.parameters():
            param.requires_grad = False

        trainable_param = []
        for module in trainable_scope.split(','):
            if hasattr(self.model, module):
                # print(getattr(self.model, module))
                for param in getattr(self.model, module).parameters():
                    param.requires_grad = True
                trainable_param.extend(getattr(self.model, module).parameters())
        return trainable_param

    def train_model(self):

        start_epoch = self.start_epoch
        only_eval = False
        only_test = False

        if len(self.cfg.PHASE) == 1 and self.cfg.PHASE[0] == 'eval':
            self.max_epochs = start_epoch + 1
            only_eval = True

        if len(self.cfg.PHASE) == 1 and (self.cfg.PHASE[0] == 'test' ):
            self.max_epochs = start_epoch + 1
            only_test = True

        if len(self.cfg.PHASE) == 1 and (self.cfg.PHASE[0] == 'visualize'):
            self.max_epochs = start_epoch + 1

        warm_up = self.cfg.TRAIN.LR_SCHEDULER.WARM_UP_EPOCHS
        for epoch in iter(range(start_epoch+1, self.max_epochs+1)):
            #learning rate
            sys.stdout.write('\rEpoch {epoch:d}/{max_epochs:d}:\n'.format(epoch=epoch, max_epochs=self.max_epochs))
            if epoch > warm_up and only_test  == False and only_eval == False:
                self.exp_lr_scheduler.step(epoch-warm_up)
            if 'train' in cfg.PHASE:
                self.train_epoch(self.model, self.train_loader, self.optimizer, self.criterion, self.train_writer, epoch, self.use_gpu)
            if epoch % cfg.TRAIN.CHECKPOINTS_EPOCHS == 0 and ((only_test == False) and (only_eval == False)):
                self.save_checkpoints(epoch)

            if ('eval' in cfg.PHASE and epoch%cfg.TRAIN.CHECKPOINTS_EPOCHS == 0) or (only_eval == True):
                with torch.no_grad():
                    if cfg['TEST'].BATCH_SIZE == 2:
                        self.eval_epoch_batch_size_2(self.model, self.eval_loader, self.detector, self.output_dir + str(epoch),
                                        self.use_gpu)
                    else:
                        self.eval_epoch(self.model, self.eval_loader, self.detector, self.output_dir + str(epoch), self.use_gpu,epoch,self.eval_writer)
            if ('test' in cfg.PHASE and epoch%cfg.TRAIN.CHECKPOINTS_EPOCHS == 0)  or (only_test == True):
                with torch.no_grad():
                    if cfg['TEST'].BATCH_SIZE == 2:
                        self.test_epoch_batch_size_2(self.model, self.test_loader, self.detector, self.output_dir + str(epoch),
                                        self.use_gpu, viz_th=self.cfg.POST_PROCESS.VIZ_THRESHOLD)
                    else:
                        self.test_epoch(self.model, self.test_loader, self.detector, self.output_dir + str(epoch),
                                self.use_gpu, viz_th = self.cfg.POST_PROCESS.VIZ_THRESHOLD)
            if 'visualize' in cfg.PHASE:
                with torch.no_grad():
                    self.visualize_epoch(self.model, self.visualize_loader, self.priorbox, self.viz_writer, epoch,  self.use_gpu, self.output_viz_dir + str(epoch))


    def test_model(self):
        previous = self.find_previous()
        if previous:
            for epoch, resume_checkpoint in zip(previous[0], previous[1]):
                if self.cfg.TEST.TEST_SCOPE[0] <= epoch <= self.cfg.TEST.TEST_SCOPE[1]:
                    sys.stdout.write('\rEpoch {epoch:d}/{max_epochs:d}:\n'.format(epoch=epoch, max_epochs=self.cfg.TEST.TEST_SCOPE[1]))
                    self.resume_checkpoint(resume_checkpoint)
                    if 'eval' in cfg.PHASE:
                        with torch.no_grad():
                            self.eval_epoch(self.model, self.eval_loader, self.detector, self.criterion, self.writer, epoch, self.use_gpu)
                    if 'test' in cfg.PHASE:
                        with torch.no_grad():
                            self.test_epoch(self.model, self.test_loader, self.detector, self.output_dir , self.use_gpu,
                                    viz_th = self.cfg.POST_PROCESS.VIZ_THRESHOLD)
                    if 'visualize' in cfg.PHASE:
                        with torch.no_grad():
                            self.visualize_epoch(self.model, self.visualize_loader, self.priorbox, self.writer, epoch,  self.use_gpu)
        else:
            sys.stdout.write('\rCheckpoint {}:\n'.format(self.checkpoint))
            self.resume_checkpoint(self.checkpoint)
            if 'eval' in cfg.PHASE:
                self.eval_epoch(self.model, self.eval_loader, self.detector, self.criterion, self.writer, 0, self.use_gpu)
            if 'test' in cfg.PHASE:
                self.test_epoch(self.model, self.test_loader, self.detector, self.output_dir , self.use_gpu,
                        viz_th = self.cfg.POST_PROCESS.VIZ_THRESHOLD)
            if 'visualize' in cfg.PHASE:
                self.visualize_epoch(self.model, self.visualize_loader, self.priorbox, self.writer, 0,  self.use_gpu)

    def train_epoch(self, model, data_loader, optimizer, criterion, writer, epoch, use_gpu):
        model.train()
        epoch_size = len(data_loader)
        #Below statement resets the data_loader, kind of indication of one epoch finishing.
        batch_iterator = iter(data_loader)

        loc_loss = 0
        total_loss_lb = 0
        conf_loss = 0
        _t = Timer()

        device = torch.device("cuda:0" if use_gpu else "cpu")
        for iteration in iter(range((epoch_size))):

            _t.tic()

            images, targets = next(batch_iterator)
            images = images.to(device)
            with torch.no_grad():
                targets = [anno.to(device) for anno in targets]

            out = model(images)
            out = post_forward(out,self.code_size,self.num_classes)
            # backprop
            optimizer.zero_grad()

            loss_l, loss_c ,loss_lb  = criterion(out, targets)
           # print("Mean Smooth L1 Loss on Extra Features "+str(total_loss_lb_norm))
            # some bugs in coco train2017. maybe the annonation bug.
            if loss_l.item() == float("Inf"):
                continue

            loss = loss_l + loss_c
            loss.backward()

            optimizer.step()

            loc_loss += loss_l.item()
            if self.num_ExtraFeatures>0 :
                total_loss_lb+=loss_lb.item()

            conf_loss += loss_c.item()

            time = _t.toc()
            #loc_loss =0
            #conf_loss =0
            # log per iter
            if self.num_ExtraFeatures>0:
                log = '\r==>Train: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}] || loc_loss: {loc_loss:.4f} cls_loss: {cls_loss:.4f}\r'.format(
                    prograss='#'*int(round(10*iteration/epoch_size)) + '-'*int(round(10*(1-iteration/epoch_size))), iters=iteration, epoch_size=epoch_size,
                    time=time, loc_loss=loss_l.item(),loc_ef=loss_lb.item(), cls_loss=loss_c.item())
            else :
                log = '\r==>Train: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}] || loc_loss: {loc_loss:.4f} cls_loss: {cls_loss:.4f}\r'.format(
                    prograss='#'*int(round(10*iteration/epoch_size)) + '-'*int(round(10*(1-iteration/epoch_size))), iters=iteration, epoch_size=epoch_size,
                    time=time, loc_loss=loss_l.item(), cls_loss=loss_c.item())
            sys.stdout.write(log)
            sys.stdout.flush()

        # log per epoch
        lr = optimizer.param_groups[0]['lr']
        if self.num_ExtraFeatures>0:
            log = '\r==>Train: || Total_time: {time:.3f}s || loc_loss: {loc_loss:.4f} conf_loss: {conf_loss:.4f} || lr: {lr:.6f}\n'.format(lr=lr,
                    time=_t.total_time, loc_loss=loc_loss/epoch_size, loc_ef=total_loss_lb/epoch_size, conf_loss=conf_loss/epoch_size)
        else :
            log = '\r==>Train: || Total_time: {time:.3f}s || loc_loss: {loc_loss:.4f} conf_loss: {conf_loss:.4f} || lr: {lr:.6f}\n'.format(lr=lr,
                    time=_t.total_time, loc_loss=loc_loss/epoch_size, loc_ef=total_loss_lb/epoch_size, conf_loss=conf_loss/epoch_size)
        sys.stdout.write(log)
        sys.stdout.flush()

        # log for tensorboard
        writer.add_scalar('Train/loc_loss', loc_loss/epoch_size, epoch)
        writer.add_scalar('Train/conf_loss', conf_loss/epoch_size, epoch)
        writer.add_scalar('Train/lr', lr, epoch)

    # keep this function for reference, as it is having simple image fetch mechanism without data loader
    # will run with only 2 GPUS.
    def eval_epoch_batch_size_2(self, model, data_loader, detector, output_dir, use_gpu):
        model.eval()

        dataset = data_loader.dataset
        num_images = len(dataset)
        num_classes = detector.num_classes
        all_boxes = [[[] for _ in range(num_images)] for _ in range(num_classes)]
        empty_array = np.transpose(np.array([[],[],[],[],[]]),(1,0))

        _t = Timer()

        batch_size = 2

        scale = [[] for _ in range(batch_size)]

        device = torch.device("cuda" if use_gpu else "cpu")
        #for i in iter(range((10))):
        for i in iter(range(int((num_images + batch_size - 1)/batch_size))):
            _t.tic()

            img0 = dataset.pull_image(batch_size*i)
            if (2*i + 1) < num_images:
                img1 = dataset.pull_image(2*i + 1)
            else:
                img1 = dataset.pull_image(2*i)

            scale[0] = [img0.shape[1], img0.shape[0], img0.shape[1], img0.shape[0]]
            scale[1] = [img1.shape[1], img1.shape[0], img1.shape[1], img1.shape[0]]
            for idx in range(self.num_keypoints):
                scale[0].append(img0.shape[1])
                scale[0].append(img0.shape[0])
                scale[1].append(img1.shape[1])
                scale[1].append(img1.shape[0])

            img0 = dataset.preproc(img0)[0]
            img1 = dataset.preproc(img1)[0]

            # Tapping of image data to test the image from numpy file

            img0 = img0.unsqueeze(0)
            img1 = img1.unsqueeze(0)

            img = torch.cat((img0, img1), 0)
            images = torch.Tensor(img.to(device))

            # forward
            out = model(images)
            out = post_forward(out,self.code_size,self.num_classes)

            if hasattr(model,'softmax'):
                conf_score = model.softmax(out[1].view(-1, self.num_classes))  # conf preds
            else:
                conf_score = model.module.softmax(out[1].view(-1, self.num_classes))  # conf preds

            # detect
            detections = detector.forward([out[0], conf_score])


            # TODO: make it smart:

            for j in range(1, num_classes):
                for batch_i in range(2):
                    cls_dets = list()
                    for det in detections[batch_i][j]:
                        if det[0] > 0:
                            d = det.cpu().numpy()
                            if self.num_ExtraFeatures > 0 :
                                score, box = d[0], d[1:-self.num_ExtraFeatures]
                                box *= scale[batch_i]
                                box = np.append(box, d[-self.num_ExtraFeatures:])
                            else:
                                score, box = d[0], d[1:]
                                box *= scale[batch_i]
                            box = np.append(box, score)
                            cls_dets.append(box)



                    if len(cls_dets) == 0: # if no detection happens then initialize with empty array
                        cls_dets = empty_array

                    if (2 * i + batch_i) < num_images:
                        all_boxes[j][2*i + batch_i] = np.array(cls_dets)
                    else:
                        all_boxes[j][2 * i] = np.array(cls_dets)

            time = _t.toc()

            # log per iter
            sys.stdout.write('\r')
            sys.stdout.flush()

            log = '\r==>Eval: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}]\r'.format(
                    prograss='#'*int(round(10*i*2/num_images)) + '-'*int(round(10*(1-2*i/num_images))), iters=2*i, epoch_size=num_images,
                    time=time)
            sys.stdout.write(log)
            sys.stdout.flush()

        sys.stdout.write('\r')
        sys.stdout.flush()

        log = '\r==>Eval: || Total_time: {time:.3f}s \n'.format(time=_t.total_time)

        sys.stdout.write(log)
        sys.stdout.flush()

        print('Evaluating detections')
        data_loader.dataset.evaluate_detections(all_boxes, output_dir)

    def eval_epoch(self, model, data_loader, detector, output_dir, use_gpu, epoch, writer):
        model.eval()

        dataset = data_loader.dataset

        num_images = len(dataset)
        epoch_size = len(data_loader)

        num_classes = detector.num_classes
        all_boxes = [[[] for _ in range(num_images)] for _ in range(num_classes)]
        empty_array = np.transpose(np.array([[], [], [], [], []]), (1, 0))

        #Below statement resets the data_loader, kind of indication of one epoch finishing.
        batch_iterator = iter(data_loader)

        img0 = dataset.pull_image(0)
        # same scaling is used for all the images
        scale = [img0.shape[1], img0.shape[0], img0.shape[1], img0.shape[0]]
        for idx in range(self.num_keypoints):
            scale.append(img0.shape[1])
            scale.append(img0.shape[0])

        _t = Timer()

        device = torch.device("cuda:0" if use_gpu else "cpu")

        prev_img_count= 0
        for iteration in iter(range((epoch_size))):
        #for iteration in iter(range((10))):

            _t.tic()

            # Below read annotation 'targets' is not used
            images, targets = next(batch_iterator)

            # ideally only gpu number of images should be sufficient. But it is not working.
            # hence making minimum number of images as batch size
            #if images.shape[0] < self.num_gpu:
            if images.shape[0] < self.cfg.TEST.BATCH_SIZE:
                img_dup = images[-1]
                for _ in range(self.cfg.TEST.BATCH_SIZE - images.shape[0]):
                    images = torch.cat((images,img_dup.unsqueeze(0)), dim=0)

            images = images.to(device)

            # forward
            out = model(images)
            out = post_forward(out,self.code_size,self.num_classes)

            if hasattr(model,'softmax'):
                conf_score = model.softmax(out[1].view(-1, self.num_classes))  # conf preds
            else:
                conf_score = model.module.softmax(out[1].view(-1, self.num_classes))  # conf preds

            # detect
            detections = detector.forward([out[0], conf_score])

            # TODO: make it smart:
            # in last iteration that number of images will be less than number of batch size

            cur_batch_size = images.shape[0]

            extra_det_prms = self.num_ExtraFeatures + (self.tag_det_prior_box_id == True)

            for j in range(1, num_classes):
                for batch_i in range(cur_batch_size):
                    cls_dets = list()
                    for det in detections[batch_i][j]:
                        if det[0] > 0: # All detection are collected here, later SCORE_THRESHOLD is used to filter these detections
                            d = det.cpu().numpy()
                            if extra_det_prms > 0 :
                                score, box = d[0], d[1:-extra_det_prms]
                                box *= scale
                                box = np.append(box, d[-extra_det_prms:])
                            else:
                                score, box = d[0], d[1:]
                                box *= scale

                            box = np.append(box, score)
                            cls_dets.append(box)
                    if len(cls_dets) == 0:  # if no detection happens then initialize with empty array
                        cls_dets = empty_array

                    if (prev_img_count + batch_i) < num_images:
                        all_boxes[j][prev_img_count + batch_i] = np.array(cls_dets)

            prev_img_count = prev_img_count + cur_batch_size

            time = _t.toc()

            # log per iter
            log = '\r==>Eval: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}] \r'.format(
                prograss='#'*int(round(10*iteration/epoch_size)) + '-'*int(round(10*(1-iteration/epoch_size))), iters=iteration, epoch_size=epoch_size,
                time=time)

            sys.stdout.write(log)
            sys.stdout.flush()

        sys.stdout.write('\r')
        sys.stdout.flush()

        log = '\r==>Eval: || Total_time: {time:.3f}s \n'.format(time=_t.total_time)

        sys.stdout.write(log)
        sys.stdout.flush()

        print('Evaluating detections')
        aps, kp_errs, ef_errs = data_loader.dataset.evaluate_detections(all_boxes, output_dir)

        # log for tensorboard
        for cls in range(self.num_classes-1):
            writer.add_scalar('Eval/map/{}'.format(cls), aps[cls], epoch)
            writer.add_scalar('Eval/kp_err/{}'.format(cls), kp_errs[cls], epoch)


    #initialize dictionary for "int" to "RGB-color" mapping
    def init_class_to_col_dict(self):
      #diff color for diff class
      diff_col_diff_class = True
      if diff_col_diff_class:
        HSV_COLOR_MAP = initHSVTable()
        #depth above hsv_color_idx_max will be mapped to index hsv_color_idx_max
        hsv_color_idx_max = 48
        #how much two classes should look differently in color
        #make sure last index in dict * sep_fac does not exceed hsv_color_idx_max
        sep_fac = 7
        #class -> color_idx
        #color_dict = {'car': HSV_COLOR_MAP[0*sep_fac], 'bus': HSV_COLOR_MAP[1*sep_fac], 'train': HSV_COLOR_MAP[2*sep_fac], 'person': HSV_COLOR_MAP[3*sep_fac], 'bicycle': HSV_COLOR_MAP[4*sep_fac], 'motorbike': HSV_COLOR_MAP[5*sep_fac], 'trafficsign': HSV_COLOR_MAP[6*sep_fac] }
        color_dict = {'vehicle': HSV_COLOR_MAP[0*sep_fac], 'person': HSV_COLOR_MAP[1*sep_fac], 'trafficsign': HSV_COLOR_MAP[2*sep_fac] }
        return color_dict, HSV_COLOR_MAP

    #show polygon formed by keypoints
    def showPolygons(self, img=[], dets=[], det_idx = 0, color_map=[], class_idx=0):
        polyPoints = []
        for kpt in range(int((dets.shape[1] - 5) / 2)):
            polyPoints.append([int(dets[det_idx, 4 + 2*kpt]), int(dets[det_idx, 4 + 2*kpt + 1])])
        polyPoints = np.asarray(polyPoints, dtype=np.int32)
        polyPoints = polyPoints.reshape([len(polyPoints),2])
        #how much two classes should look differently in color
        #make sure last index in dict * sep_fac does not exceed hsv_color_idx_max
        sep_fac = 16
        poly_color = color_map[class_idx*sep_fac]
        cv2.polylines(img, [polyPoints], True,(int(poly_color[0]), int(poly_color[1]), int(poly_color[2])),thickness=2, lineType=cv2.LINE_AA)
        return

    def test_epoch_batch_size_2(self, model, data_loader, detector, output_dir, use_gpu, viz_th=0.5):
        model.eval()
        dataset = data_loader.dataset
        num_images = len(dataset)
        num_classes = detector.num_classes
        all_boxes = [[[] for _ in range(num_images)] for _ in range(num_classes)]
        empty_array = np.transpose(np.array([[],[],[],[],[]]),(1,0))

        _t = Timer()
        scale = [[],[]] # bcz batch size is 2

        device = torch.device("cuda" if use_gpu else "cpu")
        write_ground_truth= False
        write_image = True
        color_dict, hsv_color_map = self.init_class_to_col_dict()
        for i in iter(range(int((num_images + 1)/2))):
        #for i in iter(range((1))):
            img0 = dataset.pull_image(2*i)
            if (2*i + 1) < num_images:
                img1 = dataset.pull_image(2*i + 1)
            else:
                img1 = dataset.pull_image(2*i)

            scale[0] = [img0.shape[1], img0.shape[0], img0.shape[1], img0.shape[0]]
            scale[1] = [img1.shape[1], img1.shape[0], img1.shape[1], img1.shape[0]]
            for idx in range(self.num_keypoints):
                scale[0].append(img0.shape[1])
                scale[0].append(img0.shape[0])
                scale[1].append(img1.shape[1])
                scale[1].append(img1.shape[0])

            img0_processed = dataset.preproc(img0)[0]
            img1_processed = dataset.preproc(img1)[0]

            # Tapping of image data to test the image from numpy file
            #np.save('/user/a0393749/temp/input_ref.npy', img0)
            #img0 = np.load('/user/a0393749/temp/input.npy')
            #img0 = torch.from_numpy(img0)
            #img1 = np.load('/user/a0393749/temp/input.npy')
            #img1 = torch.from_numpy(img1)

            img0_processed = img0_processed.unsqueeze(0)
            img1_processed = img1_processed.unsqueeze(0)
            img_processed = torch.cat((img0_processed, img1_processed), 0)

            img = [img0, img1]

            images = torch.Tensor(img_processed.to(device))

            _t.tic()
            # forward
            out = model(images)
            out = post_forward(out,self.code_size,self.num_classes)

            if hasattr(model, 'softmax'):
                conf_score = model.softmax(out[1].view(-1, self.num_classes))  # conf preds
            else:
                conf_score = model.module.softmax(out[1].view(-1, self.num_classes))  # conf preds

            # detect
            detections = detector.forward([out[0], conf_score])

            time = _t.toc()

            # TODO: make it smart:

            for j in range(1, num_classes):
                for batch_i in range(2):
                    cls_dets = list()
                    for det in detections[batch_i][j]:
                        #print("det:", det)
                        if det[0] > 0:
                            d = det.cpu().numpy()
                            if self.num_ExtraFeatures > 0 :
                                score, box = d[0], d[1:-self.num_ExtraFeatures]
                                box *= scale[batch_i]
                                box = np.append(box, d[-self.num_ExtraFeatures:])
                            else:
                                score, box = d[0], d[1:]
                                box *= scale[batch_i]

                            box = np.append(box, score)
                            cls_dets.append(box)

                    if len(cls_dets) == 0: # if no detection happens then initialize with empty array
                        cls_dets = empty_array

                    if (2 * i + batch_i) < num_images:
                        all_boxes[j][2*i + batch_i] = np.array(cls_dets)
                    else:
                        all_boxes[j][2 * i] = np.array(cls_dets)

            if write_image == True:
                font = cv2.FONT_HERSHEY_SIMPLEX
                fontScale = 0.8
                fontColor = (0, 0, 255)
                lineType = 1
                for batch_i in range(2):
                    if (2 * i + batch_i) < num_images:
                        img_cur = img[batch_i]
                        if write_ground_truth == True:
                            anno = dataset.pull_anno(2 * i + batch_i)
                        for class_idx in range(1, num_classes):

                            dets = all_boxes[class_idx][2*i + batch_i]

                            for k in range(dets.shape[0]):
                                if dets[k, -1] >= viz_th:
                                    show_box_and_kp = True
                                    #show keypoints and bounding box
                                    if show_box_and_kp:
                                        if write_ground_truth == True:
                                            cv2.rectangle(img_cur,(int(anno[0][0]),int(anno[0][1])), (int(anno[0][2]),int(anno[0][3])),(0, 255, 0), 1)
                                        cv2.rectangle(img_cur,(int(dets[k, 0]),int(dets[k, 1])), (int(dets[k, 2]),int(dets[k, 3])),(0, 0, 255), 1)
                                        for kpt in range(int((dets.shape[1] - 5) / 2)):
                                            if write_ground_truth == True:
                                                cv2.circle(img_cur, (int(anno[0, 4 + 2*kpt]), int(anno[0, 4 + 2*kpt + 1])), 3, (0, 255, 0), -1)

                                            if  not (((dets[k, 4 + 2*kpt] - (dets[k, 0] - 10.0)) < 0.0) or ((dets[k, 4 + 2*kpt] - (dets[k, 2] + 10.0))> 0.0) or  ((dets[k, 4 + 2*kpt+1] - (dets[k, 1] - 10.0)) < 0.0) or ((dets[k, 4 + 2*kpt+1] - (dets[k, 3] + 10.0))> 0.0)):
                                                cv2.circle(img_cur, (int(dets[k, 4 + 2*kpt]), int(dets[k, 4 + 2*kpt + 1])), 3, (0, 0, 255), -1)


    #                                   if write_ground_truth = True:
    #                                       cv2.rectangle(img_cur,(int(dets[k, 0]),int(dets[k, 1])), (int(dets[k, 2]),int(dets[k, 3])),(0, 0, 255), 1)
    #                                        for kpt in range(int((dets.shape[1] - 5) / 2)):
    #                                            cv2.circle(img_cur, (int(dets[k, 4 + 2*kpt]), int(dets[k, 4 + 2*kpt + 1])), 3, (255, 0, 0), -1)
                                        #for extra in range(self.num_ExtraFeatures):
                                         #    feat_text =  "{:5.2f}".format(dets[k,-self.num_ExtraFeatures+extra-1])
                                         #    cv2.putText(img_cur,feat_text,
                                         #                (10, 20+extra*30),
                                         #                cv2.FONT_HERSHEY_SIMPLEX,
                                         #                1.0,
                                         #                fontColor,  #TO WRITE FEATURES
                                         #                thickness=2)
                                    else:
                                       #show polygon formed by keypoints
                                       self.showPolygons(img=img_cur, dets=dets, det_idx = k, color_map=hsv_color_map, class_idx = class_idx)

                                    for extra in range(self.num_ExtraFeatures):
                                        cv2.putText(img_cur, str(
                                            dets[0, -self.num_ExtraFeatures + extra - 1])[0:5],
                                                    (30 + extra * 80, 30),
                                                    font,
                                                    fontScale,
                                                    (0,0,255),  # TO WRITE FEATURES
                                                    lineType)
                                        if write_ground_truth == True:
                                            cv2.putText(img_cur, str(
                                                anno[0, -self.num_ExtraFeatures + extra - 1])[
                                                                 0:5],
                                                        (30 + extra * 80, 30),
                                                        font,
                                                        fontScale,
                                                        (0,255,0),  # TO WRITE FEATURES
                                                        lineType)
                        image = data_loader.dataset.ids[2*i + batch_i]
                        dest_image = os.path.join(output_dir, os.path.basename(image ))
                        os.makedirs(os.path.dirname(dest_image), exist_ok=True)
                        print("Writing Detection file", dest_image, )
                        img_cur = cv2.cvtColor(img_cur, cv2.COLOR_BGR2RGB)
                        cv2.imwrite(dest_image,img_cur, [int(cv2.IMWRITE_JPEG_QUALITY), 97])
            # log per iter
            sys.stdout.write('\r')
            sys.stdout.flush()

            log = '\r==>Test: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}]\r'.format(
                    prograss='#'*int(round(10*i/num_images)) + '-'*int(round(10*(1-i/num_images))), iters=2*i, epoch_size=num_images,
                    time=time)
            sys.stdout.write(log)
            sys.stdout.flush()

        data_loader.dataset._write_voc_results_file(all_boxes, output_dir,num_extra_features=self.num_ExtraFeatures)

    def test_epoch(self, model, data_loader, detector, output_dir, use_gpu, viz_th=0.5):
        model.eval()

        dataset = data_loader.dataset

        num_images = len(dataset)
        epoch_size = len(data_loader)

        num_classes = detector.num_classes
        all_boxes = [[[] for _ in range(num_images)] for _ in range(num_classes)]
        empty_array = np.transpose(np.array([[], [], [], [], []]), (1, 0))

        #Below statement resets the data_loader, kind of indication of one epoch finishing.
        batch_iterator = iter(data_loader)

        img0 = dataset.pull_image(0)
        # same scaling is used for all the images
        scale = [img0.shape[1], img0.shape[0], img0.shape[1], img0.shape[0]]
        print(scale)
        for idx in range(self.num_keypoints):
            scale.append(img0.shape[1])
            scale.append(img0.shape[0])

        _t = Timer()

        device = torch.device("cuda:0" if use_gpu else "cpu")

        prev_img_count= 0
        write_image = True
        write_detection_per_image = True
        write_ground_truth= True
        color_dict, hsv_color_map = self.init_class_to_col_dict()
        winner_anchor_box = np.zeros((0, 2))

        for iteration in iter(range((epoch_size))):
            _t.tic()

            # Below read annotation 'targets' is not used
            images, targets = next(batch_iterator)

            # ideally only gpu number of images should be sufficient. But it is not working.
            # hence making minimum number of images as batch size
            #if images.shape[0] < self.num_gpu:
            if images.shape[0] < self.cfg.TEST.BATCH_SIZE:
                img_dup = images[-1]
                for _ in range(self.cfg.TEST.BATCH_SIZE - images.shape[0]):
                    images = torch.cat((images,img_dup.unsqueeze(0)), dim=0)

            images = images.to(device)

            # forward
            out = model(images)
            out = post_forward(out,self.code_size,self.num_classes)

            if hasattr(model,'softmax'):
                conf_score = model.softmax(out[1].view(-1, self.num_classes))  # conf preds
            else:
                conf_score = model.module.softmax(out[1].view(-1, self.num_classes))  # conf preds

            # detect
            detections = detector.forward([out[0], conf_score])

            # TODO: make it smart:
            # in last iteration that number of images will be less than number of batch size

            cur_batch_size = images.shape[0]
            orig_det_prms  = self.num_ExtraFeatures + (self.detector.tag_det_prior_box_id == True)
            extra_det_prms = self.num_ExtraFeatures + 4*(self.detector.tag_det_prior_box_id == True) # four extra parameter loc_y, loc_x, head#, ar

            for j in range(1, num_classes):
                for batch_i in range(cur_batch_size):
                    cls_dets = list()
                    for det in detections[batch_i][j]:
                        if det[0] > viz_th:
                            d = det.cpu().numpy()
                            if extra_det_prms > 0 :
                                score, box = d[0], d[1:-orig_det_prms]
                                box *= scale
                                if self.detector.tag_det_prior_box_id == False:
                                    box = np.append(box, d[-orig_det_prms:])
                                else :
                                    prior_box_num = int(d[-orig_det_prms:][0])
                                    box = np.append(box, self.prior_box_map[prior_box_num])
                                    winner_anchor_box = np.append(winner_anchor_box,[self.prior_box_map[prior_box_num][-2:]],axis=0)
                            else:
                                score, box = d[0], d[1:]
                                box *= scale

                            box = np.append(box, score)
                            cls_dets.append(box)

                    if len(cls_dets) == 0:  # if no detection happens then initialize with empty array
                        cls_dets = empty_array

                    if (prev_img_count + batch_i) < num_images:
                        all_boxes[j][prev_img_count + batch_i] = np.array(cls_dets)

            if write_detection_per_image == True:

                for batch_i in range(cur_batch_size):

                    curFile_content = ''

                    if (prev_img_count + batch_i) < num_images:
                        for class_idx in range(1, num_classes):
                            dets = all_boxes[class_idx][prev_img_count + batch_i]
                            class_name = dataset._classes[class_idx]
                            for k in range(dets.shape[0]):
                                if dets[k, -1] >= viz_th:
                                    top_left_x = str(dets[k, 0]) + ' '
                                    top_left_y = str(dets[k, 1]) + ' '
                                    bot_right_x= str(dets[k, 2]) + ' '
                                    bot_right_y= str(dets[k, 3]) + ' '

                                    num_keypoints = dets[k][4:-(1+extra_det_prms):2].size

                                    kp = np.empty([0, 2])

                                    for kp_x,kp_y in zip(dets[k][4:-(1+extra_det_prms):2], dets[k][5:-extra_det_prms:2]):
                                        kp = np.append(kp, [[kp_x, kp_y]], axis=0)

                                    curLine = class_name + ' 0 0 0 ' + top_left_x + top_left_y + bot_right_x + bot_right_y + '0 0 0 0 0 0 0 ' + str(dets[k, -1]) + ' '

                                    for i in range(num_keypoints):
                                        curLine = curLine + str(dets[k][4:-(1+extra_det_prms):2][i]) + ' '
                                        curLine = curLine + str(dets[k][5:-(1+extra_det_prms):2][i]) + ' '

                                    for feat in dets[k,-extra_det_prms-1:-1]:
                                        curLine = curLine + str(feat) + ' '

                                    curLine = curLine + "\n"

                                    curFile_content = curFile_content + curLine

                        image = data_loader.dataset.ids[prev_img_count + batch_i]

                        sub_dir_name, bare_file_name = os.path.split(image)
                        bare_file_name = os.path.splitext(bare_file_name)[0]

                        det_file_name = os.path.join(os.path.join(output_dir, sub_dir_name), 'test_detOp_' + bare_file_name + '.txt')

                        os.makedirs(os.path.dirname(det_file_name), exist_ok=True)
                        f_out = open(det_file_name, "w")
                        print("Writing Detection text file", det_file_name)
                        f_out.write(curFile_content)
                        f_out.close()

                # print(kp)

            if write_image == True:
                font                   = cv2.FONT_HERSHEY_SIMPLEX
                fontScale              = 0.45
                fontColor              = (0,255,255)
                lineType               = 1
                for batch_i in range(cur_batch_size):
                    if (prev_img_count + batch_i) < num_images:
                        img_cur = dataset.pull_image(prev_img_count + batch_i)
                        for class_idx in range(1, num_classes):

                            dets = all_boxes[class_idx][prev_img_count + batch_i]

                            for k in range(dets.shape[0]):
                                if dets[k, -1] >= viz_th and self.cfg.DATASET.OBJ_CLASSES[class_idx] in self.cfg.POST_PROCESS.VIZ_CLASS:
                                    show_box_and_kp = False
                                    disp_class_name = False
                                    #show keypoints and bounding box
                                    if show_box_and_kp:
                                        cv2.rectangle(img_cur,(int(dets[k, 0]),int(dets[k, 1])), (int(dets[k, 2]),int(dets[k, 3])),(0, 0, 255), 1)
                                        for kpt in range(int((dets.shape[1] - 5 - self.num_ExtraFeatures) / 2)):
                                           cv2.circle(img_cur, (int(dets[k, 4 + 2*kpt]), int(dets[k, 4 + 2*kpt + 1])), 3, (255, 0, 0), -1)
                                           if disp_class_name == True:
                                             cv2.putText(img_cur,str(dataset._classes[class_idx]),
                                                         (10 + self.num_ExtraFeatures * 100, 10),
                                                         font,
                                                         fontScale,
                                                         fontColor,  #TO WRITE CLASS
                                                         lineType)
                                    else:
                                       #show polygon formed by keypoints
                                       if extra_det_prms > 0:
                                           self.showPolygons(img=img_cur, dets=dets[:, :-extra_det_prms], det_idx=k,
                                                             color_map=hsv_color_map, class_idx=class_idx)
                                       else:
                                           self.showPolygons(img=img_cur, dets=dets[:, :], det_idx=k,
                                                             color_map=hsv_color_map, class_idx=class_idx)

                                    score = dets[k, -1]
                                    fontColorScore = (0,0,255)

                                    center_loc_x = int(np.mean(dets[k, 0:4:2]))
                                    center_loc_y = int(np.mean(dets[k, 1:4:2]))

                                    cv2.putText(img_cur, '{:3.2f}'.format(score), # 3 decimalplaces are displayed
                                               (center_loc_x, center_loc_y), #display the score at center of the box
                                                font,
                                                fontScale,
                                                fontColorScore,
                                                lineType)

                                    for extra in range(extra_det_prms):
                                        cv2.putText(img_cur,
                                                    '{:4.1f}'.format(dets[k, -extra_det_prms + extra - 1]),
                                                    (center_loc_x + 45, center_loc_y),
                                                    # display the score at first key point location
                                                    font,
                                                    fontScale,
                                                    fontColor,  # TO WRITE FEATURES
                                                    lineType)

                        image = data_loader.dataset.ids[prev_img_count + batch_i]
                        image = os.path.splitext(image)[0]
                        dest_image = os.path.join(output_dir, image)
                        dest_image = dest_image + '.png'
                        os.makedirs(os.path.dirname(dest_image), exist_ok=True)
                        print("Writing Detection image file", dest_image)

                        if self.cfg.DATASET.BGR_OR_RGB == True:
                            img_cur = cv2.cvtColor(img_cur, cv2.COLOR_RGB2BGR)

                        cv2.imwrite(dest_image,img_cur, [int(cv2.IMWRITE_JPEG_QUALITY), 97])

            prev_img_count = prev_img_count + cur_batch_size

            time = _t.toc()

            # log per iter
            log = '\r==>Test: || {iters:d}/{epoch_size:d} in {time:.3f}s [{prograss}] \r'.format(
                prograss='#'*int(round(10*iteration/epoch_size)) + '-'*int(round(10*(1-iteration/epoch_size))), iters=iteration, epoch_size=epoch_size,
                time=time)

            sys.stdout.write(log)
            sys.stdout.flush()

        if winner_anchor_box.shape[0] > 0:
            uniq, count = np.unique(winner_anchor_box, axis=0, return_counts=True)
            for ancr, cnt in zip(uniq,count):
                print(ancr,cnt)

        sys.stdout.write('\r')
        sys.stdout.flush()

        log = '\r==>Test: || Total_time: {time:.3f}s \n'.format(time=_t.total_time)

        sys.stdout.write(log)
        sys.stdout.flush()

        print('Writing detections text file')
        data_loader.dataset._write_voc_results_file(all_boxes, output_dir , self.num_ExtraFeatures)

    def visualize_epoch(self, model, data_loader, priorbox, writer, epoch, use_gpu, log_dir=None):

        model.eval()

        os.makedirs(log_dir, exist_ok=True)

        my_file_gt_stat = open('{}/gt_stat_{}.txt'.format(log_dir,epoch), "w")

        my_file_gt_stat.write("image_id   gt_id  #Anchor_matched\n")

        save_detail_stat=True
        write_image=True

        for img_index in range(len(data_loader.dataset)):

            # get img
            image = data_loader.dataset.pull_image(img_index)
            annos = data_loader.dataset.pull_anno(img_index)
            truth = annos[:,:-1]/[image.shape[1],image.shape[0],image.shape[1],image.shape[0],
                                image.shape[1], image.shape[0], image.shape[1], image.shape[0],
                                image.shape[1], image.shape[0], image.shape[1], image.shape[0]]
            labels = annos[:,-1]

            if(len(labels) > 0):
                best_truth_idx,best_prior_idx,best_truth_overlap,best_prior_overlap=match(self.cfg.MATCHER.MATCHED_THRESHOLD,
                                                                                          self.cfg.MATCHER.UNMATCHED_THRESHOLD,
                                                                                          torch.Tensor(truth), self.priors, self.cfg.MATCHER.VARIANCE,
                                                                                          torch.Tensor(labels), None, None, 0,
                                                                                          boxes_from_kp=False, numextrafeatures=0,
                                                                                          alone_gt_boost_th=self.cfg.MATCHER.ALONE_GT_BOOST_TH)

                if save_detail_stat == True:
                    best_truth_positive_idx = (best_truth_overlap >= self.cfg.MATCHER.MATCHED_THRESHOLD)

                    np.savetxt('{}/{}_best_truth_matched_stat.txt'.format(log_dir,img_index), (torch.arange(best_truth_idx.shape[0])[best_truth_positive_idx].cpu().numpy(),
                                                                                       best_truth_idx[best_truth_positive_idx].cpu().numpy(),
                                                                                       best_truth_overlap[best_truth_positive_idx].cpu().numpy()), fmt="%06.1f")

                    best_truth_positive_idx = (best_truth_overlap >= self.cfg.MATCHER.UNMATCHED_THRESHOLD) & (best_truth_overlap < self.cfg.MATCHER.MATCHED_THRESHOLD)

                    np.savetxt('{}/{}_best_truth_near_matched_stat.txt'.format(log_dir,img_index), (torch.arange(best_truth_idx.shape[0])[best_truth_positive_idx].cpu().numpy(),
                                                                                       best_truth_idx[best_truth_positive_idx].cpu().numpy(),
                                                                                       best_truth_overlap[best_truth_positive_idx].cpu().numpy()), fmt="%06.1f")

                    np.savetxt('{}/{}_best_prior_stat.txt'.format(log_dir,img_index), (best_prior_idx.cpu().numpy(), best_prior_overlap.cpu().numpy()), fmt="%06.1f")

                for idx,_ in enumerate(annos):

                    tot_anchor_matches = ((best_truth_idx == idx) & (best_truth_overlap >= self.cfg.MATCHER.MATCHED_THRESHOLD)).sum()
                    tot_anchor_prob_matches = ((best_truth_idx == idx) & (best_truth_overlap >= self.cfg.MATCHER.UNMATCHED_THRESHOLD) & (best_truth_overlap < self.cfg.MATCHER.MATCHED_THRESHOLD)).sum()

                    my_file_gt_stat.write("{} {} {} {} \n".format(img_index, idx, tot_anchor_matches, tot_anchor_prob_matches))

                    if tot_anchor_matches == 0:
                        print('Frame Idx {}, ground truth id {} did not match with any anchor box'.format(img_index, idx))

                # visualize archor box
                if write_image == True:

                    image_1 = image.copy()
                    viz_prior_box(writer, priorbox, image, epoch, best_truth_idx, best_truth_overlap, self.cfg.MATCHER.MATCHED_THRESHOLD, log_dir=log_dir,img_index=img_index)

                    font = cv2.FONT_HERSHEY_SIMPLEX
                    fontScale = 0.45
                    fontColor = (255, 255, 0)
                    lineType = 1

                    for idx, anno in enumerate(annos):
                        class_idx = int(anno[-1])
                        sep_fac = 7
                        hsv_color_map = initHSVTable()
                        poly_color = hsv_color_map[class_idx * sep_fac]

                        polyPoints = []

                        for kpt in range(4):
                            polyPoints.append([int(anno[4 + 2 * kpt]), int(anno[4 + 2 * kpt + 1])])

                        polyPoints = np.asarray(polyPoints, dtype=np.int32)
                        polyPoints = polyPoints.reshape([len(polyPoints), 2])

                        cv2.polylines(image_1, [polyPoints], True, (int(poly_color[0]), int(poly_color[1]), int(poly_color[2])),
                                      thickness=2, lineType=cv2.LINE_AA)

                        center_loc_x = int(np.mean(anno[0:4:2]))
                        center_loc_y = int(np.mean(anno[1:4:2]))

                        cv2.putText(image_1, '{:02d}'.format(idx),  # 3 decimalplaces are displayed
                                    (center_loc_x -45, center_loc_y),  # display the score at center of the box
                                    font,
                                    fontScale,
                                    fontColor,
                                    lineType)
                        if best_prior_overlap[idx] > self.cfg.MATCHER.MATCHED_THRESHOLD:

                            cv2.putText(image_1, '{:04d}'.format(best_prior_idx[idx].cpu().numpy()),  # 3 decimalplaces are displayed
                                        (center_loc_x, center_loc_y),  # display the score at center of the box
                                        font,
                                        fontScale,
                                        fontColor,
                                        lineType)

                            cv2.putText(image_1, '{:3.2f}'.format(best_prior_overlap[idx].cpu().numpy()),  # 3 decimalplaces are displayed
                                        (center_loc_x + 45, center_loc_y),  # display the score at center of the box
                                        font,
                                        fontScale,
                                        fontColor,
                                        lineType)

                    writer.add_image('ground_truth_map', image_1, epoch)

                    if log_dir != None:
                        cv2.imwrite('{}/{}_gt_box.png'.format(log_dir,img_index),image_1)
        my_file_gt_stat.close()
            # get preproc
            #preproc = data_loader.dataset.preproc
            #preproc.add_writer(writer, epoch)
            # preproc.p = 0.6
            #device = torch.device('cuda' if use_gpu else 'cpu')
            # preproc image & visualize preprocess prograss
            #with torch.no_grad():
            #    images = Variable(preproc(image, anno)[0].unsqueeze(0)).to(device)


            # visualize feature map in base and extras
            #base_out = viz_module_feature_maps(writer, model.module.base, [images], module_name='base', epoch=epoch)
            #extras_out = viz_module_feature_maps(writer, model.module.extras, [base_out[13]], module_name='extras', epoch=epoch)
            #conf_out = viz_module_feature_maps(writer, model.module.conf, [base_out[11], base_out[13], extras_out[0], extras_out[1], extras_out[2], extras_out[3]], module_name='conf', epoch=epoch)

            # visualize feature map in feature_extractors
            #viz_feature_maps(writer, model(images, 'feature'), module_name='feature_extractors', epoch=epoch)

            #model.train()
            #images.requires_grad = True
            #images.volatile=False
            #base_out = viz_module_grads(writer, model, model.module.base, images, images, preproc.means, module_name='base', epoch=epoch)

            # TODO: add more...


    def configure_optimizer(self, trainable_param, cfg):
        if cfg.OPTIMIZER == 'sgd':
            optimizer = optim.SGD(trainable_param, lr=cfg.LEARNING_RATE,
                        momentum=cfg.MOMENTUM, weight_decay=cfg.WEIGHT_DECAY)
        elif cfg.OPTIMIZER == 'rmsprop':
            optimizer = optim.RMSprop(trainable_param, lr=cfg.LEARNING_RATE,
                        momentum=cfg.MOMENTUM, alpha=cfg.MOMENTUM_2, eps=cfg.EPS, weight_decay=cfg.WEIGHT_DECAY)
        elif cfg.OPTIMIZER == 'adam':
            optimizer = optim.Adam(trainable_param, lr=cfg.LEARNING_RATE,
                        betas=(cfg.MOMENTUM, cfg.MOMENTUM_2), eps=cfg.EPS, weight_decay=cfg.WEIGHT_DECAY)
        else:
            AssertionError('optimizer can not be recognized.')
        return optimizer


    def configure_lr_scheduler(self, optimizer, cfg):
        if cfg.SCHEDULER == 'step':
            scheduler = lr_scheduler.StepLR(optimizer, step_size=cfg.STEPS[0], gamma=cfg.GAMMA)
        elif cfg.SCHEDULER == 'multi_step':
            scheduler = lr_scheduler.MultiStepLR(optimizer, milestones=cfg.STEPS, gamma=cfg.GAMMA)
        elif cfg.SCHEDULER == 'exponential':
            scheduler = lr_scheduler.ExponentialLR(optimizer, gamma=cfg.GAMMA)
        elif cfg.SCHEDULER == 'SGDR':
            scheduler = lr_scheduler.CosineAnnealingLR(optimizer, T_max=cfg.MAX_EPOCHS)
        else:
            AssertionError('scheduler can not be recognized.')
        return scheduler


    def export_graph(self):
        self.model.train(False)
        dummy_input = Variable(torch.randn(1, 3, cfg.MODEL.IMAGE_SIZE[0], cfg.MODEL.IMAGE_SIZE[1])).cuda()
        # Export the model
        torch_out = torch.onnx._export(self.model,             # model being run
                                       dummy_input,            # model input (or a tuple for multiple inputs)
                                       "graph.onnx",           # where to save the model (can be a file or file-like object)
                                       export_params=True)     # store the trained parameter weights inside the model file
        # if not os.path.exists(cfg.EXP_DIR):
        #     os.makedirs(cfg.EXP_DIR)
        # self.writer.add_graph(self.model, (dummy_input, ))


def train_model():
    s = Solver()

    if s.use_gpu:
        device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    else:
        device = "cpu"

    if cfg.TRAIN.QUANTIZE == True:
        from pytorch_jacinto_ai.xnn.quantize import QuantTrainModule
        # assert s.use_gpu==False or torch.cuda.device_count() == 1, 'multi gpu is not supported in the quantized training'
        dummy_input=torch.rand((1,3,cfg.MODEL.IMAGE_SIZE[0], cfg.MODEL.IMAGE_SIZE[1])).cuda()
        s.model = QuantTrainModule(s.model, dummy_input=dummy_input)

    # Loading of previous checkpoint TII
    previous = s.find_previous()
    start_epoch = 0
    print("Now checking which model to load")
    print("cfg.RESUME_CHECKPOINT :", cfg.RESUME_CHECKPOINT)
    print("previous: ", previous)
    if previous and (cfg.CHECK_PREVIOUS == True): # if previous checkpoint is available then supercede the --model argument
        start_epoch = previous[0][-1]
        print('Resuming from the check point {}'.format(previous[1][-1]))
        s.resume_checkpoint(previous[1][-1])
    elif cfg.RESUME_CHECKPOINT != \
            '':
        print('Resuming from the check point {}'.format(cfg.RESUME_CHECKPOINT))
        s.resume_checkpoint(cfg.RESUME_CHECKPOINT)
    else:
        print("Calling s.initialize")
        start_epoch = s.initialize()

    s.start_epoch = start_epoch
    ## Multiple GPU enabling
    if s.use_gpu:
        print('Utilize GPUs for computation')

        print('Have {} GPU available'.format(torch.cuda.device_count()))
        if (torch.cuda.device_count() > 1) and cfg.TEST.WRITE_LAYER_IP_OP == False: # done enable multi gpu in tensor output flow
            print("Let's use {} GPUs".format(torch.cuda.device_count()))
            s.model = torch.nn.DataParallel(s.model)
        #else:
        #    s.model = s.model.cuda()

        # move prior boxes and model parameter to GPU TII
        cudnn.benchmark = True

    s.priors.to(device)
    s.model.to(device)

    if cfg.TEST.WRITE_LAYER_IP_OP and 'test' in cfg.PHASE and len(
            cfg.PHASE) == 1:  # execute below in only test phase to avoid data getting written in train and eval phase
        tensor_write_hook_en = True
    else:
        tensor_write_hook_en = False

    tensor_write_hook_en = False

    if tensor_write_hook_en == True:
        for i, (name, module) in enumerate(s.model.module.base.named_modules()):
            module.name = 'PSD_VD_SSD_base_' + name+'_'+str(i)
            print(module.name)
            module.register_forward_hook(write_tensor_hook_function)

        for i, (name, module) in enumerate(s.model.module.extras.named_modules()):
            module.name = 'PSD_VD_SSD_Extra_' + name+'_'+str(i)
            print(module.name)
            module.register_forward_hook(write_tensor_hook_function)

        for i, (name, module) in enumerate(s.model.module.loc.named_modules()):
            module.name = 'PSD_VD_SSD_loc_' + name+'_'+str(i)
            print(module.name)
            module.register_forward_hook(write_tensor_hook_function)

        for i, (name, module) in enumerate(s.model.module.conf.named_modules()):
            module.name = 'PSD_VD_SSD_conf_' + name+'_'+str(i)
            print(module.name)
            module.register_forward_hook(write_tensor_hook_function)

    save_start_model = False

    if save_start_model == True:
      s.save_checkpoints(-1,-1)

    s.train_model()
    return True

def test_model():
    s = Solver()
    s.test_model()
    return True
