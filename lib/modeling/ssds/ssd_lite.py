# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#==============================================================================
#Some parts of the code are borrowed from: https://github.com/ShuangXieIrene/ssds.pytorch
#with the following license:

#MIT License

#Copyright (c) 2018 Irene

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import torch
import torch.nn as nn
import numpy as np
#import torch.nn.functional as F
from torch.autograd import Variable

import os

from lib.layers import *

class SSDLite(nn.Module):
    """Single Shot Multibox Architecture
    See: https://arxiv.org/pdf/1512.02325.pdf for more details.

    Args:
        phase: (string) Can be "eval" or "train" or "feature"
        base: base layers for input
        extras: extra layers that feed to multibox loc and conf layers
        head: "multibox head" consists of loc and conf conv layers
        feature_layer: the feature layers for head to loc and conf
        num_classes: num of classes 
    """

    def __init__(self, base, extras, head, feature_layer, num_classes, num_keypoints=0,num_extrafeatures=0,
                 abox_split_fact=1, feature_norm_en=False, avoid_relu6=False, boxes_from_kp=False,
                 softmax_sigmoid = True,conf_extra_layer=False):
        super(SSDLite, self).__init__()
        self.num_classes = num_classes
        # SSD network
        self.base = nn.ModuleList(base)
        if feature_norm_en == True: 
            self.norm = L2Norm(feature_layer[1][0], 20)
        self.extras = nn.ModuleList(extras)
        self.extras_relu = nn.ModuleList([nn.ReLU(inplace=True) for _ in self.extras])

        self.loc = nn.ModuleList(head[0])
        self.conf = nn.ModuleList(head[1])

        if softmax_sigmoid == True:
            self.softmax = nn.Softmax(dim=-1)
        else:
            self.softmax = nn.Sigmoid()

        self.feature_layer = feature_layer[0]
        self.num_keypoints= num_keypoints
        self.num_extrafeatures= num_extrafeatures

        self.abox_split_fact =abox_split_fact
        self.boxes_from_kp = boxes_from_kp

        if boxes_from_kp == True:
          self.box_prm_size = 0
        else:
          self.box_prm_size = 4

        self.codeSize = self.box_prm_size + 2*self.num_keypoints+self.num_extrafeatures
        self.feature_norm_en = feature_norm_en
        self.avoid_relu6 = avoid_relu6
        self.conf_extra_layer = conf_extra_layer

    def forward(self, x, phase=None):
        """Applies network layers and ops on input image(s) x.

        Args:
            x: input image or batch of images. Shape: [batch,3,300,300].

        Return:
            Depending on phase:
            test:
                Variable(tensor) of output class label predictions,
                confidence score, and corresponding location predictions for
                each object detected. Shape: [batch,topk,7]

            train:
                list of concat outputs from:
                    1: confidence layers, Shape: [batch*num_priors,num_classes]
                    2: localization layers, Shape: [batch,num_priors*4]

            feature:
                the features maps of the feature extractor
        """
        if phase is None:
            phase = 'train' if self.training else 'eval'

        sources, loc, conf = [list() for _ in range(3)]
        # apply bases layers and cache source layer outputs
        for k in range(len(self.base)):
            x = self.base[k](x)

            for feature in self.feature_layer:
                if k == feature: #if k in self.feature_layer
                    if self.feature_norm_en == True:
                        if len(sources) == 0:
                            s = self.norm(x)
                            sources.append(s)
                        else:
                            sources.append(x)
                    else:
                        sources.append(x)

        # apply extra layers and cache source layer outputs
        for k, v in enumerate(self.extras):
            # x = F.relu(v(x), inplace=True)
            relu = self.extras_relu[k]
            x = relu(v(x))
            sources.append(x)
            # if k % 2 == 1:
            #     sources.append(x)

        if phase == 'feature':
            return sources

        for idx, (x, l) in enumerate(zip(sources, self.loc)):
            loc.append(l(x))

        if self.conf_extra_layer == False:
            for idx, (x, c) in enumerate(zip(sources, self.conf)):
                conf.append(c(x))
        else:
            for idx, (x, c0, c1) in enumerate(zip(sources, self.conf[0::2], self.conf[1::2])):
                conf.append(c1(c0(x)))

        output = (loc, conf)

        return output

def post_forward(input, codeSize, num_classes):

    in_loc  = input[0]
    in_conf = input[1]

    loc, conf = [list() for _ in range(2)]

    # apply multibox head to source layers
    for idx, l in enumerate(in_loc):
        loc.append(l.permute(0, 2, 3, 1).contiguous())

    for idx, c in enumerate(in_conf):
        conf.append(c.permute(0, 2, 3, 1).contiguous())

    loc = torch.cat([o.view(o.size(0), -1) for o in loc], 1)
    conf = torch.cat([o.view(o.size(0), -1) for o in conf], 1)

    output = (
        loc.view(loc.size(0), -1, codeSize),
        conf.view(conf.size(0), -1, num_classes),
    )

    return output


def add_extras(base, feature_layer, mbox, num_classes, num_keypoints=0, abox_split_fact=1, avoid_relu6=False,boxes_from_kp=False,num_extrafeatures=0,loc_conf_kernel_size=3,conf_extra_layer=False):
    extra_layers = []
    loc_layers = []
    conf_layers = []
    in_channels = None
    if boxes_from_kp == True:
      box_prm_size = 0
    else:
      box_prm_size = 4

    for layer, depth, box in zip(feature_layer[0], feature_layer[1], mbox):
        if layer == 'S':
            extra_layers += [ _conv_dw(in_channels, depth, stride=2, padding=1, expand_ratio=1,avoid_relu6=avoid_relu6) ]
            in_channels = depth
        elif layer == '':
            extra_layers += [ _conv_dw(in_channels, depth, stride=1, expand_ratio=1,avoid_relu6=avoid_relu6) ]
            in_channels = depth
        else:
            in_channels = depth
        if abox_split_fact == 1:
          loc_layers += [nn.Conv2d(in_channels, box * (box_prm_size + 2 * num_keypoints+num_extrafeatures),
                                   kernel_size=loc_conf_kernel_size, padding=(loc_conf_kernel_size>>1))]
          if conf_extra_layer == True:
              conf_layers += [nn.Conv2d(in_channels, in_channels, kernel_size=loc_conf_kernel_size,
                                        padding=(loc_conf_kernel_size >> 1))]

          conf_layers += [nn.Conv2d(in_channels, box * num_classes, kernel_size=loc_conf_kernel_size,
                                    padding=(loc_conf_kernel_size>>1))]

        else:
          # treat box*abox_split_fact as number of classes possible for a key points
          # dont compete keypoints witin the classes.
          #TODO Check

          # temporary fix to make conf output layer to be multiple of num_classes
          numConfOutLayer = int((box * num_classes  + box * abox_split_fact * abox_split_fact * (num_keypoints+num_extrafeatures/2) + num_classes - 1)/num_classes) * num_classes
          conf_layers += [nn.Conv2d(in_channels, numConfOutLayer, kernel_size=3, padding=1)]
          # 4 parameters ( topx,topy, w, h) is global parameter of box
          # 2N keypoints for each type of box and split factor
          loc_layers += [nn.Conv2d(in_channels, (box * 4) + (box * abox_split_fact * abox_split_fact * 2 * num_keypoints ), kernel_size=3, padding=1)]

    return base, extra_layers, (loc_layers, conf_layers)

# based on the implementation in https://github.com/tensorflow/models/blob/master/research/object_detection/models/feature_map_generators.py#L213
# when the expand_ratio is 1, the implemetation is nearly same. Since the shape is always change, I do not add the shortcut as what mobilenetv2 did.
def _conv_dw(inp, oup, stride=1, padding=0, expand_ratio=1,avoid_relu6=False):

    if avoid_relu6 == False:
        return nn.Sequential(
            # pw
            nn.Conv2d(inp, oup * expand_ratio, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup * expand_ratio),
            nn.ReLU6(inplace=True),
            # dw
            nn.Conv2d(oup * expand_ratio, oup * expand_ratio, 3, stride, padding, groups=oup * expand_ratio, bias=False),
            nn.BatchNorm2d(oup * expand_ratio),
            nn.ReLU6(inplace=True),
            # pw-linear
            nn.Conv2d(oup * expand_ratio, oup, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup),
        )
    else:
        return nn.Sequential(
            # pw
            nn.Conv2d(inp, oup * expand_ratio, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup * expand_ratio),
            nn.ReLU(inplace=True),
            # dw
            nn.Conv2d(oup * expand_ratio, oup * expand_ratio, 3, stride, padding, groups=oup * expand_ratio,
                      bias=False),
            nn.BatchNorm2d(oup * expand_ratio),
            nn.ReLU(inplace=True),
            # pw-linear
            nn.Conv2d(oup * expand_ratio, oup, 1, 1, 0, bias=False),
            nn.BatchNorm2d(oup),
        )


def build_ssd_lite(base, feature_layer, mbox, num_classes, num_keypoints = 0,num_extrafeatures=0, abox_split_fact = 1,feature_norm_en=False,avoid_relu6=False,boxes_from_kp=False,loc_conf_kernel_size=3,softmax_sigmoid=True):
    base_, extras_, head_ = add_extras(base(), feature_layer, mbox, num_classes, num_keypoints=num_keypoints,num_extrafeatures=num_extrafeatures,abox_split_fact=abox_split_fact,avoid_relu6=avoid_relu6,boxes_from_kp=boxes_from_kp,loc_conf_kernel_size=loc_conf_kernel_size)
    return SSDLite(base_, extras_, head_, feature_layer, num_classes, num_keypoints=num_keypoints,num_extrafeatures=num_extrafeatures, abox_split_fact=abox_split_fact,feature_norm_en=feature_norm_en,avoid_relu6=avoid_relu6,boxes_from_kp=boxes_from_kp,softmax_sigmoid=softmax_sigmoid)
