# ssds.pytorch.box.kp

This repository contains implementation of our work [Deep Learning based Parking Spot Detection and Classification in Fish-Eye Images](https://ieeexplore.ieee.org/document/9012933).
Our PyTorch based implementation is on top of [ssds.pytorch](https://github.com/ShuangXieIrene/ssds.pytorch/tree/v0.3.1). The repo ssds.pytorch was developed for 2D object detect. Our work enhances to predict key points along with boxes. This is useful in many scenarios where object can not be 'tightly' represented by up-right rectangles. One such example is any markings on the road. Due to perspective effect the objects rectangle in the real world will not stay rectangle in the image. Also fisheye images presents another scenario where such phenomenon is observed. More information can be found in our [published paper](https://ieeexplore.ieee.org/document/9012933).

<p align="center">
  <img style="border:4px solid black;" width="460" height="300" src=./doc/imgs/parking_illustration.jpg>
</p>

<center>The above example shows a parking spot that cannot be represented tightly by upright rectangle (in yellow). Shape enclosed by 4 key-points (in blue) represents it tighter. </center>
<br>

## Table of Contents
- [Supported Features](#supported-features)
- [Installation](#installation)
- [Usage](#usage)
- [Sample Output](#sample-output)
- [Dataset](#dataset)
- [Annotation Scheme](#annotation-scheme)
- [Accuracy](#accuracy)
- [Pre-Trained Weights](#pre-trained-weights)
- [Train on Custom Dataset](#train-on-custom-dataset)
- [Deploying On TI Deep Learning Soc](#deploying-on-ti-deep-learning-soc)
- [Acknowledgement](#acknowledgement)
- [Reference](#reference)

## Supported Features
- Key Point Based Object Detection in Fish Eye Images
- Three classes : Empty parking spot, Occupied parking spot and Vehicle
- Single Shot Detector Based Meta Architecture
- MobileNet-V1 as Backbone Network
- Visualization with [tensorboard-pytorch](https://github.com/lanpa/tensorboard-pytorch): training loss, eval loss/mAP, example anchor boxes.

## Installation
1. Go to directory "ssds.pytorch.box.kp\" and create conda environment using below command
'conda env create --name ssds --file=environment.yml'
2. Activate conda environment using the command "conda activate ssds"
3. Download the coco pre-trained model from [Link](https://drive.google.com/open?id=1yBpd3aIDvlK2j7HxsNj8kJuASTCaN5Bo) and place at ./weights/ssd-lite/. This is needed for fresh training of our network for parking spot and vehicle detection. However a trained model for parking spot and vehicle is available at ./weights/ssd_lite_box_kp

## Usage
Use below commands specific to training/evaluation/testing under ssds conda environment.

### Training
 "python train.py --cfg=./experiments/cfgs/config_ssd_lite_psd_vd.yml --phase=train". config_ssd_lite_psd_vd.yml is set to use coco pre-trained model from ./weights/ssd_lite/mobilenet_v1_ssd_lite_coco_18.8.pth
Typically it takes 48 hrs for 640 epochs on machine with 4 GTX1080 graphics cards. You can speed up training by using better cards like GTX1080Ti/RTX2080TI.

### Evaluation
  "python train.py --cfg=./experiments/cfgs/config_ssd_lite_psd_vd.yml --phase=eval".
This will use trained model generated in the above training command. If want to evaluate with pre-trained psd-vd model on internal dataset TI_PSD_VD_V3 then use below command
"python train.py --cfg ./experiments/cfgs/config_ssd_lite_psd_vd.yml --phase=eval --model=./weights/ssd_lite_box_kp/ssd_lite_mobilenet_v1_tiod_box_kp.pth"
### Testing
  "python train.py --cfg=./experiments/cfgs/config_ssd_lite_psd_vd.yml --phase=test".
This is for generating visualization. This will use trained model generated in the above training command. If want to test with pre-trained psd-vd model on internal dataset TI_PSD_VD_V3 then use below command
"python train.py --cfg ./experiments/cfgs/config_ssd_lite_psd_vd.yml --phase=test --model=./weights/ssd_lite_box_kp/ssd_lite_mobilenet_v1_tiod_box_kp.pth"

- Change the configuration file based on the note in [config_parse.py](./lib/utils/config_parse.py).
- Refer comments in the ./experiments/cfgs/config_ssd_lite_psd_vd.yml file for changing default values.
- Training check points are  available at $(EXP_DIR)/train_chkpts
- Training logs are available at $(EXP_DIR)/train_chkpts/train_log
- Evaluation logs are available at $(EXP_DIR)/train_chkpts/eval_log
- Test outputs are available at $(EXP_DIR)/train_chkpts/test_out


For quick sanity check run above commands with config_ssd_lite_psd_vd_tiny.yml. This config file usages the tiny dataset (./data/ti_psd_vd_v3_tiny) provided with this repo.
After training for 1 epoch on tiny dataset, when evaluated it should get Mean AP above 70%.


## Sample Output
<p align="center">
  <img style="border:4px solid black;" width="460" height="300" src="./doc/imgs/sequence0003_camera002_data_0000000540.png" rel = "Sample Detected Objects">
</p>

- Lines are drawn between two adjacent detected key points.
- Refer our [published paper](https://ieeexplore.ieee.org/document/9012933) for pixel level mask generation from these detected corner points.

## Dataset
We have developed internal dataset called, **TI_PSD_VD_V3** which has bounding box + additional key points annotation. It has been annotated for the following three classes,
- Empty Parking Spot
- Occupied Parking Spot
- Vehicle

## Annotation Scheme
Four corner points co-ordinates are captured for each parking spot and vehicle. Order of key point is maintained while annotation. First and second key points are the left and right corner of the entry of parking spot respectively. Rest other corner points are captured in anti-clockwise direction. For vehicles, first and second key points are the left most and right most key points on the ground respectively of the rectangle which encompasses the vehicle. Other two key points are the other two corners of vehicle captured in anti-clockwise direction of vehicle.

Bounding box parameter is the rectangular box enclosing these captured four key points. Bounding box parameters are not annotated but derived after key points are captured.

Tiny dataset with its corresponding annotation is available at ./data/ti_psd_vd_v3_mini


## Accuracy
Accuracy for TI_PSD_VD_v3 data set is as below @IOU threshold = 0.6. This is the accuracy corresponding to pre-trained model mentioned below.

   | Class                | box mAP            |
   | -----------          | -----------    |
   |Empty Parking Spot    | 85.48%|
   |Occupied Parking Spot | 75.75%|
   |Vehicle               | 67.25%|
   |Mean AP               | 76.16%|

## Pre-trained Weights
Pre-trained weights with above accuracy is provided at ./weights/ssd_lite_box_kp/ssd_lite_mobilenet_v1_tiod_box_kp.pth


## Train on Custom Dataset
For training on your own dataset, point your dataset_root to repo_home/data/dataset_root. The dataset_root is expected to have the following structure.

  - dataset_root/images/train
  - dataset_root/images/val
  - dataset_root/annos/train
  - dataset_root/annos/val

Make sure the following values are correct in ./experiments/cfgs/config_ssd_lite_psd_vd.yml.

  - NUM_CLASSES: 4
  - VIZ_CLASS: ['parking_empty','parking_occupied', 'vehicle']
  - OBJ_CLASSES: ('__background__', 'parking_empty', 'parking_occupied', 'vehicle')

## Deploying On TI Deep Learning Soc
To run this model on TI TDA4 class of devices, it needs two files- ONNX model for core CNN part and prototxt file for post-processing(detection layer). These files can be generated by setting SAVE_OD_PROTOTXT and SAVE_ONNX to True in the config file. These files are needed to import the trained onnx model to the TI Deep Learning Library [TIDL](https://software-dl.ti.com/jacinto7/esd/processor-sdk-rtos-jacinto7/latest/exports/docs/psdk_rtos/docs/user_guide/sdk_components_j721e.html#tidl) compatible format for deploying on various TI Deep Learning Socs.

## Acknowledgement

Our work is developed on top of [ssds.pytorch](https://github.com/ShuangXieIrene/ssds.pytorch/tree/v0.3.1). The repo [ssds.pytorch](https://github.com/ShuangXieIrene/ssds.pytorch/tree/v0.3.1) itself is dependent on various other repos like, [ssd.pytorch](https://github.com/amdegroot/ssd.pytorch), [faster-rcnn.pytorch](https://github.com/jwyang/faster-rcnn.pytorch), [RFBNet](https://github.com/ruinmessi/RFBNet), [Detectron](https://github.com/facebookresearch/Detectron) and [Tensorflow Object Detection API](https://github.com/tensorflow/models/tree/master/research/object_detection). We would like to thanks respective owners of these repositories.


## Reference
- [Deep Learning based Parking Spot Detection and Classification in Fish-Eye Images](https://ieeexplore.ieee.org/document/9012933)
