# Texas Instruments (C) 2018-2021
# All Rights Reserved

# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice, this
#  list of conditions and the following disclaimer.

# * Redistributions in binary form must reproduce the above copyright notice,
#  this list of conditions and the following disclaimer in the documentation
#  and/or other materials provided with the distribution.

# * Neither the name of the copyright holder nor the names of its
#  contributors may be used to endorse or promote products derived from
#  this software without specific prior written permission.

# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
# DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
# FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
# DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
# SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
# OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

import numpy as np
def initHSVTable():
  #HSV table in RGB format
  HSV_COLOR_MAP = np.array([ 
   [1.0000,         0,         0],
   [1.0000,    0.0938,         0],
   [1.0000,    0.1875,         0],
   [1.0000,    0.2813,         0],
   [1.0000,    0.3750,         0],
   [1.0000,    0.4688,         0],
   [1.0000,    0.5625,         0],
   [1.0000,    0.6563,         0],
   [1.0000,    0.7500,         0],
   [1.0000,    0.8438,         0],
   [1.0000,    0.9375,         0],
   [0.9688,    1.0000,         0],
   [0.8750,    1.0000,         0],
   [0.7813,    1.0000,         0],
   [0.6875,    1.0000,         0],
   [0.5938,    1.0000,         0],
   [0.5000,    1.0000,         0],
   [0.4063,    1.0000,         0],
   [0.3125,    1.0000,         0],
   [0.2188,    1.0000,         0],
   [0.1250,    1.0000,         0],
   [0.0313,    1.0000,         0],
   [     0,    1.0000,    0.0625],
   [     0,    1.0000,    0.1563],
   [     0,    1.0000,    0.2500],
   [     0,    1.0000,    0.3438],
   [     0,    1.0000,    0.4375],
   [     0,    1.0000,    0.5313],
   [     0,    1.0000,    0.6250],
   [     0,    1.0000,    0.7188],
   [     0,    1.0000,    0.8125],
   [     0,    1.0000,    0.9063],
   [     0,    1.0000,    1.0000],
   [     0,    0.9063,    1.0000],
   [     0,    0.8125,    1.0000],
   [     0,    0.7188,    1.0000],
   [     0,    0.6250,    1.0000],
   [     0,    0.5313,    1.0000],
   [     0,    0.4375,    1.0000],
   [     0,    0.3438,    1.0000],
   [     0,    0.2500,    1.0000],
   [     0,    0.1563,    1.0000],
   [     0,    0.0625,    1.0000],
   [0.0313,         0,    1.0000],
   [0.1250,         0,    1.0000],
   [0.2188,         0,    1.0000],
   [0.3125,         0,    1.0000],
   [0.4063,         0,    1.0000],
   [0.5000,         0,    1.0000],
   [0.5938,         0,    1.0000],
   [0.6875,         0,    1.0000],
   [0.7813,         0,    1.0000],
   [0.8750,         0,    1.0000],
   [0.9688,         0,    1.0000],
   [1.0000,         0,    0.9375],
   [1.0000,         0,    0.8438],
   [1.0000,         0,    0.7500],
   [1.0000,         0,    0.6563],
   [1.0000,         0,    0.5625],
   [1.0000,         0,    0.4688],
   [1.0000,         0,    0.3750],
   [1.0000,         0,    0.2813],
   [1.0000,         0,    0.1875],
   [1.0000,         0,    0.0938]])                          
  
  #convert it to BGR
  for index,color in enumerate(HSV_COLOR_MAP):
    HSV_COLOR_MAP[index] = [HSV_COLOR_MAP[index][2],HSV_COLOR_MAP[index][1],HSV_COLOR_MAP[index][0]]  
  HSV_COLOR_MAP = HSV_COLOR_MAP*255.0
  HSV_COLOR_MAP = HSV_COLOR_MAP.astype(int)
  return HSV_COLOR_MAP

